namespace :db do
  desc 'Populates database with test data'
  task :populate => :environment do
    assets      = HeaderAccount.find_by_name('Assets')
    liabilities = HeaderAccount.find_by_name('Liabilities')
    income      = HeaderAccount.find_by_name('Income')
    expense     = HeaderAccount.find_by_name('Expense')

    fixed_assets = CategoryType.create!(
        name: 'Fixed Assets',
        description: 'Cash, Cash in the Banks and Other valuable can be cashed in short term.',
        header_account: assets
    )

    current_liabilities = CategoryType.create!(
        name: 'Current liabilities',
        description: 'Payables in a short term period',
        header_account: liabilities
    )

    sales_income = CategoryType.create!(
        name: 'Sales Income',
        description: 'Money earned through business',
        header_account: income
    )

    cost_of_goods_sold = CategoryType.create!(
        name: 'Cost of goods sold',
        description: 'Direct expenses related to sales',
        header_account: expense
    )

    DetailType.create!(
        name: 'Vehicles',
        description: 'The vehicles company owned or Partially owned',
        category_type: fixed_assets
    )

    DetailType.create!(
        name: 'Rent Payable',
        description: 'Payable Rents',
        category_type: current_liabilities
    )

    DetailType.create!(
        name: 'Shift Sales',
        description: 'Only sales in with walking customers',
        category_type: sales_income
    )

    DetailType.create!(
        name: 'Fertilizer',
        description: 'Fertilizer',
        category_type: cost_of_goods_sold
    )
  end
end
