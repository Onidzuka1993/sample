require 'rails_helper'

RSpec.describe FoldersHelper do
	let(:transfer) 			{create(:transfer_folder)}
	let(:expense)  			{create(:folder, folder_type: 'expense')}
	let(:check)		 			{create(:folder, folder_type: 'check')}
	let(:journal_entry) {create(:folder, folder_type: 'journal_entry')}

	describe '#get_folder_path' do
		context 'when transfer' do
			it 'returns transfers#show path' do
				path = get_folder_path(transfer)

				expect(path).to eql(edit_transfer_path(transfer))
			end
		end

		context 'when expense' do
			it 'returns expenses#show path' do
				path = get_folder_path(expense)

				expect(path).to eql(edit_expense_path(expense))
			end
		end

		context 'when check' do
			it 'returns checks#show path' do
				path = get_folder_path(check)

				expect(path).to eql(edit_check_path(check))
			end
		end

		context 'when journal entry' do
			it 'returns journal_entries#show path' do
				path = get_folder_path(journal_entry)

				expect(path).to eql(edit_journal_entry_path(journal_entry))
			end
		end
	end
end