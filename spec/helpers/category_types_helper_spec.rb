require 'rails_helper'

RSpec.describe CategoryTypesHelper do
  describe '#category_types' do
    it 'returns array of category types' do
      expect(category_types).to match_array([
          'Income',
          'Cost of Goods Sold',
          'Expenses',
          'Accounts receivable',
          'Current Assets',
          'Bank',
          'Fixed Assets',
          'Other Assets',
          'Account payable(A/P)',
          'Credit Card',
          'Current Liabilities',
          'Long Term Liabilities',
          'Equity'
        ]
      )
    end
  end
end