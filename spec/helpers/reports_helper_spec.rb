require 'rails_helper'

RSpec.describe ReportsHelper do
  describe '#options_for_select' do
    it 'returns options for select' do
      expected_result = [
          ['Year to date',  'year_to_date'],
          ['This month',    'this_month '],
          ['Last month',    'last_month'],
          ['Last 3 months', 'last_three_months'],
          ['Last year',     'last_year'],
          'Custom date'
      ]

      result = select_options

      expect(result).to eql(expected_result)
    end
  end
end