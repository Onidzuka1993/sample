require 'rails_helper'

RSpec.describe AccountsHelper do
  describe '#sort_by' do
    context 'when given only column parameter' do
      it 'returns path with direction asc' do
        params[:direction] = 'desc'

        result = sort_by('name')
        expect(result).to eql(accounts_path(column: 'name', direction: 'asc'))
      end

      it 'returns path with direction asc' do
        params[:direction] = ''

        result = sort_by('name')
        expect(result).to eql(accounts_path(column: 'name', direction: 'asc'))
      end

      it 'returns path with direction desc' do
        params[:direction] = 'asc'

        result = sort_by('name')
        expect(result).to eql(accounts_path(column: 'name', direction: 'desc'))
      end
    end

    context 'when given column and direction parameter' do
      it 'returns path with query string' do
        result = sort_by('name', 'asc')

        expect(result).to eql(accounts_path(column: 'name', direction: 'asc'))
      end
    end
  end
end