require 'rails_helper'

RSpec.describe ApplicationHelper do
	describe '#formatted_amount' do
		it 'return currency formatted amount' do
			amount = formatted_amount(100)

			expect(amount).to eql('$100.00')
		end
	end
end