require 'rails_helper'

RSpec.describe CategoryTypesController do
	let(:header_account) {HeaderAccount.find_by_name('Assets')}
  let(:category_type)  {CategoryType.find_by_name!('Bank')}

  let(:use_case) {double(CategoryType)}

  describe '#new' do
    it 'renders new template' do
      get :new

      expect(assigns[:category_type]).to be_a_new(CategoryType)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:new)
    end
  end

  describe '#create' do
    let(:create_params)  { {name: 'test name', description: 'test description', header_account_id: header_account.id} }

    it 'creates category types' do
      post :create, params: {category_type: create_params}

      expect(flash[:notice]).to eql('Category type has successfully been saved.')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(CategoryType.last)
    end
  end

  describe '#edit' do
    it 'renders edit template' do
      get :edit, params: {id: category_type.id}

      expect(assigns[:category_type]).to    eql(category_type)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:edit)
    end
  end

  describe '#update' do
    let(:category_type_params) { {name: 'test name', description: 'test description', header_account_id: header_account.id} }
    let(:invalid_params)     { {name: '', description: ''} }

    it 'updates category type' do
      put :update, params: {id: category_type.id, category_type: category_type_params}

      expect(flash[:notice]).to eql('Category type has successfully been updated.')

      expect(response).to redirect_to category_type
      expect(response).to have_http_status(:found)
    end

    context 'when invalid params' do
      it 'does not update' do
        put :update, params: {id: category_type.id, category_type: invalid_params}

        expect(response).to render_template(:edit)
      end
    end
  end

  describe '#show' do
    it 'renders show template' do
      get :show, params: {id: category_type.id}

      expect(assigns[:category_type]).to eql(category_type)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end

    context 'XHR request' do
      it 'it renders category types' do
        get :show, params: {id: category_type.id}, xhr: true

        expect(response.body).to eql(category_type.detail_types.to_json(only: ['id', 'name']))
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe '#index' do
    it 'renders index template' do
      get :index

      expect(assigns[:category_types].count).to eql(2)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
		end

		it 'eager loads header accounts' do
			expect(CategoryType).to receive(:includes).with(:header_account)

			get :index
		end
  end

  describe '#destroy' do
		before {allow(RemoveCategoryType).to receive(:call).with(category_type_id: '1').and_return(use_case)}

    it 'deletes record from db' do
			allow(use_case).to receive(:success?).and_return true
      allow(use_case).to receive(:message).and_return('Category type deleted.')

      delete :destroy, params: {id: category_type.id}

      expect(flash[:alert]).to eql('Category type deleted.')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to category_types_url
    end

    context 'category type has categories' do
      it 'renders index template' do
        allow(use_case).to receive(:success?).and_return false
        allow(use_case).to receive(:message).and_return 'This category type has detail types.'
        allow(use_case).to receive(:category_types).and_return Array.new

        delete :destroy, params: {id: category_type.id}

        expect(flash[:alert]).to eql('This category type has detail types.')

        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:index)
      end
    end
	end
end