require 'rails_helper'

RSpec.describe AccountsController do
  let(:detail_type) {DetailType.find_by_name!('Cash')}
  let(:account)     {Account.find_by_name!('Cash')}

  describe '#new' do
    before {get :new}

		it 'sorts Header Accounts by name' do
      expect(HeaderAccount).to receive(:order).with(:name).once

			get :new
		end

    it 'assigns instance variable' do
      expect(assigns[:account]).to be_a_new(Account)
		end

    it_behaves_like 'new examples'
  end

  describe '#create' do
    before(:example) {post :create, params: {account: {detail_type_id: detail_type.id, name: 'test', description: 'test'}}}

		it 'assigns flash message' do
      account = Account.last
      expect(flash[:notice]).to eql('Account has successfully been saved.')
		end

		it_behaves_like 'create or update examples' do
			let(:object) {Account.last}
		end

    context 'when invalid params' do
      before do
        expect(HeaderAccount).to receive(:order).with(:name).once
        expect(CategoryType).to  receive(:order).with(:name).once
        expect(DetailType).to    receive(:order).with(:name).once

        post :create, params: {account: {detail_type_id: detail_type.id, name: '', description: ''}}
      end

      it_behaves_like 'new examples'
    end
  end

  describe '#show' do
		before(:example) {get :show, params: {id: account.id}}

    it 'assigns instance variable' do
      expect(assigns[:account]).to eql(account)
		end

		it_behaves_like 'show examples'

		context 'when xhr' do
			before {get :show, params: {id: account.id}, xhr: true}

			it 'returns account balance' do
				expect(response.body).to eql({balance: account.total_balance_amount}.to_json)
			end
		end
  end

  describe '#edit' do
    it 'render edit template' do
      get :edit, params: {id: account.id}

      expect(assigns[:account]).to eql(account)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:edit)
    end
  end

  describe '#update' do
    it 'updates account' do
      account_params = {detail_type_id: detail_type.id, name: 'test', description: 'test'}

      put :update, params: {id: account.id, account: account_params }

      expect(flash[:notice]).to eql('Account has successfully been updated.')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(account)
    end

    context 'when invalid params' do
      it 'does not update account' do
        invalid_params = {detail_type_id: '', name: '', description: ''}

        put :update, params: {id: account.id, account: invalid_params}

        expect(response).to have_http_status(:ok)
        expect(response).to render_template :edit
      end
    end
  end

  describe '#index' do
    it 'renders index template' do
      get :index

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end

    context 'when given params' do
      it 'renders index template' do
        get :index, params: {column: 'name', direction: 'desc'}

        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:index)
      end
		end

		context 'when XHR request' do
			it 'renders json of all accounts' do
				get :index, xhr: true

				expect(response).to      have_http_status(:ok)
				expect(response.body).to eql((Account.order(:name).to_json(only: ['id', 'name'])))
			end
		end
  end

  describe '#destroy' do
    it 'destroys account (uses paranoia gem)' do
      delete :destroy, params: {id: account.id}

      deleted_accounts = Account.only_deleted

      expect(deleted_accounts.count).to eql(1)
      expect(deleted_accounts.first).to eql(account)

      expect(flash[:alert]).to eql('Account deleted.')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(accounts_url)
    end
  end
end