require 'rails_helper'

RSpec.describe JournalEntriesController do
	describe '#new' do
		before {get :new}

		it_behaves_like 'new examples'
	end

	describe '#create' do
		let(:journal_entry) {double(:journal_entry, message: {error: 'test'})}

		before(:example) do
			allow(JournalEntryOrganizer).to receive(:call).and_return(journal_entry)
			allow(journal_entry).to 			  receive(:success?).and_return(true)

			post :create, params: {journal_entry: {test: 'test'}}, xhr: true, format: :json
		end

		it 'returns http status ok' do
			expect(response).to have_http_status(:created)
		end

		it 'returns success' do
			expect(response.body).to eql({success: true}.to_json)
		end

		context 'when error' do
			before do
				allow(JournalEntryOrganizer).to receive(:call).and_return(journal_entry)
				allow(journal_entry).to 			  receive(:success?).and_return(false)

				post :create, params: {journal_entry: {test: 'test'}}, xhr: true
			end

			it 'returns http status 301' do
				expect(response).to have_http_status(:unprocessable_entity)
			end

			it 'returns error message' do
				expect(response.body).to eql({error: 'test'}.to_json)
			end
		end
	end

	describe '#edit' do
		let(:folder) {create(:folder, folder_type: 'journal_entry')}

		before {get :edit, params: {id: folder.id}}

		it 'finds journal entry folder' do
			expect(assigns[:folder]).to eql(folder)
		end

		it_behaves_like 'edit examples'
	end

	describe '#update' do
		it ''
	end
end