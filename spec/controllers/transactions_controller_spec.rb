require 'rails_helper'

RSpec.describe TransactionsController do
	describe '#show' do
		before {get :show}

		it 'render show template' do
			expect(response).to render_template(:show)
		end
		
		it 'returns http status 200' do
			expect(response).to have_http_status(:ok)
		end
	end

	describe '#index' do
		before do
			expect(Transaction).to receive(:all)

			get :index
		end

		it 'renders index template' do
			expect(response).to render_template(:index)
		end

		it 'returns http status 200' do
			expect(response).to have_http_status(:ok)
		end
	end
end