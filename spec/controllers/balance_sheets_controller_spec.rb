require 'rails_helper'

RSpec.describe BalanceSheetsController do
  let(:use_case) 			{double}
  let(:balance_sheet) {double}

  describe '#show' do
    before do
      expect(GetBalanceSheet).to receive(:call).with(date: '12/12/12').and_return(use_case)
      expect(use_case).to        receive(:success?).and_return(true)
      expect(use_case).to        receive(:balance_sheet).and_return(balance_sheet)
      expect(balance_sheet).to   receive(:reload)
      expect(use_case).to   		 receive(:date)

			get :show, params: {date: '12/12/12'}
    end

		it_behaves_like 'show examples'
	end

	context 'when no accounts found' do
		before do
			allow(GetBalanceSheet).to  receive(:call).with(date: Date.current.to_s).and_return(use_case)
			allow(use_case).to				 receive(:success?).and_return(false)
			allow(use_case).to 				 receive(:message).and_return('Accounts not found')

			get :show, params: {date: Date.current.to_s}
		end

		it 'assigns flash' do
			expect(flash.now[:alert]).to eql('Accounts not found')
		end

		it 'returns http status ok' do
			expect(response).to have_http_status(:ok)
		end

		it 'renders reports/index template' do
			expect(response).to render_template('reports/index')
		end
	end
end