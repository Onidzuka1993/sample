require 'rails_helper'

RSpec.describe DetailTypesController do
	let(:category_type) {CategoryType.find_by_name!('Bank')}
  let(:detail_type)   {DetailType.find_by_name!('Cash')}

	let(:use_case)      {double(DetailType)}

  describe '#new' do
    before {expect(HeaderAccount).to receive(:order).with(:name)}

    it 'renders new template' do
      get :new

      expect(assigns[:detail_type]).to be_a_new(DetailType)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:new)
		end
  end

  describe '#create' do
    it 'creates detail type' do
      post :create, params: {detail_type: {category_type_id: category_type.id, name: 'test', description: 'test'}}

      detail_type = DetailType.last

      expect(flash[:notice]).to eql('Detail type has successfully been saved.')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(detail_type)
    end

    context 'when invalid params' do
      before do
        expect(HeaderAccount).to receive(:order).with(:name)
        expect(CategoryType).to receive(:order).with(:name)
      end

      it 'renders edit template' do
        post :create, params: {detail_type: {category_type_id: '', name: '', description: ''}}

        expect(response).to render_template(:new)
      end
    end
  end

  describe '#show' do
		before {get :show, params: {id: detail_type.id}}

		it_behaves_like 'show examples'

		it 'assigns value for instance variable' do
		  expect(assigns[:detail_type]).to eql(detail_type)
		end
  end

  describe '#edit' do
    it 'renders edit template' do
      get :edit, params: {id: detail_type.id}

      expect(assigns[:detail_type]).to eql(detail_type)

      expect(response).to render_template(:edit)
      expect(response).to have_http_status(:ok)
    end
  end

  describe '#update' do
    it 'updates detail type' do
      put :update, params: {id: detail_type.id, detail_type: {category_type_id: category_type.id, name: 'test', description: 'test'}}

      expect(flash[:notice]).to eql('DetailType has successfully been updated.')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(detail_type)
    end

    context 'when invalid params' do
      it 'does not update detail type' do
        put :update, params: {id: detail_type.id, detail_type: {category_type_id: '', name: '', description: ''}}

        expect(response).to render_template(:edit)
      end
    end
  end

  describe '#index' do
    it 'renders index template' do
      get :index

      expect(assigns[:detail_types].count).to eql(2)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
		end

		it 'eager loads category types' do
			expect(DetailType).to receive(:includes).with(:category_type)

			get :index
		end
  end

  describe '#destroy' do
		before {allow(RemoveDetailType).to receive(:call).with(detail_type_id: '1').and_return(use_case)}

    it 'removes detail type' do
			allow(use_case).to receive(:success?).and_return true
			allow(use_case).to receive(:message).and_return('Detail type deleted.')

      delete :destroy, params: {id: detail_type.id}

      expect(flash[:alert]).to eql('Detail type deleted.')

      expect(response).to redirect_to(detail_types_url)
      expect(response).to have_http_status(:found)
    end

    context 'when detail type has accounts' do
			before do
        allow(use_case).to receive(:success?).and_return false
        allow(use_case).to receive(:message).and_return('This detail type has accounts.')
        allow(use_case).to receive(:detail_types).and_return(Array.new)
			end

      it 'does not remove detail type' do
        delete :destroy, params: {id: detail_type.id}

        expect(flash[:alert]).to eql('This detail type has accounts.')

        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:index)
      end
    end
  end
end