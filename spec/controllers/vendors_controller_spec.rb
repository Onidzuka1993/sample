require 'rails_helper'

RSpec.describe VendorsController do
	let(:vendor) {create(:vendor)}
	let(:params) {{name: 'John', email: 'john@gmail.com', phone_number: '7109998888', address: '120 N St Creede, CO'}}

	describe '#new' do
		before {get :new}

		it_behaves_like 'new examples'

		it 'assigns vendor' do
			expect(assigns[:vendor]).to be_a_new(Vendor)
		end
	end

	describe '#create' do
		before {post :create, params: {vendor: params}}

		it_behaves_like 'create or update examples' do
			let(:object) {Vendor.last}
		end

		it 'sets flash message' do
			expect(flash[:notice]).to eql('Vendor has successfully been saved.')
		end

		context 'when invalid request' do
			before {post :create, params: {vendor: params.merge(name: '')}}

			it_behaves_like 'new examples'
		end
	end

	describe '#show' do
		before {get :show, params: {id: vendor.id}}

		it_behaves_like 'show examples'

		it 'assigns instance variable' do
			expect(assigns[:vendor]).to eql(vendor)
		end
	end

	describe '#edit' do
		before {get :edit, params: {id: vendor.id}}

		it_behaves_like 'edit examples'

		it 'assigns instance variable' do
			expect(assigns[:vendor]).to eql(vendor)
		end
	end

	describe '#update' do
		before { put :update, params: {id: vendor.id, vendor: params} }

		it 'sets flash message' do
			expect(flash[:notice]).to eql('Vendor has successfully been updated.')
		end

		it_behaves_like 'create or update examples' do
			let(:object) {vendor}
		end

		context 'when invalid params' do
			before { put :update, params: {id: vendor.id, vendor: params.merge(name: '')} }

			it_behaves_like 'edit examples'
		end
	end

	describe '#index' do
		before {get :index}

		it 'returns all vendors' do
			2.times { |index| create(:vendor, email: "john#{index}@gmail.com")}

			expect(assigns[:vendors].count).to eql(Vendor.count)
		end

		it_behaves_like 'index examples'
	end

	describe '#destroy' do
		it 'removes vendor' do
			delete :destroy, params: {id: vendor.id}

			expect(flash[:alert]).to eql('Vendor deleted.')
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(vendors_url)
		end
	end
end