require 'rails_helper'

RSpec.describe ChecksController do
	describe '#new' do
		before {get :new}

		it_behaves_like 'new examples'
	end

	describe '#create' do
		let(:check) {double(:check, print_check_data: {test: 'test'}, message: {error: 'test'})}

		before(:example) do
			allow(CreateCheck).to receive(:call).and_return(check)
			allow(check).to 			receive(:success?).and_return(true)

			post :create, params: {check: {test: 'test'}}, xhr: true, format: :json
		end

		it 'returns http status ok' do
			expect(response).to have_http_status(:ok)
		end

		it 'returns renders json with print check data' do
			expect(response.body).to eql({test: 'test'}.to_json)
		end

		context 'when error' do
			before do
				allow(CreateCheck).to receive(:call).and_return(check)
				allow(check).to 			receive(:success?).and_return(false)

				post :create, params: {check: {test: 'test'}}, xhr: true
			end

			it 'returns http status 301' do
				expect(response).to have_http_status(:unprocessable_entity)
			end

			it 'returns error message' do
				expect(response.body).to eql({error: 'test'}.to_json)
			end
		end
	end

	describe '#edit' do
		let(:folder) {create(:folder, folder_type: 'check')}

		before {get :edit, params: {id: folder.id}}

		it 'finds check folder' do
			expect(assigns[:folder]).to eql(folder)
		end

		it_behaves_like 'edit examples'
	end

	describe '#update' do
		it 'updates check transactions'
	end
end