require 'rails_helper'

RSpec.describe CustomersController do
	let(:customer) {create(:customer)}
	let(:params) 	 {{name: 'John', email: 'john@gmail.com', phone_number: '7109998888', address: '120 N St Creede, CO'}}

	describe '#new' do
		before {get :new}

		it_behaves_like 'new examples'

		it 'assigns customer' do
			expect(assigns[:customer]).to be_a_new(Customer)
		end
	end

	describe '#create' do
		before {post :create, params: {customer: params}}

		it_behaves_like 'create or update examples' do
			let(:object) {Customer.last}
		end

		it 'sets flash message' do
			expect(flash[:notice]).to eql('Customer has successfully been saved.')
		end

		context 'when invalid request' do
			before {post :create, params: {customer: params.merge(name: '')}}

			it_behaves_like 'new examples'
		end
	end

	describe '#show' do
		before {get :show, params: {id: customer.id}}

		it_behaves_like 'show examples'

		it 'assigns instance variable' do
			expect(assigns[:customer]).to eql(customer)
		end
	end

	describe '#edit' do
		before {get :edit, params: {id: customer.id}}

		it_behaves_like 'edit examples'

		it 'assigns instance variable' do
			expect(assigns[:customer]).to eql(customer)
		end
	end

	describe '#update' do
		before { put :update, params: {id: customer.id, customer: params} }

		it 'sets flash message' do
			expect(flash[:notice]).to eql('Customer has successfully been updated.')
		end

		it_behaves_like 'create or update examples' do
			let(:object) {customer}
		end

		context 'when invalid params' do
			before { put :update, params: {id: customer.id, customer: params.merge(name: '')} }

			it_behaves_like 'edit examples'
		end
	end

	describe '#index' do
		before {get :index}

		it 'returns all customers' do
			2.times { |index| create(:customer, email: "john#{index}@gmail.com")}

			expect(assigns[:customers].count).to eql(Customer.count)
		end

		it_behaves_like 'index examples'
	end

	describe '#destroy' do
		it 'removes customer' do
			delete :destroy, params: {id: customer.id}

			expect(flash[:alert]).to eql('Customer deleted.')
			expect(response).to have_http_status(:found)
			expect(response).to redirect_to(customers_url)
		end
	end
end