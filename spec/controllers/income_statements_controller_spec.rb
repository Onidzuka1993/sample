require 'rails_helper'

RSpec.describe IncomeStatementsController do
	let(:use_case) {double(GetIncomeStatement)}

  describe '#show' do
		before do
			allow(GetIncomeStatement).to receive(:call).and_return(use_case)
		end

    it 'renders show template' do
			allow(use_case).to receive(:success?).and_return(true)
      allow(use_case).to receive(:income_statement).and_return(Array.new)
      allow(use_case).to receive(:net_income).and_return(1000.0)
      allow(use_case).to receive(:date).and_return(Date.current)

      get :show, params: {filter_by: 'year_to_date'}

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end

    context 'when request is invalid' do
      before do
        allow(use_case).to receive(:success?).and_return(false)
        allow(use_case).to receive(:message).and_return('Accounts not found')

        get :show, params: {filter_by: 'last_year'}
			end

      it 'renders index template' do
        expect(response).to render_template('reports/index')
			end

			it 'returns https status 200' do
        expect(response).to have_http_status(:ok)
			end

			it 'sets flash' do
				expect(flash[:alert]).to eql('Accounts not found')
			end
    end
  end
end