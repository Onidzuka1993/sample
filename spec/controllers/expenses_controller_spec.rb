require 'rails_helper'

RSpec.describe ExpensesController do
	describe '#new' do
		before {get :new}

		it_behaves_like 'new examples'
	end

	describe '#create' do
		let(:expense) {double(:expense, message: {error: 'test'})}

		before(:example) do
			allow(CreateExpense).to receive(:call).and_return(expense)
			allow(expense).to				receive(:success?).and_return(true)

			post :create, params: {expense: {test: 'test'}}, xhr: true, format: :json
		end

		it 'returns http status ok' do
			expect(response).to have_http_status(:ok)
		end

		it 'returns success' do
			expect(response.body).to eql({success: true}.to_json)
		end

		context 'when error' do
			before do
				allow(CreateExpense).to receive(:call).and_return(expense)
				allow(expense).to       receive(:success?).and_return(false)

				post :create, params: {expense: {test: 'test'}}, xhr: true
			end

			it 'returns http status 422' do
				expect(response).to have_http_status(422)
			end

			it 'returns error' do
				expect(response.body).to eql({error: 'test'}.to_json)
			end
		end
	end

	describe '#edit' do
		let(:folder) {create(:folder, folder_type: 'expense')}

		before {get :edit, params: {id: folder.id}}

		it 'finds expense folder' do
			expect(assigns[:folder]).to eql(folder)
		end

		it_behaves_like 'edit examples'
	end

	describe '#update' do
		it ''
	end
end