require 'rails_helper'

RSpec.describe HeaderAccountsController do
  let(:header_account) { HeaderAccount.find_by_name!('Assets') }

  describe '#edit' do
		before {get :edit, params: {id: header_account.id}}

		it_behaves_like 'edit examples'

    it 'finds header_account' do
      expect(assigns[:header_account]).to eql(header_account)
		end
  end

  describe '#update' do
    let(:update_params) { {description: 'test description'} }
    let(:invalid_params) { {description: ''} }

		before { put :update, params: {id: header_account.id, header_account: update_params} }

    it 'sets flash messsage' do
      expect(flash[:notice]).to eql('Account has successfully been updated.')
		end

		it_behaves_like 'create or update examples' do
			let(:object) {header_account}
		end

    context 'when invalid params' do
			before { put :update, params: {id: header_account.id, header_account: invalid_params} }

			it_behaves_like 'edit examples'
    end
  end

  describe '#show' do
		before { get :show, params: {id: header_account.id} }

    it 'finds header account' do
      expect(assigns[:header_account]).to eql(header_account)
		end

		it_behaves_like 'show examples'

    context 'XHR request' do
			before { get :show, params: {id: header_account.id}, xhr: true }

      it 'it renders category types' do
        expect(response.body).to eql(header_account.category_types.to_json(only: ['id', 'name']))
			end

			it 'returns http status 200' do
        expect(response).to have_http_status(:ok)
			end
    end
  end

  describe '#index' do
		before { get :index }

		it 'returns all header accounts' do
      expect(assigns[:header_accounts].count).to eql(5)
		end

		it_behaves_like 'index examples'
  end
end