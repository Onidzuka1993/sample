require 'rails_helper'

RSpec.describe FoldersController do
	let(:transfer) 			{create(:transfer_folder)}
	let(:expense)  			{create(:folder, folder_type: 'expense')}
	let(:check)    			{create(:folder, folder_type: 'check')}
	let(:journal_entry) {create(:folder, folder_type: 'journal_entry')}

	describe '#index' do
		before { get :index, params: {filter_by: 'past_thirty_days'} }

		it_behaves_like 'index examples'
	end
end