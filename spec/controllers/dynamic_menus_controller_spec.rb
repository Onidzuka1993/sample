require 'rails_helper'

RSpec.describe DynamicMenusController do
  let(:cash)            {DetailType.find_by_name('Cash')}
  let(:opening_balance) {DetailType.find_by_name('Opening Balance')}
  let(:dynamic_menu)    {create(:dynamic_menu, from_list: [cash.id], to_list: [opening_balance.id], detail_type_ids: [cash.id, opening_balance.id])}

  describe '#new' do
    before {expect(DetailType).to receive(:order).with(:name)}

    it 'renders new template' do
      get :new

      expect(assigns[:dynamic_menu]).to be_a_new(DynamicMenu)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:new)
    end
  end

  describe '#create' do
    it 'creates dynamic menu' do
      params = {name: 'test', description: 'test', to_list: [cash.id], from_list: [opening_balance.id]}

      post :create, params: {dynamic_menu: params}

      expect(flash[:notice]).to eql('Dynamic menu has successfully been saved')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(DynamicMenu.last)
    end
  end

  describe '#show' do
    it 'renders show template' do
      get :show, params: {id: dynamic_menu.id}

      expect(assigns[:dynamic_menu]).to eql(dynamic_menu)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
  end

  describe '#edit' do
    it 'renders edit template' do
      get :edit, params: {id: dynamic_menu.id}

      expect(assigns[:dynamic_menu]).to eql(dynamic_menu)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:edit)
    end
  end

  describe '#update' do
    it 'updates dynamic menu' do
      params = {name: 'name', description: 'desc', to_list: [opening_balance.id], from_list: [cash.id]}

      put :update, params: {id: dynamic_menu.id, dynamic_menu: params}

      expect(flash[:notice]).to eql('Your dynamic menu has successfully been updated')

      expect(response).to redirect_to dynamic_menu
      expect(response).to have_http_status(:found)
    end
  end

  describe '#index' do
    before do
      create(:dynamic_menu, from_list: [cash.name], to_list: [opening_balance.name], detail_type_ids: [cash.id])
      create(:dynamic_menu, name: 'test', from_list: [cash.name], to_list: [opening_balance.name], detail_type_ids: [cash.id])
    end

    it 'renders index template' do
      get :index

      expect(assigns[:dynamic_menus].count).to eql(2)

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end
  end

  describe '#destroy' do
    it 'removes dynamic menu' do
      delete :destroy, params: {id: dynamic_menu.id}

      expect(flash[:alert]).to eql('You removed dynamic menu')

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(dynamic_menus_url)
    end
  end
end