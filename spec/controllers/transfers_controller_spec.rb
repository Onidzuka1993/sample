require 'rails_helper'

RSpec.describe TransfersController do
  let(:cash) {Account.find_by_name('Cash')}
  let(:car)  {create(:assets_account, name: 'car')}

  describe '#new' do
    before {get :new}

		it_behaves_like 'new examples'
  end

  describe '#create' do
    it 'redirects to transactions list' do
      params = {amount: 100, source_account_id: cash.id, target_account_id: car.id, happened_at: Date.current}

      post :create, params: params, xhr: true

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:create)
    end
	end

  describe '#edit' do
    let(:folder) {create(:folder, folder_type: 'transfer')}

    before {get :edit, params: {id: folder.id}}

    it 'finds transfer folder' do
      expect(assigns[:folder]).to eql(folder)
    end

    it_behaves_like 'edit examples'
	end

	describe '#update' do
		it ''
  end
end