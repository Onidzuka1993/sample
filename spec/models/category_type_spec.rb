require 'rails_helper'

RSpec.describe CategoryType do
	context 'associations' do
		it 'belongs to' do
			is_expected.to belong_to(:header_account)
		end

		it 'has_many' do
			is_expected.to have_many(:detail_types)
		end
	end

	context 'validations' do
		it 'validates presence of' do
			is_expected.to validate_presence_of(:name)
			is_expected.to validate_presence_of(:description)
		end

		it 'validates uniqueness of' do
			is_expected.to validate_uniqueness_of(:name).case_insensitive
		end
	end

	describe '#total_balance_amount' do
		let(:header_account) {HeaderAccount.find_by_name!('Assets')}

		let(:current_assets) {create(:current_assets)}
		let(:fixed_assets)   {create(:category_type, name: 'fixed assets', header_account: header_account)}

		let(:equipment)      {create(:other_current_assets)}
		let(:vehicle)			   {create(:detail_type, name: 'vehicle')}

		let(:calculator)		 {create(:assets_account, name: 'calculator')}
		let(:microwave)			 {create(:assets_account, name: 'microwave')}

		let(:params)         {{transaction_type: 'debit'}}

		before do
			DebitAccount.call(transaction_params: params.merge(amount: '200.0', source_account_id: calculator.id))
			DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: microwave.id))
		end

		it 'returns total balance' do
			expect(current_assets.total_balance_amount).to eql(300.0)
		end

		context 'when header account has no sub accounts' do
			it 'return 0.0' do
				expect(fixed_assets.total_balance_amount).to eql(0.0)
			end
		end
	end
end