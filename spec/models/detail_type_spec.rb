require 'rails_helper'

RSpec.describe DetailType do
  let(:header_account) {HeaderAccount.find_by_name!('Assets')}

  let(:current_assets) {create(:current_assets)}
  let(:equipment)      {create(:detail_type, name: 'equipment', category_type: current_assets)}
  let(:vehicle)        {create(:detail_type, name: 'vehicle', category_type: current_assets)}

  context 'associations' do
    it do
      is_expected.to belong_to(:category_type)
      is_expected.to have_many(:accounts)
      is_expected.to have_many(:dynamic_menus_detail_types)
      is_expected.to have_many(:dynamic_menus).through(:dynamic_menus_detail_types)
    end
  end

  context 'validations' do
    it 'validates presence of' do
      is_expected.to validate_presence_of(:name)
      is_expected.to validate_presence_of(:description)
		end

		it 'validates uniqueness of' do
      is_expected.to validate_uniqueness_of(:name).case_insensitive
		end
  end

  context 'delegations' do
    it do
      is_expected.to delegate_method(:header_account_id).to(:category_type)
    end
  end

  context '#total_balance_amount' do
		let(:calculator) {create(:assets_account, name: 'calculator', detail_type: equipment)}
		let(:microwave)  {create(:assets_account, name: 'microwave', detail_type: equipment)}
		let(:table)      {create(:assets_account, name: 'table', detail_type: equipment)}

		let(:params)     {{transaction_type: 'debit'}}

    before do
      DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: calculator.id))
      DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: microwave.id))
      DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: table.id))
    end

    it 'returns total_balance of accounts' do
      expect(equipment.total_balance_amount).to eql(300.0)
    end

    context 'when detail_type has no accounts' do
      it 'it returns 0.0 of total balance' do
        expect(vehicle.total_balance_amount).to eql(0.0)
      end
    end
  end
end