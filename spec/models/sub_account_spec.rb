require 'rails_helper'

RSpec.describe CategoryType do
  let(:header_account)       {HeaderAccount.find_by_name!('Assets')}

	let(:current_assets)       {create(:current_assets)}
	let(:fixed_assets)         {create(:assets, name: 'Fixed Assets')}

  context 'associations' do
    it do
      is_expected.to belong_to(:header_account)
    end
  end

  context 'validations' do
    it do
      is_expected.to validate_presence_of(:name)
      is_expected.to validate_presence_of(:description)
      is_expected.to validate_uniqueness_of(:name).case_insensitive
    end
  end

  describe '#total_balance_amount' do
		let(:car)       {create(:assets_account, name: 'car')}
		let(:equipment) {create(:assets_account, name: 'equipment')}
		let(:params)    {{transaction_type: 'debit'}}

    before do
      DebitAccount.call(transaction_params: params.merge(amount: '200.0', source_account_id: car.id))
      DebitAccount.call(transaction_params: params.merge(amount: '200.0', source_account_id: equipment.id))
    end

    it 'returns total balance amount' do
      expect(current_assets.total_balance_amount).to eql(400.0)
    end

    context 'when sub account has no categories' do
      it 'returns 0.0' do
        expect(fixed_assets.total_balance_amount).to eql(0.0)
      end
    end
  end
end