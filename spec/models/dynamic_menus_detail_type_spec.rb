require 'rails_helper'

RSpec.describe DynamicMenusDetailType do
  context 'associations' do
    it do
      is_expected.to belong_to(:detail_type)
      is_expected.to belong_to(:dynamic_menu)
    end
  end
end