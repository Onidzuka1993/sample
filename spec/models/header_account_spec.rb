require 'rails_helper'

RSpec.describe HeaderAccount do
  context 'associations' do
    it do
      is_expected.to have_many(:category_types)
    end
  end

  context 'validations' do
    it do
      is_expected.to validate_presence_of(:name)
      is_expected.to validate_presence_of(:description)
      is_expected.to validate_uniqueness_of(:name).case_insensitive
    end
  end

  describe '#total_balance_amount' do
    let(:header_account) {HeaderAccount.find_by_name!('Assets')}
		let(:car)            {create(:assets_account, name: 'Car')}
		let(:building)       {create(:assets_account, name: 'Building')}
		let(:propane)        {create(:assets_account, name: 'Propane')}
		let(:params)         {{transaction_type: 'debit'}}

    before do
      DebitAccount.call(transaction_params: params.merge(amount: '200.0', source_account_id: car.id))
      DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: building.id))
      DebitAccount.call(transaction_params: params.merge(amount: '300.0', source_account_id: propane.id))
    end

    it 'returns total balance' do
      expect(header_account.total_balance_amount).to eql(600.0)
    end

    context 'when header account has no sub accounts' do
      let(:equity) {HeaderAccount.find_by_name('Equity')}

      it 'return 0.0' do
        expect(equity.total_balance_amount).to eql(0.0)
      end
    end
  end
end