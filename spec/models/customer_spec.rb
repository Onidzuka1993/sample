require 'rails_helper'

RSpec.describe Customer do
	context 'validations' do
		it 'validates presence of' do
			is_expected.to validate_presence_of(:name)
			is_expected.to validate_presence_of(:phone_number)
			is_expected.to validate_presence_of(:email)
			is_expected.to validate_presence_of(:address)
		end

		it 'validates uniqueness of' do
			is_expected.to validate_uniqueness_of(:email).case_insensitive
		end
	end
end