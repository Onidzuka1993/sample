require 'rails_helper'

RSpec.describe Folder do
	let(:cash)	 {Account.find_by_name('Cash')}
	let(:car)    {create(:assets_account)}
	let(:folder) {create(:folder)}

	before do |example|
		if example.metadata[:run_before]
			create_list(:transaction, 2, folder_id: folder.id, transaction_type: 'debit', source_account_id: cash.id, amount: 100)
			create_list(:transaction, 2, folder_id: folder.id, transaction_type: 'credit', target_account_id: car.id, amount: 100)
		end
	end

	context 'associations' do
		it 'has many' do
			is_expected.to have_many(:journal_transactions)
		end

		it 'belongs to' do
			is_expected.to belong_to(:vendor)
		end
	end

	context 'nested attributes' do
		it 'accepts nested attributes' do
			is_expected.to accept_nested_attributes_for(:journal_transactions).allow_destroy(true)
		end
	end

	context 'validations' do
		it 'validates presence of' do
			is_expected.to validate_presence_of(:happened_at)
			is_expected.to validate_presence_of(:status)
		end

		it 'validates inclusion of' do
			statuses = %w(created failed)
			is_expected.to validate_inclusion_of(:status).in_array(statuses)

			folder_types = %w(transfer deposit expense journal_entry check)
			is_expected.to validate_inclusion_of(:folder_type).in_array(folder_types)
		end
	end

	describe '#amount' do
		context 'when transfer' do
			before do
				subject.folder_type = 'transfer'
				expect(subject).to receive(:transfer_amount)
			end

			it 'calls #transfer_amount' do
				subject.amount
			end
		end

		context 'when expense' do
			before do
				subject.folder_type = 'expense'
				expect(subject).to receive(:expense_amount)
			end

			it 'calls #expense_amount' do
				subject.amount
			end
		end

		context 'when check' do
			before do
				subject.folder_type = 'check'
				expect(subject).to receive(:check_amount)
			end

			it 'calls #check_amount' do
				subject.amount
			end
		end

		context 'when journal entry' do
			before do
				subject.folder_type = 'journal_entry'
				expect(subject).to receive(:journal_entry_amount)
			end

			it 'calls #journal_entry_amount' do
				subject.amount
			end
		end
	end

	describe '#transfer_amount' do
		let(:transfer) {create(:transfer_folder)}

		before do
			create(:transaction, transaction_type: 'transfer', amount: 100, folder_id: transfer.id, source_account_id: cash.id, target_account_id: car.id)
		end

		it 'returns transfer amount' do
			expect(transfer.transfer_amount).to eql(100.0)
		end
	end

		describe '#expenses_amount', :run_before do
		it 'returns sum of expenses' do
			expect(folder.expense_amount).to eql(200.0)
		end
	end

	describe '#checks_amount', :run_before do
		it 'returns sum of checks amount' do
			expect(folder.check_amount).to eql(200.0)
		end
	end

	describe '#journal_entry_amount' do
		it 'returns subtraction of debits and credit of journal entry' do
			expect(folder.journal_entry_amount).to eql(0.0)
		end
	end
end