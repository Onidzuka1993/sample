require 'rails_helper'

RSpec.describe Transaction do
  let(:debit_transaction)    {build(:transaction, source_account_id: '10')}
  let(:credit_transaction)   {build(:transaction, target_account_id: '20', transaction_type: 'credit')}
  let(:transfer_transaction) {build(:transaction, transaction_type: 'transfer')}

	let(:folder) 			  		   {create(:folder, folder_type: 'check')}

	context 'callbacks' do
		let(:debit)  {create(:transaction, transaction_type: 'debit', source_account_id: cash.id, amount: 200, folder_id: folder.id)}
		let(:credit) {create(:transaction, transaction_type: 'credit', target_account_id: jeep.id, amount: 200, folder_id: folder.id) }

		let(:cash)   {Account.find_by_name('Cash')}
		let(:jeep)	 {create(:assets_account, name: 'Jeep')}

		before do
			cash.update_attribute(:total_balance_amount, 200.0)
			jeep.update_attribute(:total_balance_amount, 200.0)
		end

		describe '#reverse_transaction' do
			it 'reverses debit transaction' do
				expect{debit.destroy}.to change{cash.reload.total_balance_amount}.from(200.0).to(400)
			end

			it 'reverses credit transaction' do
				expect{credit.destroy}.to change{jeep.reload.total_balance_amount}.from(200.0).to(0.0)
			end
		end
	end

  context 'associations' do
		it 'belongs to' do
      is_expected.to belong_to(:source_account)
      is_expected.to belong_to(:target_account)
      is_expected.to belong_to(:journal_transaction)
		end
	end

  context 'validations' do
		it 'validates presence of' do
      is_expected.to validate_presence_of(:status)
      is_expected.to validate_presence_of(:transaction_type)
      is_expected.to validate_presence_of(:amount)
		end

		it 'validates numericality of' do
      is_expected.to validate_numericality_of(:amount)
		end

		it 'validates inclusion of' do
      statuses 					= %w(created completed)
      transaction_types = %w(credit debit transfer)

      is_expected.to validate_inclusion_of(:status).in_array(statuses)
      is_expected.to validate_inclusion_of(:transaction_type).in_array(transaction_types)
		end

    context 'when transaction type is debit' do
      before {allow(subject).to receive(:debit?).and_return(true)}

      it 'validates presence of ' do
        is_expected.to validate_presence_of(:source_account_id)
        is_expected.to validate_numericality_of(:source_account_id)
			end

      context 'when is not debit' do
        before {allow(subject).to receive(:debit?).and_return(false)}

        it 'does not validate presence of source_account_id' do
          is_expected.to_not validate_presence_of(:source_account_id)
				end

				it 'does not validate numericality of source_account_id' do
          is_expected.to_not validate_numericality_of(:source_account_id)
				end
      end
    end

    context 'when transaction type is credit' do
      before {allow(subject).to receive(:credit?).and_return(true)}

      it 'validates presence of target_account_id' do
        is_expected.to validate_presence_of(:target_account_id)
			end

			it 'validates numericality of target_account_id' do
        is_expected.to validate_numericality_of(:target_account_id)
			end

      context 'when is not credit' do
        before {allow(subject).to receive(:credit?).and_return(false)}

        it 'does not validate presence of target_account_id' do
          is_expected.to_not validate_presence_of(:target_account_id)
				end

				it 'does not validate numericality of target_account_id' do
          is_expected.to_not validate_numericality_of(:target_account_id)
				end
      end
    end

    context 'when transaction type is transfer' do
      before {allow(subject).to receive(:transfer?).and_return(true)}

      it 'validates presence of' do
        is_expected.to validate_presence_of(:source_account_id)
        is_expected.to validate_presence_of(:target_account_id)
			end

			it 'validates numericality of' do
        is_expected.to validate_numericality_of(:source_account_id)
        is_expected.to validate_numericality_of(:target_account_id)
			end

      context 'when is not transfer' do
        before {allow(subject).to receive(:transfer?).and_return(false)}

        it 'does not validate presence of' do
          is_expected.to_not validate_presence_of(:source_account_id)
          is_expected.to_not validate_presence_of(:source_account_id)
				end

				it 'does not valite numericality of' do
          is_expected.to_not validate_numericality_of(:source_account_id)
          is_expected.to_not validate_numericality_of(:target_account_id)
				end
      end
    end
  end

  describe '#debit?' do
    it 'returns true if transaction type is debit' do
      expect(debit_transaction.debit?).to be true
    end
  end

  describe '#credit?' do
    it 'returns true if transaction type is credit' do
      expect(credit_transaction.credit?).to be true
    end
  end

  describe '#transfer?' do
    it 'returns true if transaction type is transfer' do
      expect(transfer_transaction.transfer?).to be true
    end
	end

	describe '#account_id' do
		context 'when debit transaction' do
			it 'returns source_account_id' do
				expect(debit_transaction.account_id).to eql(10)
			end

		end

		context 'when credit transaction' do
			it 'returns target_account_id' do
				expect(credit_transaction.account_id).to eql(20)
			end
		end

		context 'when transfer transaction' do
			it 'raises error' do
				expect{transfer_transaction.account_id}.to raise_error(RuntimeError)
			end
		end
	end
end