require 'rails_helper'

RSpec.describe DynamicMenu do
  let(:header_account) {HeaderAccount.find_by_name!('Assets')}
  let(:category_type)    {create(:category_type, header_account: header_account)}
  let(:detail_type1)   {create(:detail_type, category_type: category_type)}
  let(:detail_type2)   {create(:detail_type, name: 'test', category_type: category_type)}

  context 'associations' do
    it do
      is_expected.to have_many(:dynamic_menus_detail_types).dependent(:destroy)
      is_expected.to have_many(:detail_types).through(:dynamic_menus_detail_types)
    end
  end

  context 'validations' do
    before do
      create(:dynamic_menu, from_list: [detail_type1.name], to_list: [detail_type2.name], detail_type_ids: [detail_type1.id])
    end

    it do
      is_expected.to validate_presence_of(:name)
      is_expected.to validate_presence_of(:description)
      is_expected.to validate_presence_of(:from_list)
      is_expected.to validate_presence_of(:to_list)
      is_expected.to validate_presence_of(:detail_type_ids)

      is_expected.to validate_uniqueness_of(:name).case_insensitive
    end
  end
end