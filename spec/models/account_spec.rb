require 'rails_helper'

RSpec.describe Account do
	let(:folder)         {create(:folder)}
  let(:header_account) {HeaderAccount.find_by_name('Assets')}
  let(:category_type)  {create(:current_assets)}
  let(:detail_type)    {create(:other_current_assets)}
  let(:account)        {create(:assets_account)}

  let(:transaction)    {create(:transaction, source_account_id: account.id, folder_id: folder.id)}

  context 'associations' do
    it 'belongs to' do
      is_expected.to belong_to(:detail_type)
		end

		it 'has many' do
      is_expected.to have_many(:source_transactions)
      is_expected.to have_many(:source_transactions)
		end
  end

  context 'validations' do
    it 'validates presence of' do
      is_expected.to validate_presence_of(:name)
      is_expected.to validate_presence_of(:description)
      is_expected.to validate_presence_of(:total_balance_amount)
		end

		it 'validates uniqueness of' do
      is_expected.to validate_uniqueness_of(:name).case_insensitive
		end

		it 'validates numericality of' do
			is_expected.to validate_numericality_of(:total_balance_amount)
		end
  end

  context 'delegations' do
    it do
      is_expected.to delegate_method(:header_account_id).to(:detail_type)
      is_expected.to delegate_method(:category_type_id).to(:detail_type)
    end
  end

  describe '#total_balance_amount' do
    it "shows account's total balance" do
      expect(subject.total_balance_amount).to eql(0.0)
    end

    context 'when balance 1000.0' do
      before {account.update_attribute(:total_balance_amount, 1000.0)}

      it "shows account's total balance" do
        expect(account.total_balance_amount).to eql(1000.0)
			end
    end
	end
end