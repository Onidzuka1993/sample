FactoryGirl.define do
	factory :detail_type do
		name 'detail type test name'
		description 'detail type test description'
	end

  factory :other_long_term_liabilities, class: DetailType do
    name 'other long term liabilities'
    description 'other long term liabilities'
		initialize_with { DetailType.find_or_create_by(name: 'other long term liabilities') }
    association :category_type, factory: :long_term_liabilities
	end

	factory :other_current_assets, class: DetailType do
		name 'other current assets'
		description 'other current assets'
		initialize_with { DetailType.find_or_create_by(name: 'other current assets') }
		association :category_type, factory: :current_assets
	end

	factory :equipment_rental, class: DetailType do
		name 'equipment rental'
		description 'equipment rental'
		initialize_with { DetailType.find_or_create_by(name: 'equipment rental') }
		association :category_type, factory: :expenses
	end

	factory :sales_of_product_income, class: DetailType do
		name 'sales of product income'
		description 'sales of product income'
		initialize_with { DetailType.find_or_create_by(name: 'sales of product income') }
		association :category_type, factory: :income
	end
end