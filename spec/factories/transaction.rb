FactoryGirl.define do
  factory :transaction do
    status           'completed'
    transaction_type 'debit'
    amount           10000.0
	end
end