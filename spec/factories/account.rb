FactoryGirl.define do
  factory :assets_account, class: Account do
    name 'assets account'
    description 'current assets account'
		association :detail_type, factory: :other_current_assets
	end

	factory :expense_account, class: Account do
		name 'expense account'
		description 'expense account'
		association :detail_type, factory: :equipment_rental
	end

	factory :income_account, class: Account do
		name 'income account'
		description 'income account'
		association :detail_type, factory: :sales_of_product_income
	end
end