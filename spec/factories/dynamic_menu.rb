FactoryGirl.define do
  factory :dynamic_menu do
    name 'menu test name'
    description 'menu test description'
  end
end