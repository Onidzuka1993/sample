FactoryGirl.define do
	factory :category_type do
		name 'category type test name'
		description 'category type test description'
	end

  factory :long_term_liabilities, class: CategoryType do
    name 'Long Term Liabilities'
    description 'Long Term Liabilities'
		initialize_with { CategoryType.find_or_create_by(name: 'Long Term Liabilities') }
    before(:create) {|category_type| category_type.header_account = HeaderAccount.find_by_name('Liabilities')}
	end

	factory :current_assets, class: CategoryType do
		name 'Current Assets'
		description 'Current Assets'
		initialize_with { CategoryType.find_or_create_by(name: 'Current Assets') }
		before(:create) {|category_type| category_type.header_account = HeaderAccount.find_by_name('Assets')}
	end

	factory :expenses, class: CategoryType do
		name 'Expenses'
		description 'Expenses'
		initialize_with { CategoryType.find_or_create_by(name: 'Expenses') }
		before(:create) {|category_type| category_type.header_account = HeaderAccount.find_by_name('Expense')}
	end

	factory :income, class: CategoryType do
		name 'Income'
		description 'Income'
		initialize_with { CategoryType.find_or_create_by(name: 'Income') }
		before(:create) {|category_type| category_type.header_account = HeaderAccount.find_by_name('Income') }
	end

	factory :assets, class:  CategoryType do
		name 'Assets'
		description 'Assets'
		before(:create) {|category_type| category_type.header_account = HeaderAccount.find_by_name('Assets') }
	end
end