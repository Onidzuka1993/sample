FactoryGirl.define do
	factory :folder do
		status 			'created'
		folder_type 'deposit'
		happened_at Date.current
	end

	factory :transfer_folder, class: Folder do
		status 			'created'
		folder_type 'transfer'
		happened_at Date.current
	end
end