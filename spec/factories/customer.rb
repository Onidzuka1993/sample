FactoryGirl.define do
	factory :customer do
		name 				 'customer test name'
		email 			 'customer@gmail.com'
		phone_number '7196581111'
		address 		 'customer test address'
	end
end