FactoryGirl.define do
	factory :vendor do
		name 				 'vendor test name'
		email 			 'vendor@gmail.com'
		phone_number '7196581111'
		address 		 'vendor test address'
	end
end