require 'rails_helper'

RSpec.describe TransferFunds do
  let(:cash) {Account.find_by_name('Cash')}
  let(:car)  {create(:assets_account, name: 'Car')}

	let(:transfer_params) {{amount: 100.0, source_account_id: cash.id, target_account_id: car.id, happened_at: Date.current, memo: 'memo'}}

  describe '.call' do
    before { cash.update_attribute(:total_balance_amount, 1000.0) }

		after invalid_transfer: true do
			expect {TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: cash.id))}.to_not change(Transaction, :count)
			expect {TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: cash.id))}.to_not change(Folder, :count)
		end

		it "creates folder'" do
			expect {TransferFunds.call(transfer_params: transfer_params)}.to change{Folder.count}.from(0).to(1)

			folder = Folder.last
			expect(folder).to have_attributes({folder_type: 'transfer', memo: 'memo', happened_at: Date.current})
		end

		it 'puts transaction into a folder' do
			TransferFunds.call(transfer_params: transfer_params)

			expect(Folder.last.journal_transactions.count).to eql(1)
		end

    it 'creates transfer transaction' do
			expect {TransferFunds.call(transfer_params: transfer_params)}.to change{Transaction.count}.from(0).to(1)

      expect(Transaction.last.transaction_type).to eql('transfer')
      expect(Transaction.last.status).to           eql('completed')
		end

		it 'returns true on #success?' do
      use_case = TransferFunds.call(transfer_params: transfer_params)

			expect(use_case.success?).to be true
		end

		it "decreases source account's balance" do
      expect {TransferFunds.call(transfer_params: transfer_params)}.to change{cash.reload.total_balance_amount}.from(1000.0).to(900.0)
		end

		it "increases target account's balance" do
      expect {TransferFunds.call(transfer_params: transfer_params)}.to change{car.reload.total_balance_amount}.from(0.0).to(100.0)
		end

    context 'when self selection transfer' do
      it 'does not change balance', invalid_transfer: true do
        TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: cash.id))
			end

			it 'returns false on #success?' do
        use_case = TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: cash.id))

        expect(use_case.success?).to eql(false)
			end

			it 'returns error message' do
        use_case = TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: cash.id))

        expect(use_case.errors).to  eql(["You can't transfer to the same account"])
			end
		end

		context 'when accounts from different categories' do
			let(:equity_account) {Account.find_by_name!('Opening Balance')}

			it 'returns false on #success?' do
				use_case = TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: equity_account.id))

				expect(use_case.success?).to be false
			end

			it 'returns error message' do
        use_case = TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: equity_account.id))

        expect(use_case.errors).to eql(["You can't transfer between accounts from different categories"])
			end

			it 'does not transfer', invalid_transfer: true do
				TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: equity_account.id))
			end
		end
		
		context 'when invalid date' do
			it 'returns error message' do
				use_case = TransferFunds.call(transfer_params: transfer_params.merge(happened_at: (Date.current + 1.day).to_s))

				expect(use_case.success?).to be false
				expect(use_case.errors).to match_array(['Happened at cannot be after today'])
			end

			it 'does not transfer', invalid_transfer: true do
				TransferFunds.call(transfer_params: transfer_params.merge(happened_at: (Date.current + 1.day).to_s))
			end
		end

		context 'when no accounts specified' do
			context 'when source account is blank' do
				it 'returns error message' do
					use_case = TransferFunds.call(transfer_params: transfer_params.merge(source_account_id: ''))

					expect(use_case.success?).to be false
					expect(use_case.errors).to match_array(['You must specify transfer from account and transfer to account'])
				end

				it 'does not transfer', invalid_transfer: true do
					TransferFunds.call(transfer_params: transfer_params.merge(source_account_id: ''))
				end
			end

			context 'when target account is blank' do
				it 'returns error message' do
					use_case = TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: ''))

					expect(use_case.success?).to be false
					expect(use_case.errors).to match_array(['You must specify transfer from account and transfer to account'])
				end

				it 'does not transfer', invalid_transfer: true do
					TransferFunds.call(transfer_params: transfer_params.merge(target_account_id: ''))
				end
			end
		end

    context 'when invalid params' do
			context 'for transaction' do
				it 'returns errors' do
					use_case = TransferFunds.call(transfer_params: transfer_params.merge(amount: ''))

					expect(use_case.success?).to     eql(false)
					expect(use_case.errors.count).to eql(2)
				end

				it 'does not transfer', invalid_transfer: true do
					TransferFunds.call(ransfer_params: transfer_params.merge(amount: ''))
				end
			end

			context 'for folder' do
				it 'returns errors' do
					use_case = TransferFunds.call(transfer_params: transfer_params.merge(happened_at: ''))

					expect(use_case.success?).to 		 eql(false)
					expect(use_case.errors.count).to eql(1)
				end

				it 'does not transfer', invalid_transfer: true do
					TransferFunds.call(transfer_params: transfer_params.merge(happened_at: ''))
				end
			end
    end
  end
end