require 'rails_helper'

RSpec.describe GetFolders do
	describe '.call' do
		let!(:folder1) {create(:folder, created_at: 30.days.ago.end_of_day)}
		let!(:folder2) {create(:folder, created_at: 60.days.ago.end_of_day)}
		let!(:folder3) {create(:folder, created_at: 90.days.ago.end_of_day)}
		let!(:folder4) {create(:folder, created_at: 99.days.ago.end_of_day)}

		it 'returns folders for the past 30 days' do
			use_case = GetFolders.call(filter_by: 'past_thirty_days')

			expect(use_case.success?).to 			be true

			expect(use_case.folders.count).to eql(1)
			expect(use_case.title).to 				eql('Transactions for past 30 days')
			expect(use_case.folders).to 			include(folder1)
		end

		it 'returns folders for the past 60 days' do
			use_case = GetFolders.call(filter_by: 'past_sixty_days')

			expect(use_case.success?).to 			be true

			expect(use_case.folders.count).to eql(2)
			expect(use_case.title).to 				eql('Transactions for past 60 days')
			expect(use_case.folders).to 			include(folder1, folder2)
		end

		it 'returns folders for the past 90 days' do
			use_case = GetFolders.call(filter_by: 'past_ninety_days')

			expect(use_case.success?).to 			be true

			expect(use_case.folders.count).to eql(3)
			expect(use_case.title).to 				eql('Transactions for past 90 days')
			expect(use_case.folders).to 			include(folder1, folder2, folder3)
		end

		it 'returns all folders' do
			use_case = GetFolders.call(filter_by: 'all')

			expect(use_case.success?).to 			be true

			expect(use_case.title).to 				eql('All transactions')
			expect(use_case.folders.count).to eql(4)
			expect(use_case.folders).to 			include(folder1, folder2, folder3, folder4)
		end

		context 'when undefined method' do
			it 'returns past 30 days folders' do
				use_case = GetFolders.call(filter_by: 'invalid_method')

				expect(use_case.success?).to 			be true

				expect(use_case.folders.count).to eql(1)
				expect(use_case.folders).to 			include(folder1)
			end
		end

		context 'when no folder found' do
			before {Folder.destroy_all}

			it 'returns an error message' do
				use_case = GetFolders.call(filter_by: 'past_thirty_days')

				expect(use_case.success?).to be false
				expect(use_case.message).to  eql('Transactions for past thirty days not found.')
			end
		end
	end
end