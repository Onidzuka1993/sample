require 'rails_helper'

RSpec.describe SortAccount do
  describe '.call' do
    context 'when params are not given' do
      before {expect(Account).to receive(:includes).with(detail_type: :category_type)}

      it 'it receives Account.all method' do
        SortAccount.call
      end
    end

    context 'when given column and direction' do
      before {expect(Account).to receive(:order).with('name desc')}

      it 'sorts by column and direction' do
        SortAccount.call(column: 'name', direction: 'desc')
      end
    end

    context 'when given sub_account' do
      before do
        account = double('account')

        expect(Account).to receive(:includes).with(category: :sub_account).and_return(account)
        expect(account).to receive(:order).with('sub_accounts.name')
      end

      it 'sorts by sub account name' do
        SortAccount.call(column: 'sub_account', direction: 'desc')
      end
    end

    context 'when given category' do
      before do
        account = double('account')

        expect(Account).to receive(:includes).with(:category).and_return(account)
        expect(account).to receive(:order).with('categories.name')
      end

      it 'sorts by category name' do
        SortAccount.call(column: 'category', direction: 'desc')
      end
    end

    context 'when given total_balance' do
      before do
        account = double('account')

        expect(Account).to receive(:includes).with(:balances).and_return(account)
        expect(account).to receive(:sort_by)
      end

      it 'sorts by total balance amount' do
        SortAccount.call(column: 'total_balance', direction: 'desc')
      end
    end
  end
end