require 'rails_helper'

RSpec.describe DebitAccount do
	let(:cash)   {Account.find_by_name('Cash')}

	let(:params) {{amount: '1000.0', transaction_type: 'debit', source_account_id: cash.id}}

  describe '.call' do
		before {DebitAccount.call(transaction_params: params)}

    it 'returns true on #success?' do
      use_case = DebitAccount.call(transaction_params: params)

      expect(use_case.success?).to be true
		end
  end
end