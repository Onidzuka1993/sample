require 'rails_helper'

RSpec.describe ValidateJournalEntryInputs do
	let(:transaction) {{Account: '', Debits: '', Credits: '', Description: ''}}

	describe '.call' do
		it 'creates data structure from input data' do
			transactions  = [
					transaction.merge(Account: '1', Debits: '200'),
					transaction.merge(Account: '2', Credits: '100'), transaction.merge(Account: '3', Credits: '100')
			]

			inputs   = {happened_at: "#{Date.current.end_of_day}", memo: 'memo', journal_entry_table: JSON.generate(transactions)}
			use_case = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expected_data = {
					happened_at: "#{Date.current.end_of_day}",
					memo: 'memo',
					debit_transactions: [
							{Account: '1', Debits: '200', Credits: '', Description: ''}
					],
					credit_transactions: [
							{Account: '2', Debits: '', Credits: '100', Description: ''},
							{Account: '3', Debits: '', Credits: '100', Description: ''}
					]}

			expect(use_case.success?).to		 				 be true
			expect(use_case.journal_entry_params).to eql(expected_data)
		end

		it 'checks presence of date' do
			inputs 	 = {happened_at: '', journal_entry_table: JSON.generate({})}
			use_case = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expect(use_case.success?).to 	be false
			expect(use_case.message).to 	eql({error: 'You must specify a date.'})
		end

		it 'checks presence of accounts' do
			transactions = [transaction.merge(Debits: '100'), transaction.merge(Credits: '100')]

			inputs = {happened_at: "#{Date.current}", memo: 'memo', journal_entry_table: JSON.generate(transactions)}
			use_case = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expect(use_case.success?).to be false
			expect(use_case.message).to  eql({error: 'You must select an account for each split line with an amount.'})
		end

		it 'validates date' do
			inputs 	 = {happened_at: 'invalid_date', journal_entry_table: JSON.generate({})}
			use_case = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expect(use_case.success?).to 	be false
			expect(use_case.message).to 	eql({error: 'You must specify a valid date'})
		end

		it 'removes empty transactions' do
			transactions = [transaction.merge(Account: ''), transaction.merge(Debits: '1000')]

			inputs 			 = {happened_at: "#{Time.now}", journal_entry_table: JSON.generate(transactions)}
			use_case		 = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expect(use_case.success?).to 		 			 be false
			expect(use_case.transactions.count).to eql(1)
			expect(use_case.message).to 					 eql({error: 'You must fill out at least two detail lines.'})
		end

		it 'checks presence of debits and credits' do
			transactions = [transaction.merge(Account: '1'), transaction.merge(Account: '2')]

			inputs   		 = {happened_at: "#{Date.current}", memo: 'memo', journal_entry_table: JSON.generate(transactions)}
			use_case 		 = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expect(use_case.success?).to be false
			expect(use_case.message).to  eql({error: 'You must specify either credits or debits for each detail line.'})
		end

		it 'checks debits and credits inputs' do
			transactions = [
			 		transaction.merge(Account: '1', Debits: '100', Credits: '100'),
			 		transaction.merge(Account: '2', Debits: '100', Credits: '100')
			]

			inputs   = {happened_at: "#{Time.now}", journal_entry_table: JSON.generate(transactions)}
			use_case = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expect(use_case.success?).to be false
			expect(use_case.message).to  eql({error: 'You must specify either credits or debits for each detail line.'})
		end

		it 'checks equality of credits and debits' do
			transactions = [transaction.merge(Account: '1', Debits: '200'), transaction.merge(Account: '2', Credits: '100')]

			inputs   = {happened_at: "#{Time.now}", journal_entry_table: JSON.generate(transactions)}
			use_case = ValidateJournalEntryInputs.call(journal_entry_params: inputs)

			expect(use_case.success?).to be false
			expect(use_case.message).to  eql({error: 'Please balance debits and credits.'})
		end
	end
end