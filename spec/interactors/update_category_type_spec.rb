require 'rails_helper'

RSpec.describe UpdateCategoryType do
  describe '.call' do
		let(:current_assets)       {create(:current_assets)}

    let(:category_type_params) { {name: 'test name', description: 'test desc' } }
    let(:invalid_params)       { {name: '', description: ''} }

    it 'updates category type' do
      use_case = UpdateCategoryType.call(category_type_id: current_assets.id, category_type_params: category_type_params)

      expect(use_case.success?).to be true
      expect(current_assets.reload).to have_attributes(category_type_params)
    end

    context 'when invalid params given' do
      it 'does not update' do
        use_case = UpdateCategoryType.call(category_type_id: current_assets.id, category_type_params: invalid_params)

        expect(use_case.success?).to be false
        expect(use_case.category_type.errors.count).to eql(2)
      end
    end
  end
end