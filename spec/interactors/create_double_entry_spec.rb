require 'rails_helper'

RSpec.describe CreateDoubleEntry do
	let(:cash)				 		 		  {Account.find_by_name('Cash')}
	let(:opening_balance)	 		  {Account.find_by_name('Opening Balance')}

	let(:debit_transactions)    {[{Account: cash.id.to_s, Debits: '100', Credits: '', Description: 'debit'}]}
	let(:credit_transactions)   {[{Account: opening_balance.id.to_s, Debits: '', Credits: '100', Description: 'credit'}]}

	let(:journal_entry_params)  {{
			happened_at: Date.current.end_of_day,
			memo: 'memo',
			debit_transactions: debit_transactions,
			credit_transactions: credit_transactions
	}}

	describe 'call' do
		it 'returns true #success?' do
			use_case = CreateDoubleEntry.call(journal_entry_params: journal_entry_params)

			expect(use_case.success?).to be true
		end

		it 'creates folder with double entry type' do
			expect{CreateDoubleEntry.call(journal_entry_params: journal_entry_params)}.to change{Folder.count}.from(0).to(1)
			expect(Folder.first).to have_attributes(memo: 'memo', folder_type: 'journal_entry')
		end

		it 'creates debit and debit transactions' do
			expect{CreateDoubleEntry.call(journal_entry_params: journal_entry_params)}.to change{Transaction.count}.from(0).to(2)

			expected_attributes = {status: 'completed', amount: BigDecimal(100), folder_id: Folder.first.id}

			expect(Transaction.first).to have_attributes(expected_attributes.merge(
					source_account_id: cash.id,
					target_account_id: nil,
					transaction_type: 'debit',
					description: 'debit'
			))

			expect(Transaction.last).to  have_attributes(expected_attributes.merge(
					source_account_id: nil,
					target_account_id: opening_balance.id,
					transaction_type: 'credit',
					description: 'credit'
			))
		end

		it 'increases credit account' do
			expect{CreateDoubleEntry.call(journal_entry_params: journal_entry_params)}.to change{cash.reload.total_balance_amount}.from(0.0).to(100.0)
		end

		it 'increases debit account' do
			expect{CreateDoubleEntry.call(journal_entry_params: journal_entry_params)}.to change{opening_balance.reload.total_balance_amount}.from(0.0).to(100.0)
		end

		context 'when failure' do
			before do
				invalid_transactions = [{Account: '', Debits: '100', Credits: '', Description: 'debit'}]
				journal_entry_params[:debit_transactions] = invalid_transactions
			end

			it 'returns an error message' do
				use_case = CreateDoubleEntry.call(journal_entry_params: journal_entry_params)

				expect(use_case.message).to eql(error: 'fail')
			end

			it "sets status 'failed' to folder" do
				expect{CreateDoubleEntry.call(journal_entry_params: journal_entry_params)}.to change{Folder.count}.from(0).to(1)
				expect(Folder.last.status).to     eql('failed')
				expect(Folder.last.reason).not_to be nil
			end

			it 'does not change Transaction table' do
				expect{CreateDoubleEntry.call(journal_entry_params: journal_entry_params)}.to_not change(Transaction, :count)
			end
		end
	end
end