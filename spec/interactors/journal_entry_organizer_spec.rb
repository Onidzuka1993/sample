require 'rails_helper'

RSpec.describe JournalEntryOrganizer do
	let(:transaction)     {{Account: '', Debits: '', Credits: '', Description: ''}}
	let(:cash_account)    {Account.find_by_name('Cash')}
	let(:opening_account) {Account.find_by_name('Opening Balance')}

	describe '.call' do
		it 'runs ValidateJournalEntryInputs and CreateDoubleEntry Interactors' do
			transactions  = [
					transaction.merge(Account: cash_account.id.to_s, Debits: '100'),
					transaction.merge(Account: opening_account.id.to_s, Credits: '100')
			]

			inputs   = {happened_at: "#{Date.current.end_of_day}", memo: 'memo', journal_entry_table: JSON.generate(transactions)}
			use_case = JournalEntryOrganizer.call(journal_entry_params: inputs)

			expect(use_case.success?).to eql(true)
		end
	end
end