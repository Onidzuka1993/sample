require 'rails_helper'

RSpec.describe CreateExpense do
	include_context 'shared expense data'

	describe '.call' do
		before do |example|
			unless example.metadata[:skip_before]
				@use_case = CreateExpense.call(expense_params: expense_params)
			end
		end

		it_behaves_like 'expense examples'

		it 'creates folder with transactions', :skip_before do
			expected_attributes = {vendor_id: vendor.id, happened_at: Date.current, payment_method: 'Cash', memo: 'memo', folder_type: 'expense'}

			expect{CreateExpense.call(expense_params: expense_params)}.to change{Folder.count}.from(0).to(1)
			expect(Folder.first).to have_attributes(expected_attributes)
			expect(Folder.first.journal_transactions.count).to eql(4)
		end

		it 'creates transactions', :skip_before do
			expect{CreateExpense.call(expense_params: expense_params)}.to change{Transaction.count}.from(0).to(4)
		end

		it 'decreases select account' do
			expect(Account.find_by_name('Cash').total_balance_amount).to eql(-1200.0)
		end

		it 'increases expense accounts' do
			expect(rent.reload.total_balance_amount).to eql(200.0)
			expect(wages.reload.total_balance_amount).to eql(1000.0)
		end
	end
end