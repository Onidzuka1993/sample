require 'rails_helper'

RSpec.describe UpdateAccount do
	let(:expense_account)  {create(:equipment_rental)}
  let(:assets_account)   {create(:assets_account)}

  describe '.call' do
    it 'updates account' do
      account_params = {detail_type_id: expense_account.id, name: 'test', description: 'description'}

      use_case = UpdateAccount.call(account_id: assets_account.id, account_params: account_params)

      expect(use_case.success?).to be true
      expect(use_case.account).to  have_attributes(account_params)
      expect(use_case.message).to  eql('Account has successfully been updated.')
    end

    context 'when invalid params' do
      it 'does not update account' do
        account_params = {detail_type_id: '', name: '', description: ''}

        use_case = UpdateAccount.call(account_id: assets_account.id, account_params: account_params)

        expect(use_case.success?).to          be false
        expect(use_case.account.errors.count).to eql(3)
      end
    end
  end
end