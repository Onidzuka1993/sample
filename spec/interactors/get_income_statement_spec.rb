require 'rails_helper'

RSpec.describe GetIncomeStatement do
  let(:income)  {HeaderAccount.find_by_name!('Income')}
  let(:expense) {HeaderAccount.find_by_name!('Expense')}
	let(:params)  {{transaction_type: 'debit'}}

  context 'constant values' do
    it 'checks constant values' do
      expect(GetIncomeStatement::PROFIT_AND_LOSS).to eql(%w(Income Expense))
    end
  end

  describe '.call' do
    before do |example|
      if example.metadata[:run_before]
        DebitAccount.call(transaction_params: params.merge(amount: '300.0', source_account_id: crops.id))
        DebitAccount.call(transaction_params: params.merge(amount: '300.0', source_account_id: vegetables.id))

        DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: rent.id))
        DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: utilities.id))
      end
		end

		after do |example|
			if example.metadata[:run_after]
        total_income  = @use_case.income_statement.find(income.id).category_types.first.detail_types.first.accounts
        total_expense = @use_case.income_statement.find(expense.id).category_types.first.detail_types.first.accounts

        expect(total_income.count).to      eql(2)
        expect(total_income.first.name).to eql('crops')
        expect(total_income.second).to     be_nil

        expect(total_expense.count).to      eql(2)
        expect(total_expense.first.name).to eql('rent')
        expect(total_expense.second).to     be_nil
			end
		end

    context "when filter is set to 'year to date'" do
      let(:crops)      {create(:income_account, name: 'crops', created_at: Time.now.beginning_of_year)}
      let(:vegetables) {create(:income_account, name: 'vegetables', created_at: 2.year.ago)}

      let(:rent)       {create(:expense_account, name: 'rent', created_at: Time.now.beginning_of_year)}
      let(:utilities)  {create(:expense_account, name: 'utilities', created_at: 2.year.ago)}

      it 'returns all accounts from current year to now', :run_before, :run_after do
        @use_case = GetIncomeStatement.call(filter_by: 'year_to_date')

        expect(@use_case.success?).to   be true
        expect(@use_case.date).to       eql("#{Date.current.beginning_of_year} - #{Date.current}")
        expect(@use_case.net_income).to eql(200.0)
      end
    end

    context "when filter is set to 'this_month'" do
      let(:crops)      {create(:income_account, name: 'crops', created_at: Date.current)}
      let(:vegetables) {create(:income_account, name: 'vegetables', created_at: 1.month.ago)}

      let(:rent)       {create(:expense_account, name: 'rent', created_at: Date.current)}
      let(:utilities)  {create(:expense_account, name: 'utilities', created_at: 1.month.ago)}

      it 'returns all accounts from this month', :run_before, :run_after do
        @use_case = GetIncomeStatement.call(filter_by: 'this_month')

        expect(@use_case.success?).to   be true
        expect(@use_case.date).to       eql("#{Date.current.beginning_of_month} - #{Date.current}")
        expect(@use_case.net_income).to eql(200.0)
      end
    end

    context "when filter is set to 'last_month'" do
      let(:crops)      {create(:income_account, name: 'crops', created_at: 1.month.ago)}
      let(:vegetables) {create(:income_account, name: 'vegetables', created_at: 2.month.ago)}

      let(:rent)       {create(:expense_account, name: 'rent', created_at: 1.month.ago)}
      let(:utilities)  {create(:expense_account, name: 'utilities', created_at: 2.month.ago)}

      it 'returns all accounts from last_month', :run_before, :run_after do
        @use_case = GetIncomeStatement.call(filter_by: 'last_month')

        expect(@use_case.success?).to   be true
        expect(@use_case.date).to       eql("#{Date.current.prev_month.beginning_of_month} - #{Date.current.prev_month.end_of_month}")
        expect(@use_case.net_income).to eql(200.0)
      end
    end

    context "when filter is set to 'last_three_months'" do
      let(:crops)      {create(:income_account, name: 'crops', created_at: 2.month.ago)}
      let(:vegetables) {create(:income_account, name: 'vegetables', created_at: 4.month.ago)}

      let(:rent)       {create(:expense_account, name: 'rent', created_at: 2.month.ago)}
      let(:utilities)  {create(:expense_account, name: 'utilities', created_at: 4.month.ago)}

      it 'returns all accounts from last_three_months', :run_before, :run_after do
        @use_case = GetIncomeStatement.call(filter_by: 'last_three_months')

        expect(@use_case.success?).to   be true
        expect(@use_case.date).to       eql("#{Date.current.beginning_of_month - 3.month} - #{Date.current}")
        expect(@use_case.net_income).to eql(200.0)
      end
    end

    context "when filter is set to 'last_year'" do
      let(:crops)      {create(:income_account, name: 'crops', created_at: 1.year.ago)}
      let(:vegetables) {create(:income_account, name: 'vegetables', created_at: Date.current)}

      let(:rent)       {create(:expense_account, name: 'rent', created_at: 1.year.ago)}
      let(:utilities)  {create(:expense_account, name: 'utilities', created_at: Date.current)}

      it 'return all accounts from last_year', :run_before, :run_after do
        @use_case = GetIncomeStatement.call(filter_by: 'last_year')

        expect(@use_case.success?).to   be true
        expect(@use_case.date).to       eql("#{(Date.current - 1.year).beginning_of_year} - #{(Date.current - 1.year).end_of_year}")
        expect(@use_case.net_income).to eql(200.0)
      end
    end

    context "when filter is set to 'custom_date'" do
      let(:crops) {create(:income_account, name: 'crops', created_at: 1.month.ago)}
      let(:rent)  {create(:expense_account, name: 'rent', created_at: 1.week.ago)}

      before do
        DebitAccount.call(transaction_params: params.merge(amount: '300.0', source_account_id: crops.id))
        DebitAccount.call(transaction_params: params.merge(amount: '100.0', source_account_id: rent.id))
      end

      it 'return account by given date range' do
        use_case = GetIncomeStatement.call(filter_by: 'custom_date', start_date: Date.current - 3.weeks, end_date: Date.current)

        expect(use_case.success?).to   be true
        expect(use_case.date).to       eql("#{(Date.current - 3.weeks).to_date} - #{(Date.current).to_date}")
        expect(use_case.net_income).to eql(-100.0)

        total_income  = use_case.income_statement.find(income.id).category_types.first.detail_types.first.accounts rescue nil
        total_expense = use_case.income_statement.find(expense.id).category_types.first.detail_types.first.accounts

        expect(total_income).to             be_nil

        expect(total_expense.count).to      eql(1)
        expect(total_expense.first.name).to eql('rent')
      end

      context 'when invalid dates' do
        it 'returns an error message' do
          use_case = GetIncomeStatement.call(filter_by: 'custom_date', start_date: 'test', end_date: 'test')

          expect(use_case.success?).to be false
          expect(use_case.message).to eql('Invalid dates')
        end
      end

      context 'when invalid dates' do
        it 'returns an error message' do
          use_case = GetIncomeStatement.call(filter_by: 'custom_date', start_date: Date.current, end_date: Date.current - 1.day)

          expect(use_case.success?).to be false
          expect(use_case.message).to eql('Invalid dates')
        end
      end
    end

    context 'when only income' do
			let(:crops) {create(:income_account, name: 'crops', created_at: 1.year.ago)}

      before do
        DebitAccount.call(transaction_params: params.merge(amount: '300.0', source_account_id: crops.id))
      end

      it 'returns net income for expense only' do
        use_case = GetIncomeStatement.call(filter_by: 'last_year')
        expect(use_case.success?).to      be true

        expect(use_case.income_statement.detect {|income_statement| income_statement.name == 'Income'}).not_to be_nil
        expect(use_case.income_statement.detect {|income_statement| income_statement.name == 'Expense'}).to    be_nil

        expect(use_case.net_income).to  eql(300.0)
      end
    end

    context 'when only expense' do
			let(:rent) {create(:expense_account, name: 'rent', created_at: 1.year.ago)}

      before do
        DebitAccount.call(transaction_params: params.merge(amount: '400.0', source_account_id: rent.id))
      end

      it 'returns net income for expense only' do
        use_case = GetIncomeStatement.call(filter_by: 'last_year')
        expect(use_case.success?).to    be true

        expect(use_case.income_statement.detect {|income_statement| income_statement.name == 'Income'}).to      be_nil
        expect(use_case.income_statement.detect {|income_statement| income_statement.name == 'Expense'}).not_to be_nil

        expect(use_case.net_income).to  eql(-400.0)
      end
    end

    context 'when method not found' do
      let(:rent) {create(:expense_account, name: 'rent', created_at: 1.month.ago)}

      before do
        DebitAccount.call(transaction_params: params.merge(amount: '400.0', source_account_id: rent.id))
      end

      it 'calls year_to_date method' do
        use_case = GetIncomeStatement.call(filter_by: 'unknown_message')

        expect(use_case.success?).to be true
        expect(use_case.net_income).to eql(-400.0)
      end
    end

    context 'when accounts not found for this_month' do
      it 'returns error message' do
        use_case = GetIncomeStatement.call(filter_by: 'this_month')

        expect(use_case.success?).to be false
        expect(use_case.message).to  eql('Accounts for this month not found')
      end
    end

    context 'when accounts not found for this_month' do
      it 'returns error message' do
        use_case = GetIncomeStatement.call(filter_by: 'last_month')

        expect(use_case.success?).to be false
        expect(use_case.message).to  eql('Accounts for last month not found')
      end
    end

    context 'when accounts not found for this_month' do
      it 'returns error message' do
        use_case = GetIncomeStatement.call(filter_by: 'last_three_months')

        expect(use_case.success?).to be false
        expect(use_case.message).to  eql('Accounts for last three months not found')
      end
    end
  end
end