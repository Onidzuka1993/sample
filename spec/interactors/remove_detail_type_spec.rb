require 'rails_helper'

RSpec.describe RemoveDetailType do
	let(:other_current_assets) {create(:other_current_assets)}
	
  describe '.call' do
    it 'removes detail type' do
      use_case = RemoveDetailType.call(detail_type_id: other_current_assets.id)

      expect(use_case.success?).to             eql(true)
      expect(DetailType.exists?(other_current_assets.id)).to eql(false)
    end

    context 'when detail type has accounts' do
      before {create(:assets_account)}

      it 'does not remove other_current_assets' do
        use_case = RemoveDetailType.call(detail_type_id: other_current_assets.id)

        expect(use_case.success?).to be false
        expect(use_case.message).to  eql('This detail type has accounts.')
      end
    end
  end
end