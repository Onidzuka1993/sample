require 'rails_helper'

RSpec.describe CreateDebitTransaction do
	let(:cash)   {Account.find_by_name!('Cash')}

	let(:params) {{amount: '1000.0', source_account_id: cash.id}}

  describe '.call' do
		it 'returns true on #success?' do
			use_case = CreateDebitTransaction.call(transaction_params: params)

			expect(use_case.success?).to be true
		end

    it 'creates debit type transaction' do
      expected_attributes = {transaction_type: 'debit', source_account_id: cash.id, status: 'created', amount: BigDecimal.new(1000)}

			expect{@use_case = CreateDebitTransaction.call(transaction_params: params)}.to change{Transaction.count}.from(0).to(1)
      expect(@use_case.transaction).to have_attributes(expected_attributes)
		end

		it 'creates folder'do
			expect {CreateDebitTransaction.call(transaction_params: params)}.to change{Folder.count}.from(0).to(1)
		end

		it 'it saves transaction in folder' do
			CreateDebitTransaction.call(transaction_params: params)

			expect(Folder.first.journal_transactions.count).to eql(1)
		end
  end
end