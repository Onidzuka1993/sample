require 'rails_helper'

RSpec.describe ExecuteDebitTransaction do
	let(:folder)    {create(:folder)}
  let(:equipment) {create(:assets_account)}

  let(:debit_transaction) {create(:transaction, source_account_id: equipment.id, amount: 100.0, folder_id: folder.id)}

  describe '.call' do
    it 'executes transaction' do
      use_case = ExecuteDebitTransaction.call(transaction: debit_transaction)

      expect(use_case.success?).to                      be true

      expect(debit_transaction.reload.status).to        eql('completed')
      expect(equipment.reload.total_balance_amount).to  eql(100.0)
    end
  end
end