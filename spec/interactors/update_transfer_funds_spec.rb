require 'rails_helper'

RSpec.describe UpdateTransferFunds do
	let(:folder) 			 {create(:folder, folder_type: 'transfer')}
	let(:cash)   			 {Account.find_by_name('Cash')}
	let(:car)    			 {create(:assets_account, name: 'Car')}

	let(:cash_on_hand) {create(:assets_account, name: 'Cash on Hand')}
	let(:jeep) 			   {create(:assets_account, name: 'Jeep')}

	describe '#call' do
		before do |example|
			cash.update_attribute(:total_balance_amount, 400.0)
			car.update_attribute(:total_balance_amount, 100.0)

			create(:transaction, amount: 100.0, source_account_id: cash.id, target_account_id: car.id, folder_id: folder.id, transaction_type: 'transfer')

			if example.metadata[:run_before]
				@use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params)
			end
		end

		after invalid_transfer: true do
			expect {UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: cash.id))}.to_not change(Transaction, :count)
			expect {UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: cash.id))}.to_not change(Folder, :count)
		end

		let(:update_params) {{amount: 200.0, source_account_id: cash.id, target_account_id: car.id, happened_at: Date.current, memo: 'memo'}}

		it 'returns true on #success?', :run_before do
			expect(@use_case.success?).to be true
		end

		it 'cash account balance' do
			expect{UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params)}.to change{cash.reload.total_balance_amount}.from(400.0).to(300.0)
		end

		it 'car account balance' do
			expect{UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params)}.to change{car.reload.total_balance_amount}.from(100.0).to(200.0)
		end

		it 'updates folder attributes', :run_before do
			expect(folder.reload).to have_attributes(update_params.slice(:memo, :happened_at).merge(folder_type: 'transfer'))
		end

		it 'updates transaction attributes', :run_before do
			expect(folder.reload.journal_transactions.count).to eql(1)
			expect(folder.reload.journal_transactions.first).to have_attributes({amount: 200.0, source_account_id: cash.id, target_account_id: car.id, folder_id: folder.id})
		end

		context 'when accounts changed' do
			before do
				cash.update_attribute(:total_balance_amount, -100.0)
				car.update_attribute(:total_balance_amount, 100.0)
			end

			let(:update_params) {{amount: '400.0', source_account_id: cash_on_hand.id, target_account_id: jeep.id, happened_at: Date.current, memo: 'memo'}}

			it 'reverts cash account balance' do
				expect{UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params)}.to change{cash.reload.total_balance_amount}.from(-100.0).to(0.0)
			end

			it 'reverts car account balance' do
				expect{UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params)}.to change{car.reload.total_balance_amount}.from(100.0).to(0.0)
			end

			it 'creates transfer from cash_on_hand account' do
				expect{UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params)}.to change{cash_on_hand.reload.total_balance_amount}.from(0.0).to(-400.0)
			end

			it 'creates transfer to jeep account' do
				expect{UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params)}.to change{jeep.reload.total_balance_amount}.from(0.0).to(400.0)
			end
		end

		context 'when self selection transfer' do
			it 'does not change balance', invalid_transfer: true do
				UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: cash.id))
			end

			it 'returns false on #success?' do
				use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: cash.id))

				expect(use_case.success?).to eql(false)
			end

			it 'returns error message' do
				use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: cash.id))

				expect(use_case.errors).to  eql(["You can't transfer to the same account"])
			end
		end

		context 'when accounts from different categories' do
			let(:equity_account) {Account.find_by_name!('Opening Balance')}

			it 'returns false on #success?' do
				use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: equity_account.id))

				expect(use_case.success?).to be false
			end

			it 'returns error message' do
				use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: equity_account.id))

				expect(use_case.errors).to eql(["You can't transfer between accounts from different categories"])
			end

			it 'does not transfer', invalid_transfer: true do
				UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: equity_account.id))
			end
		end

		context 'when invalid date' do
			it 'returns error message' do
				use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(happened_at: (Date.current + 1.day).to_s))

				expect(use_case.success?).to be false
				expect(use_case.errors).to match_array(['Happened at cannot be after today'])
			end

			it 'does not transfer', invalid_transfer: true do
				UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(happened_at: (Date.current + 1.day).to_s))
			end
		end

		context 'when no accounts specified' do
			context 'when source account is blank' do
				it 'returns error message' do
					use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(source_account_id: ''))

					expect(use_case.success?).to be false
					expect(use_case.errors).to match_array(['You must specify transfer from account and transfer to account'])
				end

				it 'does not transfer', invalid_transfer: true do
					UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(source_account_id: ''))
				end
			end

			context 'when target account is blank' do
				it 'returns error message' do
					use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: ''))

					expect(use_case.success?).to be false
					expect(use_case.errors).to match_array(['You must specify transfer from account and transfer to account'])
				end

				it 'does not transfer', invalid_transfer: true do
					UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(target_account_id: ''))
				end
			end
		end

		context 'when invalid params' do
			context 'for transaction' do
				it 'returns errors' do
					use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(amount: ''))

					expect(use_case.success?).to     eql(false)
					expect(use_case.errors.count).to eql(2)
				end

				it 'does not transfer', invalid_transfer: true do
					UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(amount: ''))
				end
			end

			context 'for folder' do
				it 'returns errors' do
					use_case = UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(happened_at: ''))

					expect(use_case.success?).to 		 eql(false)
					expect(use_case.errors.count).to eql(1)
				end

				it 'does not transfer', invalid_transfer: true do
					UpdateTransferFunds.call(folder_id: folder.id, transfer_params: update_params.merge(happened_at: ''))
				end
			end
		end
	end
end