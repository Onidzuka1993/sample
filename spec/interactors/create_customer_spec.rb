require 'rails_helper'

RSpec.describe CreateCustomer do
	let(:customer_params) {{name: 'John Doe', email: 'john@gmail.com', phone_number: '7196582608', address: '120 N Main St. Creede CO'}}

	describe '.call' do
		it 'returns true on #success?' do
			use_case = CreateCustomer.call(customer_params: customer_params)

			expect(use_case.success?).to be true
		end

		it 'creates customer' do
			expect{CreateCustomer.call(customer_params: customer_params)}.to change{Customer.count}.from(0).to(1)
			expect(Customer.first).to have_attributes(customer_params)
		end

		context 'when invalid data' do
			it 'does not create customer' do
				expect{CreateCustomer.call(customer_params: customer_params.merge(name: ''))}.to_not change(Customer, :count)
			end

			it 'returns false on #success?' do
				use_case = CreateCustomer.call(customer_params: customer_params.merge(name: ''))

				expect(use_case.success?).to     					be false
				expect(use_case.customer.errors.count).to eql(1)
			end
		end
	end
end