require 'rails_helper'

RSpec.describe UpdateJournalEntry do
	let(:folder) 							 {create(:folder, folder_type: 'journal_entry')}

	let(:cash)  							 {Account.find_by_name('Cash')}
	let(:opening_balance) 		 {Account.find_by_name('Opening Balance')}

	let(:journal_entry_params) {{happened_at: Date.current, memo: 'memo'}}
	let(:debit_transactions)   {[{Account: cash.id, Debits: 500.0, Credits: '', Description: 'test'}]}
	let(:credit_transactions)  {[{Account: opening_balance.id, Debits: '', Credits: 500.0, Description: 'test'}]}

	describe '#call' do
		let(:params) {journal_entry_params.merge(debit_transactions: debit_transactions, credit_transactions: credit_transactions)}

		context 'when updating journal entry for Cash and Opening Balance accounts' do
			before do |example|
				create(:transaction, transaction_type: 'debit',  source_account_id: cash.id,  					amount: 900.0, folder_id: folder.id)
				create(:transaction, transaction_type: 'credit', target_account_id: opening_balance.id, amount: 900.0, folder_id: folder.id)

				cash.update_attribute(:total_balance_amount, 900.0)
				opening_balance.update_attribute(:total_balance_amount, 900.0)

				if example.metadata[:run_before]
					UpdateJournalEntry.call(folder_id: folder.id, journal_entry_params: params)
				end
			end

			it 'updates journal entry folder', :run_before do
				expect(folder.reload).to have_attributes(journal_entry_params)
			end

			it 'updates debit accounts' do
				expect{UpdateJournalEntry.call(folder_id: folder.id, journal_entry_params: params)}.to change{cash.reload.total_balance_amount}.from(900.0).to(500.0)
			end

			it 'updates credit accounts' do
				expect{UpdateJournalEntry.call(folder_id: folder.id, journal_entry_params: params)}.to change{opening_balance.reload.total_balance_amount}.from(900.0).to(500.0)
			end
		end
	end
end