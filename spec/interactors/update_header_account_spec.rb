require 'rails_helper'

RSpec.describe UpdateHeaderAccount do
  describe '.call' do
    let(:header_account) { HeaderAccount.find_by_name('Assets') }

    it 'updates header account' do
      update_params = {description: 'Test description'}
      use_case = UpdateHeaderAccount.call(header_account_id: header_account.id, update_params: update_params)

      expect(use_case.success?).to be true
      expect(header_account.reload.description).to eql(update_params[:description])
    end

    context 'when invalid params given' do
      it 'does not update header account' do
        update_params = {description: ''}
        use_case = UpdateHeaderAccount.call(header_account_id: header_account.id, update_params: update_params)

        expect(use_case.success?).to be false
        expect(use_case.header_account.errors.count).to eql(1)
      end
    end
  end
end