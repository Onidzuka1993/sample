require 'rails_helper'

RSpec.describe UpdateExpense do
	let(:folder) 			  			{create(:folder, folder_type: 'expense')}
	let(:cash_on_hand)  			{create(:assets_account, name: 'Cash on Hand')}

	let(:jeep)				  			{create(:assets_account, name: 'Jeep')}
	let(:equipment) 	  			{create(:assets_account, name: 'Equipment')}

	let(:attributes) do
		{
				folder_id: 				 folder.id,
				status: 					 'completed',
				transaction_type:  'debit',
				amount: 					 '',
				source_account_id: nil,
				target_account_id: nil,
				description:   		 ''
		}
	end

	include_context 'shared expense data'

	describe '#call' do
		before do |example|
			unless example.metadata[:skip_before]
				@use_case = UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)
			end
		end

		it_behaves_like 'expense examples'

		it 'updates folder' do
			expected_params = {vendor_id:	vendor.id, payment_method: 'Cash', happened_at: Date.current.to_date, memo: 'memo'}

			expect(folder.reload).to have_attributes(expected_params)
		end

		context 'when added transactions and changed amount' do
			let!(:transactions) do
				cash.update_attribute(:total_balance_amount, -2000.0)
				rent.update_attribute(:total_balance_amount, 2000.0)

				create(:transaction, transaction_type: 'debit', source_account_id: cash.id, amount: 2000, folder_id: folder.id)
				create(:transaction, transaction_type: 'credit', target_account_id: rent.id, amount: 2000, folder_id: folder.id)
			end

			it 'updates debit transaction' do
				expected_attributes = {amount: BigDecimal.new(200), description: 'Rent', transaction_type: 'debit', source_account_id: cash.id}
				expect(@use_case.folder.journal_transactions.first).to have_attributes(attributes.merge(expected_attributes))
			end

			it 'updates credit transaction' do
				expected_attributes = {amount: BigDecimal.new(200), description: 'Rent', transaction_type: 'credit', target_account_id: rent.id}
				expect(@use_case.folder.journal_transactions.second).to have_attributes(attributes.merge(expected_attributes))
			end

			it 'adds transactions', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{folder.journal_transactions.count}.from(2).to(4)
			end

			it 'updates cash balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{cash.reload.total_balance_amount}.from(-2000.0).to(-1200.0)
			end

			it 'updates rent account balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{rent.reload.total_balance_amount}.from(2000.0).to(200.0)
			end

			it 'creates wages account balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{wages.reload.total_balance_amount}.from(0.0).to(1000.0)
			end
		end

		context 'when accounts changed' do
			let!(:transactions) do
				cash_on_hand.update_attribute(:total_balance_amount, 1000.0)
				jeep.update_attribute(:total_balance_amount, 2000.0)

				create(:transaction, transaction_type: 'debit', source_account_id: cash_on_hand.id, amount: 2000, folder_id: folder.id)
				create(:transaction, transaction_type: 'credit', target_account_id: jeep.id, amount: 2000, folder_id: folder.id)
			end

			it 'updates cash_on_hand account balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{cash_on_hand.reload.total_balance_amount}.from(1000.0).to(3000.0)
			end

			it 'updates jeep account balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{jeep.reload.total_balance_amount}.from(200_0).to(0.0)
			end

			it 'updates cash account balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{cash.reload.total_balance_amount}.from(0.0).to(-1200.0)
			end

			it 'updates rent account', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{rent.reload.total_balance_amount}.from(0.0).to(200.0)
			end

			it 'updates rent account', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{wages.reload.total_balance_amount}.from(0.0).to(1000.0)
			end
		end

		context 'when nothing changed' do
			before do
				cash.update_attribute(:total_balance_amount, -1200.0)
				rent.update_attribute(:total_balance_amount, 200.0)
				wages.update_attribute(:total_balance_amount, 1000.0)

				create(:transaction, transaction_type: 'debit', source_account_id: cash.id, amount: 200, folder_id: folder.id)
				create(:transaction, transaction_type: 'credit', target_account_id: rent.id, amount: 200, folder_id: folder.id)

				create(:transaction, transaction_type: 'debit', source_account_id: cash.id, amount: 1000, folder_id: folder.id)
				create(:transaction, transaction_type: 'credit', target_account_id: wages.id, amount: 1000, folder_id: folder.id)

				UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)
			end

			it 'does not update cash account', :skip_before do
				expect(cash.reload.total_balance_amount).to eql(-1200.0)
			end

			it 'does not update rent account', :skip_before do
				expect(rent.reload.total_balance_amount).to eql(200.0)
			end

			it 'does not update wages account', :skip_before do
				expect(wages.reload.total_balance_amount).to eql(1000.0)
			end

			it 'does not create transactions', :skip_before do
				expect(Transaction.count).to eql(4)
			end
		end

		context 'when transactions removed' do
			before do
				cash.update_attribute(:total_balance_amount, -300.0)
				rent.update_attribute(:total_balance_amount, 300.0)

				3.times do
					create(:transaction, transaction_type: 'debit', source_account_id: cash.id, amount: 100, folder_id: folder.id)
					create(:transaction, transaction_type: 'credit', target_account_id: rent.id, amount: 100, folder_id: folder.id)
				end
			end

			it 'removes transactions', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{folder.journal_transactions.count}.from(6).to(4)
			end

			it 'updates cash account balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{cash.reload.total_balance_amount}.from(-300).to(-1200)
			end

			it 'updates cash account balance', :skip_before do
				expect{UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)}.to change{rent.reload.total_balance_amount}.from(300).to(200)
			end
		end
	end
end