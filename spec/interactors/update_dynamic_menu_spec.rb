require 'rails_helper'

RSpec.describe UpdateDynamicMenu do
	let(:cash)                 {DetailType.find_by_name!('Cash')}
	let(:opening_balance)      {DetailType.find_by_name!('Opening Balance')}
	let(:other_current_assets) {create(:other_current_assets)}

  let(:dynamic_menu)   {create(:dynamic_menu, from_list: [cash.id], to_list: [opening_balance.id], detail_type_ids: [cash.id, opening_balance.id, other_current_assets.id])}

  describe '.call' do
		let(:params)          {{name: 'test', description: 'test', from_list: [opening_balance.id], to_list: [cash.id]}}
		let(:expected_params) {{name: 'test', description: 'test', from_list: [opening_balance.id.to_s], to_list: [cash.id.to_s]}}

    it 'updates dynamic menu' do
      use_case = UpdateDynamicMenu.call(dynamic_menu_id: dynamic_menu.id, dynamic_menu_params: params)

      expect(use_case.success?).to     be true
      expect(use_case.dynamic_menu).to have_attributes(expected_params)
      expect(use_case.dynamic_menu.reload.dynamic_menus_detail_types.count).to eql(2)
    end
  end
end