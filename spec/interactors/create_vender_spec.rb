require 'rails_helper'

RSpec.describe CreateVendor do
	let(:vendor_params) {{name: 'John Doe', email: 'john@gmail.com', phone_number: '7196582608', address: '120 N Main St. Creede CO'}}

	describe '.call' do
		it 'returns true on #success?' do
			use_case = CreateVendor.call(vendor_params: vendor_params)

			expect(use_case.success?).to be true
		end

		it 'creates vendor' do
			expect{CreateVendor.call(vendor_params: vendor_params)}.to change{Vendor.count}.from(0).to(1)
			expect(Vendor.first).to have_attributes(vendor_params)
		end

		context 'when invalid data' do
			it 'does not create vendor' do
				expect{CreateVendor.call(vendor_params: vendor_params.merge(name: ''))}.to_not change(Vendor, :count)
			end

			it 'returns false on #success?' do
				use_case = CreateVendor.call(vendor_params: vendor_params.merge(name: ''))

				expect(use_case.success?).to     			  be false
				expect(use_case.vendor.errors.count).to eql(1)
			end
		end
	end
end