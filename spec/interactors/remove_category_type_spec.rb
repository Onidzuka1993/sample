require 'rails_helper'

RSpec.describe RemoveCategoryType do
	let(:current_assets) {create(:current_assets)}

  describe '.call' do
    it 'removes category type' do
      use_case = RemoveCategoryType.call(category_type_id: current_assets.id)

      expect(use_case.success?).to                       be true
      expect(CategoryType.exists?(current_assets.id)).to be false
    end

    context 'when has associated objects' do
      before {create(:other_current_assets)}

      it 'does not remove category type' do
        use_case = RemoveCategoryType.call(category_type_id: current_assets.id)

        expect(use_case.success?).to be false
        expect(use_case.message).to  eql('This category type has detail types.')
      end
    end
  end
end