require 'rails_helper'

RSpec.describe CreateDynamicMenu do
	let(:equipment) {create(:other_current_assets, name: 'equipment')}
	let(:vehicle)   {create(:other_current_assets, name: 'vehicle')}
	let(:housing)   {create(:other_current_assets, name: 'housing')}
	
  describe '.call' do
    it 'creates dynamic menu' do
      params = {
          name: 'test',
          description: 'test',
          from_list: [equipment.id, vehicle.id],
          to_list: [housing.id]
      }

      expected_params = {
          name: 'test',
          description: 'test',
          from_list: [equipment.id.to_s, vehicle.id.to_s],
          to_list: [housing.id.to_s]
      }

      expect {
        @use_case = CreateDynamicMenu.call(dynamic_menu_params: params)
      }.to change(DynamicMenu, :count).by(1)

      expect(@use_case.success?).to     be true
      expect(@use_case.dynamic_menu).to have_attributes(expected_params)

      expect(@use_case.dynamic_menu.dynamic_menus_detail_types.count).to eql(3)
    end

    context 'when invalid params' do
      it 'does not create dynamic menu' do
        params = {name: '', description: '', from_list: [equipment.id], to_list: [vehicle.id]}

        use_case = CreateDynamicMenu.call(dynamic_menu_params: params)

        expect(use_case.success?).to                 be false
        expect(use_case.dynamic_menu.errors.any?).to be true
      end

      it do
        params = {name: 'test', description: 'test', from_list: [equipment.id], to_list: [equipment.id]}

        use_case = CreateDynamicMenu.call(dynamic_menu_params: params)

        expect(use_case.success?).to                           be false
        expect(use_case.dynamic_menu.errors[:base].first).to eql("You can't add the same account list for on both ends.")
      end
    end
  end
end