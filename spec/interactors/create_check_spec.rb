require 'rails_helper'

RSpec.describe CreateCheck do
	include_context 'shared check data'

	describe '.call' do
		before do |example|
			unless example.metadata[:skip_before]
				@use_case = CreateCheck.call(check_params: check_params)
			end
		end

		it_behaves_like 'check examples'

		it 'creates folder with transactions', :skip_before do
			expected_attributes = {vendor_id: vendor.id, happened_at: Date.current, memo: 'memo', folder_type: 'check', address: 'address'}

			expect{CreateCheck.call(check_params: check_params)}.to change{Folder.count}.from(0).to(1)
			expect(Folder.first).to 																have_attributes(expected_attributes)
			expect(Folder.first.journal_transactions.count).to 			eql(4)
		end

		it 'creates transactions', :skip_before do
			expect{CreateCheck.call(check_params: check_params)}.to change{Transaction.count}.from(0).to(4)
		end

		it 'decreases select account' do
			expect(Account.find_by_name('Cash').total_balance_amount).to eql(-1200.0)
		end

		it 'increases print check table accounts' do
			expect(car.reload.total_balance_amount).to 			 eql(200.0)
			expect(furniture.reload.total_balance_amount).to eql(1000.0)
		end

		it 'returns data for print check' do
			print_check_data = {vendor_name: vendor.name, address: Folder.first.address, total_check_amount: 1200.0, memo: 'memo'}

			expect(@use_case.print_check_data).to eql(print_check_data)
		end
	end
end