require 'rails_helper'

RSpec.describe GetBalanceSheet do
	let(:balance_sheet)   {%w(Assets Liabilities Equity)}
	let(:header_accounts) {double}

  context 'constant value' do
    it 'checks constant value' do
      expect(GetBalanceSheet::BALANCE_SHEET).to match_array(balance_sheet)
    end
  end

  describe '.call' do
    it 'creates a query to retrieve all accounts from Assets, Liabilities, and Equity with custom date' do
      expect(HeaderAccount).to   receive(:includes).with(category_types: [{detail_types: :accounts}]).and_return(header_accounts)
      expect(header_accounts).to receive(:where).with(name: balance_sheet).and_return(header_accounts)
      expect(header_accounts).to receive(:where).with('accounts.created_at <= ?', Time.parse(Date.current.to_s).end_of_day).and_return(header_accounts)
      expect(header_accounts).to receive(:references).with(:accounts).and_return(header_accounts)

			allow(header_accounts).to receive(:empty?).and_return(false)

			GetBalanceSheet.call(date: Date.current.to_s)
		end
  end
end