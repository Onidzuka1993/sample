require 'rails_helper'

RSpec.describe CreateDetailType do
	let(:current_assets)     {create(:current_assets)}

  let(:detail_type_params) { {category_type_id: current_assets.id, name: 'test name', description: 'description'} }
  let(:invalid_params)     { {category_type_id: '', name: '', description: ''} }

  describe '.call' do
    it 'creates detail type' do
      use_case = CreateDetailType.call(detail_type_params: detail_type_params)

      expect(use_case.success?).to be true
      expect(use_case.detail_type).to have_attributes(detail_type_params)
    end

    context 'when invalid params' do
      it 'does not create' do
        use_case = CreateDetailType.call(detail_type_params: invalid_params)

        expect(use_case.success?).to                be false
        expect(use_case.detail_type.errors.any?).to be true
      end
    end
  end
end