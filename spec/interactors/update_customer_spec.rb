require 'rails_helper'

RSpec.describe UpdateCustomer do
	let(:customer) 			{create(:customer)}
	let(:update_params) {{name: 'test', email: 'test@gmail.com', phone_number: 'test', address: 'test'}}

	describe '.call' do
		it 'returns true on #success' do
			use_case = UpdateCustomer.call(customer_id: customer.id, customer_params: update_params)

			expect(use_case.success?).to be true
		end

		it 'updates customer' do
			use_case = UpdateCustomer.call(customer_id: customer.id, customer_params: update_params)

			expect(use_case.customer).to have_attributes(update_params)
		end

		context 'when invalid data' do
			it 'returns false on #success?' do
				use_case = UpdateCustomer.call(customer_id: customer.id, customer_params: update_params.merge(name: ''))

				expect(use_case.success?).to be false
			end
		end
	end
end