require 'rails_helper'

RSpec.describe CreateCategoryType do
  describe '.call' do
    let(:header_account) { HeaderAccount.find_by_name!('Assets') }

    it 'creates sub account' do
      params = {
          name: 'test name',
          description: 'Test description',
          header_account_id: header_account.id
      }

      use_case = CreateCategoryType.call(category_type_params: params)

      expect(use_case.success?).to be true
      expect(use_case.category_type.name).to              eql(params[:name])
      expect(use_case.category_type.description).to       eql(params[:description])
      expect(use_case.category_type.header_account.id).to eql(header_account.id)
    end

    context 'when invalid params given' do
      it 'does not create sub account' do
        params = {
            name: '',
            description: '',
            header_account_id: header_account.id
        }

        use_case = CreateCategoryType.call(category_type_params: params)

        expect(use_case.success?).to be false
        expect(use_case.category_type.errors.count).to eql(2)
      end
    end
  end
end