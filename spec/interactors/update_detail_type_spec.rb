require 'rails_helper'

RSpec.describe UpdateDetailType do
  let(:current_assets)        {create(:current_assets)}
  let(:expenses)              {create(:expenses)}

  let(:other_current_assets)  {create(:other_current_assets)}

  let(:detail_type_params) {{category_type_id: expenses.id, name: 'test name', description: 'test desc'}}
  let(:invalid_params)     {{category_type_id: '', name: '', description: ''}}

  describe '.call' do
    it 'updates detail_type' do
      use_case = UpdateDetailType.call(detail_type_id: other_current_assets.id, detail_type_params: detail_type_params)

      expect(use_case.success?).to                     be true
      expect(use_case.detail_type).to have_attributes(detail_type_params)
    end

    context 'when invalid params' do
      it 'does not update detail_type' do
        use_case = UpdateDetailType.call(detail_type_id: other_current_assets.id, detail_type_params: invalid_params)

        expect(use_case.success?).to             be false
        expect(use_case.detail_type.errors.any?).to be true
      end
    end
  end
end