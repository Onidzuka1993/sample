require 'rails_helper'

RSpec.describe CreateAccount do
	let(:other_current_assets) {create(:other_current_assets)}
  let(:account_params)  {{detail_type_id: other_current_assets.id, name: 'test name', description: 'test description'}}

  describe '.call' do
    it 'creates account' do
      expect { CreateAccount.call(account_params: account_params) }.to change(Account, :count).by(1)
		end

		it 'returns true on #success?' do
			use_case = CreateAccount.call(account_params: account_params)

      expect(use_case.success?).to be true
		end

		it 'has appropriate attributes' do
			use_case = CreateAccount.call(account_params: account_params)

      expect(use_case.account).to have_attributes(account_params)
		end

    context 'when invalid params' do
      it 'does not create account' do
        account_params = {detail_type_id: other_current_assets.id, name: '', description: ''}

        use_case = CreateAccount.call(account_params: account_params)

        expect(use_case.success?).to             be false
        expect(use_case.account.errors.count).to eql(2)
      end
    end
  end
end