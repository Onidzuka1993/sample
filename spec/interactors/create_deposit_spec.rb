require 'rails_helper'

RSpec.describe CreateDeposit do
	let(:cash)					 	{Account.find_by_name!('Cash')}
	let(:opening_balance) {Account.find_by_name!('Opening Balance')}

  let(:params)          {{amount: '1000.0', source_account_id: cash.id}}
  let(:invalid_params)  {{amount: '', source_account_id: cash.id}}

  describe '.call' do
		it 'creates folder with type deposit' do
			expect {CreateDeposit.call(params: params)}.to change{Folder.count}.from(0).to(1)
		end

		it 'creates folder with two transactions' do
			CreateDeposit.call(params: params)

			expect(Folder.first.journal_transactions.count).to eql(2)
		end

    it 'creates two transactions' do
			expect {CreateDeposit.call(params: params)}.to change{Transaction.count}.from(0).to(2)

			transaction = Transaction.second

			expect(transaction.transaction_type).to eql('credit')
			expect(transaction.target_account.name).to 	eql('Opening Balance')
		end

		it 'creates debit transaction' do
			CreateDeposit.call(params: params)

			transaction = Transaction.first

			expect(transaction.transaction_type).to 		eql('debit')
			expect(transaction.source_account.name).to	eql('Cash')

			expect(transaction.target_account).to				be_nil
		end

		it 'creates credit transaction' do
			CreateDeposit.call(params: params)

			transaction = Transaction.second

			expect(transaction.transaction_type).to 	 eql('credit')
			expect(transaction.target_account.name).to eql('Opening Balance')

			expect(transaction.source_account).to			 be_nil
		end

		it 'returns true on #success?' do
			use_case = CreateDeposit.call(params: params)

			expect(use_case.success?).to be true
		end

		it 'debits assets account' do
			expect{CreateDeposit.call(params: params)}.to change{cash.reload.total_balance_amount}.from(0.0).to(1000.0)
		end

		it 'credits equity account' do
			expect{CreateDeposit.call(params: params)}.to change{opening_balance.reload.total_balance_amount}.from(0.0).to(1000.0)
		end

		context 'when invalid params' do
			it 'returns false on #success?' do
        use_case = CreateDeposit.call(params: invalid_params)

        expect(use_case.success?).to be false
			end

			it 'returns errors' do
				use_case = CreateDeposit.call(params: invalid_params)

				expect(use_case.debit_transaction.errors.count).to eql(2)
			end
		end
  end
end