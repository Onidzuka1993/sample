RSpec.shared_context 'shared check data', shared_context: :metadata do
	let(:vendor)				{create(:vendor)}
	let(:car) 				  {create(:assets_account, name: 'Car')}
	let(:furniture) 		{create(:assets_account, name: 'Furniture')}
	let(:cash)					{Account.find_by_name('Cash')}


	let(:print_check_table) do
		[
				{Account: car.id, 			Description: 'Car', 			Amount: '200'},
				{Account: furniture.id, Description: 'Furniture', Amount: '1000'},
				{Account: '', 					Description: 'Furniture', Amount: '1000'},
				{Account: furniture.id, Description: 'Furniture', Amount: ''}
		]
	end

	let(:check_params)  do
		{
				vendor_id:				 vendor.id,
				account_id: 			 cash.id,
				payment_date: 		 Date.current.to_s,
				address: 					 'address',
				memo: 	 					 'memo',
				print_check_table: JSON.generate(print_check_table)
		}
	end
end