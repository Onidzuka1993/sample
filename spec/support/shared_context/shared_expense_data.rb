RSpec.shared_context 'shared expense data', shared_context: :metadata do
	let(:vendor) {create(:vendor)}
	let(:rent)   {create(:expense_account, name: 'Rent')}
	let(:wages)  {create(:expense_account, name: 'Wages')}
	let(:cash)   {Account.find_by_name('Cash')}

	let(:expense_entries) do
		[
				{Account: rent.id, Description: 'Rent', Amount: '200'},
				{Account: wages.id, Description: 'Wages', Amount: '1000'},
				{Account: '', Description: 'expense', Amount: '1000'},
				{Account: wages.id, Description: 'expense', Amount: ''}
		]
	end

	let(:expense_params) do
		{
				vendor_id:			 vendor.id,
				account_id: 		 cash.id,
				payment_date: 	 Date.current.to_s,
				payment_method:  'Cash',
				memo: 	 				 'memo',
				expense_entries: JSON.generate(expense_entries)
		}
	end
end