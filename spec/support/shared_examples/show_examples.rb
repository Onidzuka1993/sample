RSpec.shared_examples "show examples" do
	it {expect(response).to render_template(:show)}
	it {expect(response).to have_http_status(:ok)}
end