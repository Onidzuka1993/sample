RSpec.shared_examples 'edit examples' do
	it {expect(response).to render_template(:edit)}
	it {expect(response).to have_http_status(:ok)}
end