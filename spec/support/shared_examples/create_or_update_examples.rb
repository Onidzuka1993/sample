RSpec.shared_examples "create or update examples" do
	it {expect(response).to redirect_to(object)}
	it {expect(response).to have_http_status(:found)}
end