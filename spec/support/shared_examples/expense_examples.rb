RSpec.shared_examples 'expense examples' do
	it 'returns true on .success?' do
		expect(@use_case.success?).to be true
	end

	it 'parses JSON data' do
		expect(@use_case.entries).to_not eql(expense_entries)
		expect(@use_case.entries).to		 eql(expense_entries.select {|entry| entry[:Account].present? && entry[:Amount].present?})
	end

	it 'checks presence of cash account', :skip_before do
		use_case = CreateExpense.call(expense_params: expense_params.merge(account_id: ''))

		expect(use_case.success?).to be false
		expect(use_case.message).to  eql(error: 'At the top of the page, select the bank account for this check.')
	end

	it 'checks presence of at least one line item' do
		use_case = CreateExpense.call(expense_params: expense_params.merge(expense_entries: JSON.generate([{Account: '', Description: '', Amount: ''}])))

		expect(use_case.success?).to be false
		expect(use_case.message).to eql({error: 'You must fill out at least one detail line.'})
	end

	it 'checks presence of payment date', :skip_before do
		use_case = CreateExpense.call(expense_params: expense_params.merge(payment_date: ''))

		expect(use_case.success?).to be false
		expect(use_case.message).to  eql(error: 'You must specify a date.')
	end

	it 'checks if date valid', :skip_before do
		use_case = CreateExpense.call(expense_params: expense_params.merge(payment_date: 'invalid_date'))

		expect(use_case.success?).to be false
		expect(use_case.message).to  eql(error: 'You must specify a valid date')
	end
end