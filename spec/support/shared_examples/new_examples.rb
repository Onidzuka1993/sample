RSpec.shared_examples "new examples" do
	it { expect(response).to have_http_status(:ok) }
	it { expect(response).to render_template(:new) }
end