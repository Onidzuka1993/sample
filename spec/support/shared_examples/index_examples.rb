RSpec.shared_examples 'index examples' do
	it {expect(response).to render_template(:index)}
	it {expect(response).to have_http_status(:ok)}
end