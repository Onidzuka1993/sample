RSpec.shared_examples 'check examples' do
	it 'returns true on .success?' do
		expect(@use_case.success?).to be true
	end

	it 'parses JSON input data' do
		expect(@use_case.entries).to_not eql(print_check_table)
		expect(@use_case.entries).to 		 eql(print_check_table.select {|entry| entry[:Account].present? && entry[:Amount].present?})
	end

	it 'checks presence of cash account', :skip_before do
		use_case = CreateCheck.call(check_params: check_params.merge(account_id: ''))

		expect(use_case.success?).to be false
		expect(use_case.message).to  eql(error: 'At the top of the page, select the bank account for this check.')
	end

	it 'checks presence of at least one line item' do
		use_case = CreateCheck.call(check_params: check_params.merge(print_check_table: JSON.generate([{Account: '', Description: '', Amount: ''}])))

		expect(use_case.success?).to be false
		expect(use_case.message).to eql({error: 'You must fill out at least one detail line.'})
	end

	it 'checks presence of date', :skip_before do
		use_case = CreateCheck.call(check_params: check_params.merge(payment_date: ''))

		expect(use_case.success?).to be false
		expect(use_case.message).to  eql(error: 'You must specify a date.')
	end

	it 'checks if date valid', :skip_before do
		use_case = CreateCheck.call(check_params: check_params.merge(payment_date: 'invalid_date'))

		expect(use_case.success?).to be false
		expect(use_case.message).to  eql(error: 'You must specify a valid date')
	end
end