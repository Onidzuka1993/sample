(function() {
  "use strict";

  App.register('component').enter(function() {
    var numericHandler = function(options, self) {
      self.handler = function(){
        self.set($(self.el).autoNumeric('get'));
      }

      self.leaveHandler = function(){
        var val = $(self.el).autoNumeric('get');
        if (val.length < 1) {
          $(self.el).autoNumeric('set', 0);
          self.set(0);
        }
      }
      options['mDec'] = options['mDec'] || '2';
      options['wEmpty'] = 'empty';
      if (self.params.max) {
        options['vMax'] = self.params.max;
      }
      if (self.params.min) {
        options['vMin'] = self.params.min;
      }
      $(self.el).autoNumeric('init', options).on('keyup', self.handler).on('focusout', self.leaveHandler);
    }

    var unbind = function(){
      $(this.el).autoNumeric('destroy');
    }

    var update = function(value){
      try {
        $(this.el).autoNumeric('set', value);
      }catch(err){

      }

    }

    Vue.directive('currencyModel', {
      twoWay: true,
      params: ['precision', 'min', 'max'],
      priority: 900,
      bind: function() {
        numericHandler({aSign: '$', mDec: this.params.precision || '3'}, this);
      },
      update: update,
      unbind: unbind
    });

    Vue.directive('percentModel', {
      twoWay: true,
      params: ['precision', 'min', 'max'],
      priority: 900,
      bind: function() {
        numericHandler({aSign: '%', pSign: 's', mDec: this.params.precision || '0'}, this);
      },
      update: update,
      unbind: unbind
    });

    Vue.directive('numericModel', {
      twoWay: true,
      params: ['precision', 'max', 'min'],
      priority: 900,
      bind: function() {
        numericHandler({}, this);
      },
      update: update,
      unbind: unbind
    });
  });
})();
