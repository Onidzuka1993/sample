(function() {
  "use strict";

  App.register('component').enter(function() {

    App.pageActions = new Vue({
      el: '#v-page-action',
      template: '<span><div class="crops-links panel" v-if="exportOptions.length > 0">\
                  <div class="title">\
                    <div class="exports">\
                      Export Page as:\
                      <span v-for="exportOption in exportOptions" v-on:click="exportOption.function()">\
                        <i class="fa {{ fetchIcon(exportOption) }}"></i> {{ exportOption.name }}\
                      </span>\
                    </div>\
                  </div>\
                </div></span>',
      data: function() {
        return {
          exportOptions: [],
          exportIcons: {default: 'fa-file-text-o', pdf: 'fa-file-pdf-o', word: 'fa-file-word-o', excel: 'fa-file-excel-o', code: 'fa-file-code-o', powerpoint: 'fa-file-powerpoint-o', image: 'fa-file-image-o'}
        }
      },
      methods: {
        fetchIcon: function(exportOption){
          return this.exportIcons[exportOption.icon] || this.exportIcons.default;
        }
      }
    });
  });
})();
