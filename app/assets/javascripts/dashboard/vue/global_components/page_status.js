(function() {
  "use strict";

  App.register('component').enter(function() {
    var statuses = {
      100: { // General loading/working status
        message: 'Loading',
        additionalDetails: ''
      },
      101: {
        message: 'Loading',
        additionalDetails: 'This seams to be taking awhile.  You can click \'cancel\' to continue to interact with the website but some content might not be accurate.'
      },
      102: {
        message: 'Saving',
        additionalDetails: 'This seams to be taking awhile.  You can click \'cancel\' to continue to interact with the website but some content might not be accurate.'
      },
      200: { // General error status
        message: 'An unkown error occurred.',
        additionalDetails: 'Try refreshing the page.'
      },
      201: {
        message: 'There was a problem loading your data.',
        additionalDetails: 'Try refreshing the page.'
      },
      202: {
        message: 'There was a problem saving your data.',
        additionalDetails: 'Try resaving your data.'
      }
    };

    App.pageStatus = new Vue({
      el: '#v-page-status',
      template: '<span>\
                  <div class="content-loading" v-if="loading" transition="loading-fade-out">\
                    <i class="fa fa-spinner fa-spin"></i>\
                    <h5>\
                      {{ status.message }}\
                    </h5>\
                    <p v-if="status.showAdditionalMessage">\
                      &nbsp;{{ status.additionalDetails }}\
                    </p>\
                    <span v-on:click="dismiss()">Cancel</span>\
                  </div>\
                  <div  class="content-error" v-if="error" transition="error-fade-out">\
                    <div class="error-background" v-on:click="dismiss()"></div>\
                    <div class="error-contents">\
                      <i class="fa fa-ban"></i>\
                      <h5>\
                        Error\
                      </h5>\
                      <h6>Code {{ status.code }}</h6>\
                      <p>\
                         {{ status.message }}\
                      </p>\
                      <p>\
                        {{ status.additionalDetails }} If the problem persists, please feel free to contact us at <a href="mailto:nick@harvestprofit.com">nick@harvestprofit.com</a>.\
                      </p>\
                      <div class="actions">\
                        <span v-on:click="retry()" v-if="status.retry">Retry</span>\
                        <span v-on:click="dismiss()">Close</span>\
                      </div>\
                    </div>\
                  </div>\
                </span>',
      data: function() {
        return {
          status: {}
        }
      },
      computed: {
        loading: function() {
          if (this.status.codeCategory === 100) {
            return true;
          }else{
            return false;
          }
        },
        error: function() {
          if (this.status.codeCategory === 200) {
            return true;
          }else{
            return false;
          }
        }
      },
      ready: function() {
        this.reset();
      },
      methods: {
        putLoading: function(args){
          var ops = args || {};
          ops.code = 101
          this.putStatus(ops);
        },
        putSaving: function(args){
          var ops = args || {};
          ops.code = 102
          this.putStatus(ops);
        },
        putError: function(args){
          var ops = args || {};
          ops.code = 200
          this.putStatus(ops);
        },
        putLoadingError: function(args){
          var ops = args || {};
          ops.code = 201
          this.putStatus(ops);
        },
        putSavingError: function(args){
          var ops = args || {};
          ops.code = 202
          this.putStatus(ops);
        },
        reset: function() {
          if (this.status.additionalMessageTimeout !== null) {
            clearTimeout(this.status.additionalMessageTimeout);
          }

          this.status = {
            code: null,
            codeCategory: null,
            message: '',
            additionalDetails: '',
            retry: null,
            dismiss: null,
            showAdditionalMessage: true,
            additionalMessageTimeout: null
          }
        },
        dismiss: function() {
          if (this.status.dismiss !== undefined && this.status.dismiss !== null) {
            this.status.dismiss();
          }

          this.reset();
        },
        retry: function() {
          if (this.status.retry !== undefined && this.status.retry !== null) {
            this.status.retry();
          }
        },
        setAdditionalMessageTimeout: function() {
          if (this.status.codeCategory === 100) {
            var self = this;
            self.status.showAdditionalMessage = false;
            this.status.additionalMessageTimeout = setTimeout(function(){
              self.status.showAdditionalMessage = true;
            }, 8000);
          }
        },
        putStatus: function(args) {
          var ops = args || {};
          var code = ops.code || -1;
          var codeCategory = Math.floor(code / 100) * 100;
          var status = statuses[code];
          var statusCode = code;
          if (status === undefined) {
             status = statuses[codeCategory];
             if (status === undefined) {
               console.warn('STATUS [' + code + '] is not a valid status and does not fall in a general category.');
               statusCode = null;
             }else{
               statusCode = codeCategory;
             }
          }

          this.status.message = ops.message || status.message;
          this.status.additionalDetails = ops.additionalDetails || status.additionalDetails;
          this.status.retry = ops.retry;
          this.status.dismiss = ops.dismiss;
          this.status.code = statusCode;
          this.status.codeCategory = codeCategory;
          this.setAdditionalMessageTimeout();
        }
      }
    });
  });
})();
