(function() {
  "use strict";

  App.register('component').enter(function() {

    Vue.component('vue-input', {
      template: ' <div class="v-input">\
                    <i v-if="inputIcon" v-on:click="activeObject()" class="v-input-icon fa" :class="[inputIcon]"></i>\
                    <input v-if="numericType" v-el:input v-numeric-model="inputValue" :precision="precision" :min="min" :max="max" :placeholder="placeholder">\
                    <input v-if="currencyType" v-el:input v-currency-model="inputValue" :precision="precision" :min="min" :max="max" :placeholder="placeholder">\
                    <input v-if="percentType" v-el:input v-percent-model="inputValue" :precision="precision" :min="min" :max="max" :placeholder="placeholder">\
                    <input v-if="plainType" v-el:input v-model="inputValue" :placeholder="placeholder">\
                    <input v-if="dateType" v-el:input v-model="inputValue" :placeholder="placeholder">\
                    <div v-if="message" class="v-input-message-container">\
                      <i class="fa fa-exclamation-triangle"></i><div><h5>{{ message }}</h5><p v-if="inputFormat">Format: {{ inputFormat }}</p></div>\
                    </div>\
                  </div>',
      props: {
        'type': {
          default: 'plain'
        },
        'precision': {
          default: null
        },
        'min': {
          default: null
        },
        'max': {
          default: null
        },
        'placeholder': {
          default: null
        },
        'required': {
          default: false
        },
        'validate': {
          default: false
        },
        'toString': {
          default: false
        },
        'noIcon': {
          default: false
        },
        'value': {
          twoWay: true
        }
      },
      data: function() {
        return {
          inputValue: null,
          message: null,
          inputFormat: null,
          init: true,
          obj: null
        }
      },
      events: {
        'validate-inputs': function(callback){
          if (!this.validateContent()) {
            if (typeof callback === 'function') {
              callback();
            }
          }
        }
      },
      ready: function() {
        this.inputValue = this.value;
        if (this.dateType) {
          this.obj = new Pikaday({
            field: this.$els.input,
            format: 'MM/DD/YYYY',
          });
        }
        $(this.$els.input).on('focus', this.validateContent);
      },
      watch: {
        inputValue: function(newValue) {
          if (this.toString) {
            this.value = String(newValue);
          }else if (this.dateType) {
            this.value = new Date(newValue);
          }else{
            this.value = newValue;
          }

          if (!this.init) {
            this.validateContent();
          }else{
            this.init = false;
          }
        }
      },
      methods: {
        activeObject: function() {
          if (this.dateType) {
            this.obj.show();
          }
        },
        validateContent: function() {
          this.message = null;
          this.inputFormat = null;
          if (this.required && !App.validators.presence(this.inputValue)) {
            this.message = "This can't be empty";
            this.inputFormat = null;
            return false;
          }
          if (this.validate) {
            switch (this.type) {
              case 'email':
                var res = App.validators.email(this.inputValue);
                if (!res) {
                  this.message = "Not a valid email address.";
                  this.inputFormat = "name@domain.com";
                }
                return res;
              case 'date':
                var res = App.validators.date(this.inputValue);
                if (!res) {
                  this.message = "Not a valid date.";
                  this.inputFormat = "mm/dd/yyyy";
                }
                return res;
              default:
                return true;
            }
          }else{
            return true;
          }
        }
      },
      computed: {
        inputIcon: function() {
          if (this.dateType) {
            return 'fa-calendar';
          }
        },
        dateType: function() {
          if (this.type === 'date' || this.type === 'calendar') {
            return true;
          }else{
            return false;
          }
        },
        numericType: function() {
          if (this.type === 'number' || this.type === 'numeric') {
            return true;
          }else{
            return false;
          }
        },
        currencyType: function() {
          if (this.type === 'money' || this.type === 'currency') {
            return true;
          }else{
            return false;
          }
        },
        percentType: function() {
          if (this.type === 'percent') {
            return true;
          }else{
            return false;
          }
        },
        plainType: function() {
          if (!this.numericType && !this.currencyType && !this.percentType && !this.dateType) {
            return true;
          }else{
            return false;
          }
        }
      }
    });
  });
})();
