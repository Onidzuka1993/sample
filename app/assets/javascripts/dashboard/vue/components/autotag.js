(function() {
  "use strict";

  App.register('component').enter(function() {

    var AutoTag = Vue.extend({
      template: '<div v-on:click="openAutocomplete()" class="v-tags"><div v-on:click="$event.stopPropagation()" v-for="tag in orderedTags">{{tag}}<i class="fa fa-times-circle" v-on:click="removeItem(tag, $event)"></i></div><input placeholder="Add a crop" v-el:autocomplete type="text" v-model="selectedItem" v-on:awesomplete-select="insertItem($event.text)" v-on:focus="openAutocomplete()" /></div>',
      props: ['tags', 'stringTags', 'list'],
      data: function() {
        return {
          selectedItem: null,
          defaultValue: "All Crops",
          autocomplete: null
        }
      },
      computed: {
        orderedTags: function(){
          return this.tags.sort();
        }
      },
      watch: {
        tags: function(){
          this.stringTags = this.tags.join(",");
        }
      },
      methods: {
        openAutocomplete: function(){
          this.autocomplete.evaluate();
          this.autocomplete.open();
          this.$els.autocomplete.focus();
        },
        insertItem: function(value){
          if (this.list.indexOf(value) > -1) {
            if (value === this.defaultValue){
              this.tags = [this.defaultValue];
            }else if (this.tags.indexOf(value) < 0) {
              this.tags.push(value);
              if (this.tags.indexOf(this.defaultValue) > -1) {
                this.tags.$remove(this.defaultValue);
              }
            }
          }

          var self = this;
          setTimeout(function () {
            self.selectedItem = null;
            $(self.$els.autocomplete).val('');
          }, 0);
        },
        removeItem: function(tag, event) {
          event.stopPropagation();
          this.tags.$remove(tag);

          if (this.tags.length < 1) {
            this.tags.push("All Crops");
          }
        }
      },
      ready: function(){
        this.autocomplete = new Awesomplete(this.$els.autocomplete, {
          list:     this.list,
          maxItems: 9999,
          minChars: 0,

          sort: function(a, b) {
            if (a === "All Crops"){
              return 1;
            }else if (b === "All Crops"){
              return 1;
            }
            return a.toLowerCase().localeCompare(b.toLowerCase());
          }
        });
      }
    });

    Vue.component('autotag', AutoTag)
  });
})();
