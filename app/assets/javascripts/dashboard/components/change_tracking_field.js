/* globals numeral */
(function() {
  "use strict";

  App.resetChangeTrackingFieldsOriginalValue = function() {
    var inputType = this.getAttribute('type');
    var normalizedValue = this.value.replace('%', '').replace('$', '');

    if (inputType === 'checkbox') {
      normalizedValue = this.checked + '';
    }

    this.setAttribute('data-original-value', normalizedValue);
  };

  // Save the original value of an input or select element to a data attribute
  // for diffing against to determine if a form has been saved.
  // Triggers changes in the form with if using the "change tracking form component."
  App.activateChangeTrackingFields = function() {
    var inputsSelectors = [
      'form.track input[type="checkbox"]:not([data-original-value])',
      'form.track input[type="text"]:not([data-original-value])',
      'form.track select:not([data-original-value])'
    ];
    var inputs = $(inputsSelectors.join(','));

    // Save the original value
    inputs.each(App.resetChangeTrackingFieldsOriginalValue);

    // Track changes and flag the input if there is a change
    inputs.on('change keyup paste', function() {
      var inputType = this.getAttribute('type');
      var originalValue = this.getAttribute('data-original-value');
      var parsedValue = parseFloat(originalValue);
      var newValue = this.value;

      if (inputType === 'checkbox') {
        // Handle checkbox changes
        newValue = this.checked + '';
      } else if (!isNaN(parsedValue)) {
        // Compare numbers properly by parsing them
        originalValue = parsedValue;
        newValue = newValue.replace('%', '');
        newValue = numeral(newValue).value();
      }

      // Flag input field as changed or remove the flag
      if (newValue === originalValue) {
        this.removeAttribute('data-changed');
      } else {
        this.setAttribute('data-changed', true);
      }

      // Fire change event on the form
      $(this.form).trigger('input:change');
    });
  };

  App.register('component').enter(function() {
    App.activateChangeTrackingFields();
  });
})();
