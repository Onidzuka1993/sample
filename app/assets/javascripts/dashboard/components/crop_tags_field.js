/* globals Awesomplete */
(function() {
  "use strict";

  // Enable Crop Tags Input fields
  // @see http://xoxco.com/projects/code/tagsinput/
  // @see http://leaverou.github.io/awesomplete/
  App.register('component').enter(function() {
    function activateCropTags() {
      $('input.crop-tags').each(function() {
        var tagsInput = $(this);
        if (tagsInput.data('activated')) {
          return;
        } else {
          tagsInput.data('activated', true);
        }

        var addedTags = tagsInput.val().split(',');
        var tagsInputId = $(this).prop('id');
        var tags = tagsInput.data('tags').split(',');
        var autocompleteInput, fakeInput;

        // Remove any extra whitespace from the tags
        tags = $.map(tags, function(tag) {
          return tag.trim();
        });

        // Initialize the Tags Input field
        tagsInput.tagsInput({
          defaultText:      'Add a crop',
          placeholderColor: '#999',
          width:            '210px',

          onAddTag: function(tag) {
            // Remove the tag if it isn't part of the available tags
            // or if the tag has already been added
            if (tags.indexOf(tag) < 0) {
              tagsInput.removeTag(tag);
              return;
            }

            resizeFakeInput();

            // If the "All Crops" tag is added, disable additional
            // input until the "All Crops" tag is removed
            if (tag === 'All Crops') {
              var removeTags = addedTags.slice();
              $.each(removeTags, function() {
                tagsInput.removeTag(this);
              });
              addedTags = [];
            } else {
              if (addedTags.indexOf('All Crops') > -1) {
                tagsInput.removeTag('All Crops');
              }
            }

            // Add to the array of tags added
            addedTags.push(tag);
          },

          onRemoveTag: function(tag) {
            var index = addedTags.indexOf(tag);
            if (index > -1) {
              addedTags.splice(index, 1);
            }
            resizeFakeInput();
          }
        });

        // Set the height of the fake text field so it behaves like
        // overflow-y: hidden and grows with the number of tags.
        fakeInput = $(tagsInput.siblings('.tagsinput'));

        function setupAutocompleteInput() {
          autocompleteInput = $(fakeInput.find('input'));
          var autocomplete = new Awesomplete(autocompleteInput[0], {
            list:     tags,
            maxItems: 9999,
            minChars: 0,

            sort: function(a, b) {
              return a.toLowerCase().localeCompare(b.toLowerCase());
            }
          });

          // Open the auto complete list when the input is focused
          autocompleteInput.focus(function() {
            autocomplete.evaluate();
            autocomplete.open();
          });

          autocompleteInput.bind('awesomplete-select', function(e) {
            // Add the selected tag if it hasn't already been added
            var tag = e.originalEvent.text;
            if (addedTags.indexOf(tag) < 0) {
              tagsInput.addTag(tag);
            }

            // Check to see if an existing tag was attempted to be added
            // and reset the input text instead of marking it as not valid
            setTimeout(function() {
              if (autocompleteInput.hasClass('not_valid')) {
                autocompleteInput.val(autocompleteInput.data('default'));
                autocompleteInput.removeClass('not_valid');
                autocompleteInput.css('color', '#999');
              }
            }, 0);
          });
        }
        setTimeout(setupAutocompleteInput, 0);

        function resizeFakeInput() {
          fakeInput.css('overflow-y', 'hidden');
          fakeInput.css('height', 'auto');
          setTimeout(function() {
            // Manually set the CSS styles to grow the fake input field
            fakeInput.css('height', fakeInput.outerHeight());
            fakeInput.css('overflow-y', 'visible');
            // Reset the autocomplete field to display placeholder
            autocompleteInput.val(autocompleteInput.data('default'));
            autocompleteInput.css('color', '#999');
          }, 0);
        }
        resizeFakeInput();
      });
    }
    activateCropTags();

    $(document).on('cocoon:after-insert', function() {
      activateCropTags();
    });
  });
})();
