(function() {
  "use strict";

  // Activate number formatting of input fields for the first time
  // and any time a nested form partial is added using cocoon
  App.register('component').enter(function() {
    $(document).trigger('refresh_autonumeric');
    $(document).on('cocoon:after-insert', function() {
      $(document).trigger('refresh_autonumeric');
    });
  });
})();
