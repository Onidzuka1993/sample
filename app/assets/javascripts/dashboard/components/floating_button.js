(function() {
  "use strict";

  App.register('component').enter(function() {
    $('.floating-button > .button').click(function() {
      $(this).parent('.floating-button').toggleClass('active');
      return false;
    });
  });
})();
