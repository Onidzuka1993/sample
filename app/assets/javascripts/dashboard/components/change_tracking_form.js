(function() {
  "use strict";

  var changedInputsSelector = '[data-changed="true"]';

  // Log any form errors to the developer console
  // TODO: Handle displaying an error message
  function formAjaxError(event, xhr, status, error) {
    console.error('applications.index.request.fail', xhr, status, error);
  }

  // Show loading indicator on form submit
  function formAjaxSend(event) {
    var form = $(event.target);
    form.addClass('saving');

    var saveButton = form.find('input[type="submit"]');
    saveButton.prop('disabled', true);
    saveButton.val('Saving...');
  }

  // Reset form after successfully updating the application
  function formAjaxSuccess(event, data) {
    // Update form items that aren't replaced by the partial
    var form = $(event.target);
    form.removeClass('saving');
    form.find(changedInputsSelector).each(App.resetChangeTrackingFieldsOriginalValue);

    var saveButton = form.find('input[type="submit"]');
    saveButton.prop('disabled', true);

    // Show message on Save buton to indicated the successful save
    setTimeout(function() {
      saveButton.val('Saved!');
      setTimeout(function() {
        saveButton.val('Save');
      }, 1200);
    }, 300);
  }

  // Enable or disable the Save button based on pending changes
  function formInputChange(event) {
    var form = $(event.target);

    // Find any text inputs that have changed
    var changedInputs = form.find(changedInputsSelector);

    if (changedInputs.length > 0) {
      form.find('input[type="submit"]').prop('disabled', false);
    } else {
      form.find('input[type="submit"]').prop('disabled', true);
    }
  }

  // Handle the input changes on the form and enable/disable the submit button
  // Requires using the "change tracking field component" to receive change events.
  App.activateChangeTrackingForms = function() {
    var forms = $('form.track');

    // Remove existing bindings so they aren't fired twice
    forms.off('input:change', formInputChange);
    forms.off('ajax:send', formAjaxSend);
    forms.off('ajax:success', formAjaxSuccess);
    forms.off('ajax:error', formAjaxError);

    // Add bindings for AJAX success and for tracking input changes
    forms.on('ajax:error', formAjaxError);
    forms.on('ajax:send', formAjaxSend);
    forms.on('ajax:success', formAjaxSuccess);
    forms.on('input:change', formInputChange);
  };

  App.register('component').enter(function() {
    App.activateChangeTrackingForms();
  });
})();
