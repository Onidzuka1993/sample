(function() {
  "use strict";

  var links;

  App.register('component').enter(function() {
    var linkSelectors = [];
    linkSelectors.push('.dashboard-content a:not([data-method="delete"]):not(.add_fields):not([target="_blank"]):not([href="#"])');
    linkSelectors.push('.popover a:not([data-method="delete"]):not(.add_fields):not([target="_blank"]):not([href="#"])');
    links = $(linkSelectors.join(','));

    // Add active class on click to style while loading via turbolinks.
    links.click(function() {
      var link = $(this);
      link.addClass('active');
    });
  }).exit(function() {
    // Remove active class on page load
    links.removeClass('active');
  });
})();
