/* globals HS */
(function() {
  "use strict";

  var navigationLinks;

  App.register('component').enter(function() {
    // Enable the dropdown for the current user menu item.
    var currentUserLink = $('.dashboard-navigation .current-user');
    currentUserLink.click(function() {
      $(this).find('.dropdown').slideToggle(150);
      $('.dashboard-navigation').toggleClass('active-dropdown');
    });

    // Display a loading indicator next to the clicked navigation link
    navigationLinks = $('.dashboard-navigation ul a:not(.external)');
    navigationLinks.click(function() {
      navigationLinks.removeClass('loading');
      $(this).addClass('loading');
    });

    // Use the modal config for the Help Scout beacon
    HS.beacon.config({ modal: true });

    // Add click handler for Help Scout menu item
    $('.help-scout').click(function() {
      HS.beacon.open();
      setTimeout(function() {
        currentUserLink.click();
        $('.help-scout').removeClass('loading');
      }, 200);
      return false;
    });
  }).exit(function() {
    // Remove the classes when the body is unloaded
    navigationLinks.removeClass('loading');
  });
})();
