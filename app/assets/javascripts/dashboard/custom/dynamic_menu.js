$(document).on('turbolinks:load', function() {
    var $fromList = $('#dynamic_menu_from_list');

    $fromList.change(function () {
        var optionToRemove = $(this).val();

        $("#dynamic_menu_to_list option").each(function () {
            $(this).remove();
        });

        $('#dynamic_menu_from_list option').each(function () {
            var value = $(this).val();
            var text  = $(this).text();

            if (jQuery.inArray($(this).val(), optionToRemove) == -1) {
                $("#dynamic_menu_to_list").append($("<option></option>").attr("value", value).text(text))
            }
        });
    });
});