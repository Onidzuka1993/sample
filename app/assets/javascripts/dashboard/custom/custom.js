$(document).on('turbolinks:load', function() {
    function setDatePicker(dateField) {
        $(dateField).pickadate();
    }

    function calculateTotalAmount(cell) {
        var totalAmount = 0;
        $.each($(cell), function (index, element) {
            var CellAmount = parseFloat($(element).html()) || 0.0;

            totalAmount += CellAmount;
        });
        return totalAmount;
    }

    function removeTableLines() {
        var $addedLines = $('.added-line');
        $.each($addedLines, function (key, value) {
            value.remove();
        })
    }

    function addOptionsToSelect(table) {
        var lastSelect = $(table + ' tbody tr:last-child').find('select');

        // TODO temporarily solution
        $.ajax({
            dataType: 'json',
            url: '/accounts'
        }).done(function (data) {
            $.each(data, function (key, value) {
                lastSelect.append($('<option>', {
                    value: value['id'],
                    text: value['name']
                }));
            })
        });
    }

    function createNewJournalEntryLine() {
        var lastColumnNumber    = parseInt($('.journal-entry-table tr:last td:first').html());
        var currentColumnNumber = lastColumnNumber + 1;

        $('.journal-entry-table > tbody:last-child').append('' +
            '<tr class="body added-line">' +
            '<td>' + currentColumnNumber + '</td> ' +
            '<td><select name="account"><option value="">Select Account</option></select></td> ' +
            '<td contenteditable="true"></td> ' +
            '<td contenteditable="true"></td> ' +
            '<td contenteditable="true"></td> ' +
            '<td><a href="" onclick="return false" style="text-decoration: none" class="fa fa-remove journal-entry-table__remove_line"></a></td> ' +
            '</tr>'
        );
    }

    function createNewPrintCheckLine() {
        var lastColumnNumber    = parseInt($('.print-check-table tr:last td:first').html());
        var currentColumnNumber = lastColumnNumber + 1;

        $('.print-check-table > tbody:last-child').append('' +
            '<tr class="body added-line">' +
            '<td>' + currentColumnNumber + '</td> ' +
            '<td><select name="account"><option value="">Select Account</option></select></td> ' +
            '<td contenteditable="true"></td> ' +
            '<td contenteditable="true" class="print-check-table__cell_amount"></td> ' +
            '<td><a href="" onclick="return false" style="text-decoration: none" class="fa fa-remove print-check-table__remove_line"></a></td> ' +
            '</tr>'
        );
    }

    function createNewExpenseLine() {
        var lastColumnNumber    = parseInt($('.expense-table tr:last td:first').html());
        var currentColumnNumber = lastColumnNumber + 1;

        $('.expense-table > tbody:last-child').append('' +
            '<tr class="body added-line">' +
            '<td>' + currentColumnNumber + '</td> ' +
            '<td><select name="account"><option value="">Select Account</option></select></td> ' +
            '<td contenteditable="true"></td> ' +
            '<td contenteditable="true" class="expense-table__cell_amount"></td> ' +
            '<td><a href="" onclick="return false" style="text-decoration: none" class="fa fa-remove expense-table__remove_line"></a></td> ' +
            '</tr>'
        );
    }

    function addJournalEntryLine() {
        $('.journal-entry-table__add_a_line').click(function (e) {
            e.preventDefault();

            createNewJournalEntryLine();
            addOptionsToSelect('.journal-entry-table');
        });
    }

    function addPrintCheckLine() {
        $('.print-check-table__add_a_line').click(function (e) {
            e.preventDefault();

            createNewPrintCheckLine();
            addOptionsToSelect('.print-check-table');
        });
    }

    function addExpenseLine() {
        $('.expense-table__add_a_line').click(function (e) {
            e.preventDefault();

            createNewExpenseLine();
            addOptionsToSelect('.expense-table')
        })
    }

    function clearLines(link) {
        $(link).click(function (e) {
            e.preventDefault();

            removeTableLines();
        });
    }

    function removeJournalTableLast() {
        $('table').on("click", '.journal-entry-table__remove_line', function (e) {
            e.preventDefault();

            if ($('.journal-entry-table a').length > 5) {
                $('.journal-entry-table tr').last().remove();
            }
        });
    }
    
    function removePrintCheckTableLine() {
        $('table').on("click", ".print-check-table__remove_line", function (e) {
            e.preventDefault();

            if ($('.print-check-table a').length > 1) {
                $('.print-check-table tr').last().remove();

                var totalAmount = calculateTotalAmount('.print-check-table__cell_amount');
                $('.print-check-form__total_amount').html(accounting.formatMoney(totalAmount));
            }
        })
    }

    function removeExpenseTableLine() {
        $('table').on("click", ".expense-table__remove_line", function (e) {
            e.preventDefault();

            if ($('.expense-table a').length > 1) {
                $('.expense-table tr').last().remove();

                var totalAmount = calculateTotalAmount('.expense-table__cell_amount');
                $('.expense-form__total_amount').html(accounting.formatMoney(totalAmount));
            }
        })
    }

    function getJournalEntryTableData() {
        var tableData = $('.journal-entry-table').tableToJSON({
            ignoreColumns: [0, 5],
            extractor: {
                1: function (cellIndex, $cell) {
                    return $cell.find('select').val();
                }
            }
        });
        return tableData;
    }

    function showError(jqXHR, errorBoxClass) {
        var response      = jQuery.parseJSON(jqXHR.responseText);
        var $errorBox     = $(errorBoxClass + ' .error');
        var $errorMessage = $('.error__detail_message');

        $errorMessage.html(response['error']);
        $errorBox.show();
        $(document).scrollTop(0);
    }

    function sendJournalEntryData(tableData, url, method, reloadPage) {
        var parsedTableData = JSON.stringify(tableData);
        var happened_at     = $('#happened_at').val();
        var memo            = $('#memo').val();

        var request = $.ajax({
            url: url,
            method: method,
            data: {
                journal_entry: {
                    happened_at:         happened_at,
                    memo:                memo,
                    journal_entry_table: parsedTableData
                }
            },
            datatype: 'json'
        });

        request.done(function (message) {
            $('.journal-entry-form .error').hide();

            if (reloadPage === false) {
                swal("Journal Entry saved!", '', "success");
            } else {
                document.location.reload(true);
                swal("Journal Entry saved!", '', "success");
            }
        });

        request.fail(function (jqXHR, textStatus, error) {
            showError(jqXHR, '.journal-entry-form')
        });

        return request;
    }

    function sendPrintCheckData(tableData, url, method) {
        var parsedTableData = JSON.stringify(tableData);

        var vendorId        = $('.print-check-form__vendor').val();
        var accountId       = $('.print-check-form__account').val();
        var address         = $('.print-check-form__address').val();
        var payment_date    = $('.print-check-form__payment_date').val();
        var memo            = $('.print-check-form__memo').val();

        var request = $.ajax({
            url: url,
            method: method,
            data: {
                check: {
                    vendor_id:         vendorId,
                    account_id:        accountId,
                    payment_date:      payment_date,
                    address:           address,
                    memo:              memo,
                    print_check_table: parsedTableData
                }
            },
            datatype: 'json'
        });

        return request;
    }

    function sendExpenseData(tableData, url, method, reloadPage) {
        var parsedTableData = JSON.stringify(tableData);

        var vendorId        = $('.expense-form__vendor').val();
        var accountId       = $('.expense-form__account').val();
        var paymentDate     = $('.expense-form__payment_date').val();
        var paymentMethod   = $('.expense-form__payment_method').val();
        var memo            = $('.expense-form__memo').val();

        var request = $.ajax({
            url: url,
            method: method,
            data: {
                expense: {
                    vendor_id:       vendorId,
                    account_id:      accountId,
                    payment_date:    paymentDate,
                    payment_method:  paymentMethod,
                    memo:            memo,
                    expense_entries: parsedTableData
                }
            },
            datatype: 'json'
        });

        request.done(function (message) {
            $('.print-check-form .error').hide();

            if (reloadPage === false) {
                swal("Expense saved!", '', "success");
            } else {
                document.location.reload(true);
                swal("Expense saved!", '', "success");
            }
        });

        request.fail(function (jqXHR, textStatus, error) {
            showError(jqXHR, '.expense-form');
        });

        return request;
    }

    function saveJournalEntry() {
        $('#save-journal-entry').click(function (e) {
            e.preventDefault();
            var tableData = getJournalEntryTableData();
            sendJournalEntryData(tableData, '/journal_entries', 'POST', false);
        });
    }

    function saveJournalEntryAndClear() {
        $('#save-journal-entry-and-clear').click(function (e) {
            e.preventDefault();
            var tableData = getJournalEntryTableData();
            sendJournalEntryData(tableData, '/journal_entries', 'POST', true);
        })
    }

    function saveCheck() {
        $('#save-print-check').click(function (e) {
            e.preventDefault();
            var tableData = getTableData('.print-check-table', [0, 4]);
            var request   = sendPrintCheckData(tableData, '/checks', 'POST');

            request.done(function (message) {
                $('.print-check-form .error').hide();

                swal("Print Check saved!", '', "success");
            });

            request.fail(function (jqXHR, textStatus, error) {
                showError(jqXHR, '.print-check-form')
            });
        });
    }

    function assignPrintCheckData(message) {
        $('.check-print-version__vendor-name').html(message['vendor_name']);
        $('.check-print-version__address').html(message['address']);
        $('.check-print-version__total-amount').html(message['total_check_amount']);
        $('.check-print-version__memo').html(message['memo']);
    }

    function saveCheckAndPrint() {
        $('.print-check-form__save_and_print').click(function (e) {
            e.preventDefault();
            $(this).removeClass('active');

            var tableData = getTableData('.print-check-table', [0, 4]);
            var request   = sendPrintCheckData(tableData);

            request.done(function (message) {
                $('.print-check-form .error').hide();

                assignPrintCheckData(message);
                window.print();
                swal("Print Check saved!", '', "success");
            });

            request.fail(function (jqXHR, textStatus, error) {
                showError(jqXHR, '.print-check-form')
            });
        });
    }

    function saveExpense() {
        $('.expense-form__save').click(function (e) {
            e.preventDefault();

            var tableData = getTableData('.expense-table', [0, 4]);
            sendExpenseData(tableData, '/expenses', 'POST', false);
        });
    }

    function saveExpenseAndClear() {
        $('.expense-form__save_and_clear').click(function (e) {
            e.preventDefault();

            var tableData = getTableData('.expense-table', [0, 4]);
            sendExpenseData(tableData, '/expenses', 'POST', true)
        });
    }

    function showBalance(selectInput, balanceClass) {
        $(selectInput).change(function () {
            var accountId = $(this).val();
            var url       = '/accounts/' + accountId;

            if (accountId) {
                var request = $.ajax({
                    url: url,
                    dataType: 'json'
                });

                request.done(function (data) {
                    $(balanceClass).html(accounting.formatMoney(data['balance']))
                });
            } else {
                $(balanceClass).html('$0.00');
            }

        });
    }

    function getTableData(table, ignoreColumns) {
        return $(table).tableToJSON({
            ignoreColumns: ignoreColumns,
            extractor: {
                1: function (cellIndex, $cell) {
                    return $cell.find('select').val();
                }
            }
        });
    }

    function calculateDynamicTableTotalAmount(cell, amountBox) {
        $('table').on("keyup", cell, function () {
            var totalAmount = calculateTotalAmount(cell);
            $(amountBox).html(accounting.formatMoney(totalAmount));
        });
    }

    function renewPrintCheckTable() {
        $('.print-check-table__clear_lines').click(function (e) {
            e.preventDefault();

            removeTableLines();
            var totalAmount = calculateTotalAmount('.print-check-table__cell_amount');
            $('.print-check-form__total_amount').html(accounting.formatMoney(totalAmount));
        });
    }

    function renewExpenseTable() {
        $('.expense-table__clear_lines').click(function (e) {
            e.preventDefault();

            removeTableLines();
            var totalAmount = calculateTotalAmount('.expense-table__cell_amount');
            $('.expense-form__total_amount').html(accounting.formatMoney(totalAmount));
        });
    }

    function changeWindowLocationOnTableRowClick() {
        $('.clickable-row').click(function () {
            window.location = $(this).data("href");
        });
    }

    function updateCheck() {
        $('.print-check-form__update').click(function (e) {
            e.preventDefault();

            var folderId  = $(this).data('folder-id');
            var url       = '/checks/' + folderId;
            var tableData = getTableData('.print-check-table', [0, 4]);
            var request   = sendPrintCheckData(tableData, url, 'PUT');

            request.done(function (message) {
                $('.print-check-form .error').hide();

                swal("Print Check updated!", '', "success");
            });

            request.fail(function (jqXHR, textStatus, error) {
                showError(jqXHR, '.print-check-form')
            });
        });
    }

    function updateExpense() {
        $('.expense-form__update').click(function (e) {
            e.preventDefault();

            var folderId  = $(this).data('folder-id');
            var url       = '/expenses/' + folderId;
            var tableData = getTableData('.expense-table', [0, 4]);
            var request   = sendExpenseData(tableData, url, 'PUT', false);

            request.done(function (message) {
                $('.print-check-form .error').hide();

                swal("Expense updated!", '', "success");
            });

            request.fail(function (jqXHR, textStatus, error) {
                showError(jqXHR, '.print-check-form')
            });
        })
    }

    function updateJournalEntry() {
        $('.journal-entry-form__button__update').click(function (e) {
            e.preventDefault();

            var folderId  = $(this).data('folder-id');
            var url       = '/journal_entries/' + folderId;
            var tableData = getJournalEntryTableData();

            sendJournalEntryData(tableData, url, 'PUT', false);
        })
    }

    changeWindowLocationOnTableRowClick();

    renewPrintCheckTable();
    renewExpenseTable();

    calculateDynamicTableTotalAmount('.print-check-table__cell_amount', '.print-check-form__total_amount');
    calculateDynamicTableTotalAmount('.expense-table__cell_amount', '.expense-form__total_amount');

    setDatePicker('.journal-entry-form__happened_at');
    setDatePicker('.transfer-form__happened_at');
    setDatePicker('.print-check-form__payment_date');
    setDatePicker('.expense-form__payment_date');
    setDatePicker('.reports__balance_sheet_date');

    addJournalEntryLine();
    addPrintCheckLine();
    addExpenseLine();

    removeJournalTableLast();
    removePrintCheckTableLine();
    removeExpenseTableLine();

    clearLines('.journal-entry-table__clear_lines');
    clearLines('.expense-table__clear_lines');

    showBalance('.transfer-form__source_account_id', '.transfer-form__transfer-from');
    showBalance('.transfer-form__target_account_id', '.transfer-form__transfer-to');
    showBalance('.print-check-form__account', '.print-check-form__account_balance');
    showBalance('.expense-form__account', '.expense-form__account_balance');

    saveJournalEntry();
    saveJournalEntryAndClear();
    saveCheck();
    saveCheckAndPrint();
    saveExpense();
    saveExpenseAndClear();

    updateCheck();
    updateExpense();
    updateJournalEntry();
});