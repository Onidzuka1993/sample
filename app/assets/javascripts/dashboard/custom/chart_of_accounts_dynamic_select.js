$(document).on('turbolinks:load', function() {
    var $headerAccountSelect   = $('#header-account-select');
    var $categoryTypeSelect    = $('#category-type-select');
    var $accountCategorySelect = $('#detail_type-select');

    $headerAccountSelect.change(function () {
        var id  = $(this).val();
        var url = '/header_accounts/' + id;

        $.ajax({
            dataType: 'json',
            url: url
        }).done(function (data) {
            $categoryTypeSelect.empty();
            $accountCategorySelect.empty();
            $categoryTypeSelect.append($('<option>', {value: '', text: 'Please choose sub account'}));

            $.each(data, function (key, value) {
                $categoryTypeSelect.append($('<option>', {
                    value: value['id'],
                    text: value['name']
                }));
            })
        })
    });

    $categoryTypeSelect.change(function () {
        var id  = $(this).val();
        var url = '/category_types/' + id;

        $.ajax({
            dataType: 'json',
            url: url
        }).done(function (data) {
            $accountCategorySelect.empty();
            $accountCategorySelect.append($('<option>', {value: '', text: 'Please choose detail type'}));

            $.each(data, function (key, value) {
                $accountCategorySelect.append($('<option>', {
                    value: value['id'],
                    text: value['name']
                }))
            })
        })
    });
});