(function() {
  "use strict";

  // Update the cash price calculations when the user changes the basis or futures price values.
  function activateBehavior() {
    Vue.component('pikaday-binding', App.PikadayBinding);
    new Vue({ el: $('.dashboard-content form')[0] });

    var basis, cashPrice, discountOrPremium, futuresPrice;

    // Grab each of the input elements
    var basisInput = $('input.basis');
    var cashPriceInput = $('input.cash-price');
    var discountPremiumInput = $('input.discount-premium');
    var futuresPriceInput = $('input.futures-price');

    // Update the cash price input with the futures price + basis + discount/premium
    function updateCashPrice() {
      basis = parseFloat(basisInput.autoNumeric('get'), 10);
      futuresPrice = parseFloat(futuresPriceInput.autoNumeric('get'), 10);

      if (discountPremiumInput.length > 0) {
        discountOrPremium = parseFloat(discountPremiumInput.autoNumeric('get'), 10);
      }

      if (isNaN(discountOrPremium)) {
        discountOrPremium = 0;
      }

      if (!isNaN(basis) && !isNaN(futuresPrice)) {
        cashPrice = basis + futuresPrice + discountOrPremium;
        cashPriceInput.autoNumeric('set', cashPrice);
      }
    }
    updateCashPrice();

    // Add event handlers for text input changes
    basisInput.bind('propertychange change click keyup input paste', updateCashPrice);
    cashPriceInput.bind('propertychange change click keyup input paste', updateCashPrice);
    discountPremiumInput.bind('propertychange change click keyup input paste', updateCashPrice);
    futuresPriceInput.bind('propertychange change click keyup input paste', updateCashPrice);
  }

  App.register('contracts.new').enter(function() {
    activateBehavior();
  });

  App.register('contracts.edit').enter(function() {
    activateBehavior();
  });
})();
