(function() {
  "use strict";

  // Add crop summary above list of contracts
  App.register('contracts.index').enter(function() {
    // Create crop summary view
    App.resultsView = new App.CropsSummaryDashboard({
      el: $('#crops-summary div')[0]
    });
    var pattern = /\/\d+\/crops\/(\d+)/g;
    var cropId = pattern.exec(window.location.pathname);
    App.loadResultsApplications({ filter: { 'crop_id': cropId } });


    $(".pdf-export").click(function(e) {
      var url = window.location.pathname + '.json';
      var request = $.ajax(url);
      request.done(function(json, textStatus, xhr) {
        App.pdfGenerator.downloadContracts(json, App.resultsView);
      });

      request.fail(function(xhr, textStatus, error) {
        console.error('contracts.index.request.fail', xhr, textStatus, error);
      });
    });
  }).exit(function() {
    App.resultsView = null;
    App.cancelLoadResultsApplications();
  });
})();
