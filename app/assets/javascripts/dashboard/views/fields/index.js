/* globals Turbolinks */
(function() {
  "use strict";

  App.register('fields.index').enter(function() {
    $('.owner.filter select').change(function() {
      var owner = $(this).val();
      var path = window.location.pathname;

      if (owner.trim() !== '') {
        path += '?owner=' + owner;
      }

      Turbolinks.visit(path);
    });
  });
})();
