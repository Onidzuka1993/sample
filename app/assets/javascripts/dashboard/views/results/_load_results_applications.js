(function() {
  "use strict";

  // Return the Snapshot with the given ID from the included array.
  function findSnapshot(included, id) {
    var record;
    for (var i = 0; i < included.length; i++) {
      record = included[i];
      if (record['type'] === 'snapshots' && record['id'] + '' === id + '') {
        return record;
      }
    }
  }

  // Parses the Applications JSON and deserializes relationships.
  function parseApplicationsJson(json) {
    var applications = json.data,
        application, snapshot;

    for (var i = 0; i < applications.length; i++) {
      application = applications[i];
      snapshot = application['relationships']['snapshot_for_yesterday'];
      if (snapshot != null && snapshot['data'] != null) {
        var snapshotId = application['relationships']['snapshot_for_yesterday']['data']['id'];
        snapshot = findSnapshot(json['included'], snapshotId);
        application['relationships']['snapshot_for_yesterday']['data'] = snapshot;
      }
    }

    return applications;
  }

  // Load the applications data for the current results view
  var request;
  App.loadResultsApplications = function(options) {
    if (App.resultsView === undefined || App.resultsView === null) {
      return;
    }

    App.beginTransaction('load-results-applications');

    if (!App.resultsView.loading) {
      $('.budget-price-switch .fa-cog').addClass('fa-spin');
    }

    if (options === undefined) {
      options = {
        include: 'snapshot_for_yesterday'
      };
    }

    var year = window.location.pathname.substring(1, 5);
    var url = '/api/v1/applications.json?year_id=' + year;

    if (options.filter !== undefined) {
      var keys = Object.keys(options.filter);
      for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var value = options.filter[key];
        url += '&filter[' + key + ']=' + value;
      }
    }

    if (options.include !== undefined) {
       url += '&include=' + options.include;
    }

    // Make the API request to load the applications data
    request = $.ajax(url);
    request.done(function(json, textStatus, xhr) {
      // Initialize the profit/loss tables with the applications data
      App.resultsView.applications = parseApplicationsJson(json);
      App.resultsView.loading = false;
      App.endTransaction('load-results-applications');
    });

    request.fail(function(xhr, textStatus, error) {
      App.failTransaction('load-results-applications');
      console.error('results.show.request.fail', xhr, textStatus, error);
    });

    request.always(function() {
      $('.budget-price-switch .fa-cog').removeClass('fa-spin');
    });
  };

  App.cancelLoadResultsApplications = function() {
    request.abort();
    App.cancelTransaction('load-results-applications');
  };
})();
