(function() {
  "use strict";

  App.register('results.show').enter(function() {
    App.resultsView = new Vue({
      el: '#results',

      components: {
        'profit-loss-by-field-table': App.profitLossByFieldTable
      },

      computed: {
        allResults: function() {
          var applications = this.applications;
          var self = this;
          return applications.map(function(application) {
            return new App.Result({
              data: {
                application:    application,
                applications:   applications,
                inputIndex:     self.parsedInputIndex,
                useBudgetPrice: false
              }
            });
          });
        },

        canPrint: function() {
          return navigator.userAgent.indexOf('Firefox') < 0;
        },

        results: function() {
          var selectedOwner = this.selectedOwner;
          var useBudgetPrice = this.useBudgetPrice;

          return this.allResults.filter(function(result, index) {
            result.useBudgetPrice = useBudgetPrice;
            if (selectedOwner === 'All Owners') {
              return true;
            } else {
              return result.application.attributes.field.owner === selectedOwner;
            }
          });
        },
        parsedInputIndex: function() {
          var parsed = JSON.parse(this.inputIndex);
          if (Object.keys(parsed).length < 1) {
            parsed["other"] = "Other";
          }
          return parsed
        }
      },

      data: function() {
        return {
          applications:   [],
          loading:        true,
          selectedOwner:  'All Owners',
          useBudgetPrice: false
        };
      },

      methods: {
        exportPage: function(format) {
          if (this.applications.length < 1) { return; }

          switch (format) {
            case 'csv':
              App.csvGenerator.downloadPnlByField(this);
              break;
            case 'pdf':
              App.pdfGenerator.downloadPnlByField(this, null, 'profit-loss-by-field.pdf');
              break;
            default:
              App.pdfGenerator.downloadPnlByField(this, null, 'profit-loss-by-field.pdf');
          }
        },
        download: function() {
          if (this.applications.length < 1) { return; }
          App.pdfGenerator.downloadPnlByField(this, null, 'profit-loss-by-field.pdf');
        },

        print: function() {
          if (this.applications.length < 1) { return; }
          App.pdfGenerator.printPnlByField(this, null, 'print-pdf-iframe');
        }
      },
      props: ['inputIndex', 'entity']
    });

    App.loadResultsApplications();
  }).exit(function() {
    App.resultsView = null;
    App.cancelLoadResultsApplications();
  });
})();
