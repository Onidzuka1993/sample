/* jshint camelcase: false */
(function() {
  "use strict";

  App.cropProductionProperties = {
    percentageSold: function() {
      if (this.production === 0) {
        return 0;
      } else {
        return this.productionSold / this.production * 100;
      }
    },

    percentageUnsold: function() {
      if (this.production === 0) {
        return 100;
      } else {
        return this.productionUnsold / this.production * 100;
      }
    },

    production: function() {
      if (this.useUserEnteredYield) {
        return this.yieldInputValue * this.acres;
      }else{
        return this.results.map(function(result) {
          return result.production;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      }
    },

    productionSold: function() {
      return this.results.map(function(result) {
        return result.productionSold;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    productionUnsold: function() {
      return this.results.map(function(result) {
        return result.productionUnsold;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    yield: function() {
      return this.production / this.acres;
    },
    yieldInputValue: {
      get: function() {
        if (this.useUserEnteredYield) {
          // Make sure the number can be formatted properly by ensuring
          // user entered yield is a number no matter what they enter.
          var userEnteredYield = this.userEnteredYield + '';
          userEnteredYield = userEnteredYield.replace(/[^0123456789.]/g, '');
          userEnteredYield = numeral(userEnteredYield).value();
          return numeral(userEnteredYield).value();
        }else{

          return numeral(Math.round(this.yield * 1000) / 1000).value();
        }
      },

      set: function(value) {
        // Parse and update the user entered yield
        value = value + '';
        this.userEnteredYield = numeral(value).value();
        this.useUserEnteredYield = true;
      }
    }
  };
})();
