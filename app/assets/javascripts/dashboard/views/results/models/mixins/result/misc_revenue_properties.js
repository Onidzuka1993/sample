/* jshint camelcase: false */
(function() {
  "use strict";

  // Returns the total production for a group of crops.
  function percentageOfProductionForAGroupOfCrops(result, cropIds) {
    var production = App.productionForApplication(result.application);
    var totalProduction = result.applications.map(function(application) {
      if (cropIds.indexOf(application.attributes.crop.id) > -1) {
        return App.productionForApplication(application);
      } else {
        return 0;
      }
    }).reduce(function(previous, current) {
      return previous + current;
    }, 0);

    return production / totalProduction;
  }

  // Returns the total production for a group of crops.
  function percentageOfAcresForAGroupOfCrops(result, cropIds) {
    var planted_acres = result.application.attributes.planted_acres;
    var totalAcres = result.applications.map(function(application) {
      if (cropIds.indexOf(application.attributes.crop.id) > -1) {
        return application.attributes.planted_acres;
      } else {
        return 0;
      }
    }).reduce(function(previous, current) {
      return previous + current;
    }, 0);

    return planted_acres / totalAcres;
  }

  App.resultMiscRevenueProperties = {
    miscRevenue: function() {
      var cropId = this.application.attributes.crop.id;
      var percentageOfCropAcres = this.percentageOfCropAcres;
      var percentageOfTotalAcres = this.percentageOfTotalAcres;
      var result = this;
      var revenues = this.application.attributes.revenues;

      return revenues.map(function(revenue) {
        var revenueInDollars = revenue.amount_as_float;
        var cropIds = revenue.crops.map(function(crop) {
          return crop.id;
        });

        // The revenue applies to all crops
        if (cropIds.length === 0) {
          return revenueInDollars * percentageOfTotalAcres;
        }

        // The revenue doesn't apply to this field's crop
        if (cropIds.indexOf(cropId) < 0) {
          return 0;
        }

        // The revenue only applies to this field's crop
        if (cropIds.length === 1) {
          return revenueInDollars * percentageOfCropAcres;
        }

        // The revenue applies to this field's crop and additional crops
        return revenueInDollars * percentageOfAcresForAGroupOfCrops(result, cropIds);
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    }
  };
})();
