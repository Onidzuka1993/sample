/* jshint camelcase: false */
(function() {
  "use strict";

  // Returns the application's production based on yield, planted acres, and crop share percentage.
  App.productionForApplication = function(application) {
    var applicationYield = application.attributes.yield;
    if (applicationYield === null || applicationYield === undefined) {
      applicationYield = 0;
    }
    var cropSharePercentage = application.attributes.crop_share_percentage;
    var plantedAcres = application.attributes.planted_acres;

    if (cropSharePercentage > 0) {
      return applicationYield * ((100 - cropSharePercentage) / 100) * plantedAcres;
    } else {
      return applicationYield * plantedAcres;
    }
  };
})();
