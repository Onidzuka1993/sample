/* jshint camelcase: false */
(function() {
  "use strict";

  App.resultProductionProperties = {
    cropProduction: function() {
      var applications = this.applications;
      var cropId = this.application.attributes.crop.id;
      var cropProduction = applications.filter(function(application) {
        return cropId === application.attributes.crop.id;
      }).map(function(application) {
        return App.productionForApplication(application);
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);

      if (isNaN(cropProduction)) {
        console.error('cropProduction is not a number', 'applications', applications);
      }

      return cropProduction;
    },

    cropAcres: function() {
      var applications = this.applications;
      var cropId = this.application.attributes.crop.id;
      var cropAcres = applications.filter(function(application) {
        return cropId === application.attributes.crop.id;
      }).map(function(application) {
        return application.attributes.planted_acres;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);

      return cropAcres;
    },

    totalAcres: function() {
      var application = this.application;
      var applications = this.applications;
      var totalAcres = applications.map(function(application) {
        return application.attributes.planted_acres;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);


      return totalAcres;
    },

    percentageOfCropProduction: function() {
      var cropProduction = this.cropProduction;
      if (cropProduction <= 0) {
        return 1;
      }
      return this.production / cropProduction;
    },

    percentageOfTotalProduction: function() {
      var totalProduction = this.totalProduction;
      if (totalProduction <= 0) {
        return 1;
      }
      return this.production / totalProduction;
    },

    percentageOfCropAcres: function() {
      var cropAcres = this.cropAcres;
      if (cropAcres <= 0) {
        return 1;
      }
      return this.application.attributes.planted_acres / cropAcres;
    },

    percentageOfTotalAcres: function() {
      var totalAcres = this.totalAcres;
      if (totalAcres <= 0) {
        return 1;
      }

      return this.application.attributes.planted_acres / totalAcres;
    },

    production: function() {
      return App.productionForApplication(this.application);
    },

    productionSold: function() {
      var productionSold = this.closedContracts.map(function(contract) {
        return contract.amount_sold;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);

      if (isNaN(productionSold)) {
        console.error('productionSold is not a number', 'this.closedContracts', this.closedContracts);
      }

      return productionSold * this.percentageOfCropProduction;
    },

    productionUnsold: function() {
      return this.production - this.productionSold;
    },

    totalProduction: function() {
      var application = this.application;
      var applications = this.applications;
      var totalProduction = applications.map(function(application) {
        return App.productionForApplication(application);
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);

      if (isNaN(totalProduction)) {
        console.error('totalProduction is not a number', 'applications', applications);
      }

      return totalProduction;
    },

    yield: function() {
      var applicationYield = this.application.attributes.yield;
      if (applicationYield === null || applicationYield === undefined) {
        applicationYield = 0;
      }
      return applicationYield;
    }
  };
})();
