/* jshint camelcase: false */
/* globals numeral */
(function() {
  "use strict";

  App.cropRevenuesProperties = {
    actualCropRevenue: function() {
      return this.results.map(function(result) {
        return result.cropRevenue;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    averageCashPrice: function() {
      var res = 0;
      if (this.production != null && this.production >= 1) {
        res = this.actualCropRevenue / this.production;
      }
      return res;
    },

    averageRawCashPriceInputValue: {
      get: function() {
        // Make sure the number can be formatted properly by ensuring
        // user entered cash price is a number no matter what they enter.
        if (this.useUserEnteredCashPrice) {
          var userEnteredCashPrice = this.userEnteredCashPrice + '';
          userEnteredCashPrice = userEnteredCashPrice.replace(/[^0123456789.]/g, '');
          userEnteredCashPrice = numeral(userEnteredCashPrice).value();
          return userEnteredCashPrice;
        }else{
          this.userEnteredCashPrice = this.averageCashPrice;
          return this.averageCashPrice;
        }
      },

      set: function(value) {
        // Parse and update the user entered cash price
        value = value + '';
        var userEnteredCashPrice = numeral(value.replace('$', '')).value();
        this.userEnteredCashPrice = userEnteredCashPrice;
        this.useUserEnteredCashPrice = true;
      }
    },

    averageCashPriceInputValue: {
      get: function() {
        return numeral(this.averageRawCashPriceInputValue).format('$0,0.000');
      },

      set: function(value) {
        // Parse and update the user entered cash price
        this.averageRawCashPriceInputValue = value;
      }
    },

    averageSellingPrice: function() {
      var contractsRevenue = this.results.map(function(result) {
        return result.contractsRevenue;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);

      var productionSold = this.productionSold;
      if (productionSold === 0) {
        productionSold = 1;
      }

      return contractsRevenue / productionSold;
    },

    cropRevenue: function() {
      return this.production * this.averageRawCashPriceInputValue;
    },

    otherRevenue: function() {
      return this.results.map(function(result) {
        return result.otherRevenue;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    revenue: function() {
      return this.cropRevenue + this.otherRevenue;
    }
  };
})();
