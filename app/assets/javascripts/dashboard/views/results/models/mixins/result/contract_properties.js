/* jshint camelcase: false */
(function() {
  "use strict";

  App.resultContractProperties = {
    closedContracts: function() {
      return this.application.attributes.contracts.filter(function(contract) {
        return contract.type !== 'Contracts::Basis::Contract' && contract.open_order !== true;
      });
    },

    openContracts: function() {
      return this.application.attributes.contracts.filter(function(contract) {
        return contract.type === 'Contracts::Basis::Contract' || contract.open_order === true;
      });
    },

    openContractsAmountSold: function() {
      var openContractsAmountSold = this.openContracts.map(function(contract) {
        return contract.amount_sold;
      }).reduce(function(previous, next) {
        return previous + next;
      }, 0);

      if (isNaN(openContractsAmountSold)) {
        console.error('openContractsAmountSold is not a number', 'this.openContracts', this.openContracts);
      }

      return openContractsAmountSold * this.percentageOfCropProduction;
    }
  };
})();
