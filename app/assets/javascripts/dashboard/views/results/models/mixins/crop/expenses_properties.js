/* jshint camelcase: false */
(function() {
  "use strict";

  App.cropExpensesProperties = {
    inputs: function() {
      var calcInputs = {};

      $.each(this.results, function(key, result) {
        $.each(result.inputs, function(k,v) {
          calcInputs[k] = (calcInputs[k] || 0) + v;
        });
      });

      return calcInputs;
    },
    chemicalCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.chemical_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    costPerBushel: function() {
      if (this.production == null || this.production < 1) {
        return 0;
      }
      return this.expenses / this.production;
    },

    cropInsuranceCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.crop_insurance_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    dryingCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.drying_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    expenses: function() {
      return this.results.map(function(result) {
        return result.expenses;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    fertilizerCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.fertilizer_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    fuelCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.fuel_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    laborCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.labor_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    landCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.land_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    otherCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.other_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    paymentsDepreciationCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.payments_depreciation_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    repairCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.repair_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    seedCost: function() {
      return this.results.map(function(result) {
        return result.application.attributes.seed_cost;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    }
  };
})();
