/* jshint camelcase: false */
(function() {
  "use strict";

  App.resultExpensesProperties = {
    inputs: function() {
      var self = this;
      var calcInputs = {};
      var otherExpense = 0;
      $.each(self.inputIndex, function(key, value) {
        var totalInput = self.application.attributes.inputs[key];
        if (totalInput !== undefined) {
          calcInputs[key] = totalInput;
        }else{
          calcInputs[key] = 0;
        }
      });

      $.each(self.application.attributes.inputs, function(k,v) {
        if (self.inputIndex[k] === undefined) {
          otherExpense += v;
        }
      });

      calcInputs["other"] = (calcInputs["other"] || 0) + otherExpense;

      return calcInputs;
    },
    chemicalCost: function() {
      return this.application.attributes.chemical_cost;
    },

    costPerBushel: function() {
      return this.expenses / this.production;
    },

    cropInsuranceCost: function() {
      return this.application.attributes.crop_insurance_cost;
    },

    dryingCost: function() {
      return this.application.attributes.drying_cost;
    },

    fertilizerCost: function() {
      return this.application.attributes.fertilizer_cost;
    },

    fuelCost: function() {
      return this.application.attributes.fuel_cost;
    },

    laborCost: function() {
      return this.application.attributes.labor_cost;
    },

    landCost: function() {
      return this.application.attributes.land_cost;
    },

    otherCost: function() {
      return this.application.attributes.other_cost;
    },

    paymentsDepreciationCost: function() {
      return this.application.attributes.payments_depreciation_cost;
    },

    repairCost: function() {
      return this.application.attributes.repair_cost;
    },

    seedCost: function() {
      return this.application.attributes.seed_cost;
    }
  };
})();
