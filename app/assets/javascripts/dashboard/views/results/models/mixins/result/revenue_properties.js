/* jshint camelcase: false */
(function() {
  "use strict";

  App.resultRevenueProperties = {
    basis: function() {
      return this.application.attributes.basis;
    },

    budgetPrice: function() {
      return this.application.attributes.budget_price;
    },

    cashPrice: function() {
      var missingFuturesPrice = this.futuresPrice === undefined || this.futuresPrice === null || this.futuresPrice === 0;

      if (this.userEnteredCashPrice > 0 && !this.useBudgetPrice) {
        return this.userEnteredCashPrice;
      } else if (this.useBudgetPrice || missingFuturesPrice) {
        return this.budgetPrice;
      } else {
        return this.futuresPrice + this.basis;
      }
    },

    contractsRevenue: function() {
      var contractsRevenue = this.closedContracts.map(function(contract) {
        return contract.cash_price_amount_as_float * contract.amount_sold;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);

      if (isNaN(contractsRevenue)) {
        console.error('contractsRevenue is not a number', 'this.closedContracts', this.closedContracts);
      }

      return contractsRevenue * this.percentageOfCropProduction;
    },

    cropRevenue: function() {
      return this.contractsRevenue + this.unsoldRevenue;
    },

    futuresPrice: function() {
      return this.application.attributes.futures_price;
    },

    income: function() {
      var income = this.revenue - this.expenses;
      return income;
    },

    miscRevenueLineItemTotal: function() {
      return this.application.attributes.misc_revenue;
    },

    otherRevenue: function() {
      return this.miscRevenue + this.miscRevenueLineItemTotal;
    },

    revenue: function() {
      return this.cropRevenue + this.otherRevenue;
    },

    unsoldRevenue: function() {
      return this.productionUnsold * this.cashPrice;
    }
  };
})();
