//= require_tree ./mixins
/* jshint camelcase: false */
/* globals Vue */
(function() {
  "use strict";

  var computed = {
    acres: function() {
      return this.plantedAcres;
    },

    breakEven: function() {
      if (this.production === 0) {
        return 0;
      } else {
        return this.expenses / this.production;
      }
    },

    breakevenYield: function() {
      return (this.expenses / this.acres) / (this.revenue / this.production);
    },

    cropName: function() {
      return this.application.attributes.crop.name;
    },

    expenses: function() {
      var total = this.landCost || 0;
      $.each(this.inputs, function(key, amount) {
        total += amount;
      })

      return total;
    },

    fieldName: function() {
      return this.application.attributes.field.name;
    },

    plantedAcres: function() {
      return this.application.attributes.planted_acres;
    },

    resultFromYesterday: function() {
      return new App.Result({
        data: {
          application: this.application.relationships.snapshot_for_yesterday.data,
          applications: this.applications.map(function(application) {
            return application.relationships.snapshot_for_yesterday.data;
          }).filter(function(application) {
            return application !== null;
          }),
          useBudgetPrice: this.useBudgetPrice,
          inputIndex: this.inputIndex
        }
      });
    }
  };

  var key;

  for (key in App.resultContractProperties) {
    computed[key] = App.resultContractProperties[key];
  }

  for (key in App.resultExpensesProperties) {
    computed[key] = App.resultExpensesProperties[key];
  }

  for (key in App.resultMiscRevenueProperties) {
    computed[key] = App.resultMiscRevenueProperties[key];
  }

  for (key in App.resultProductionProperties) {
    computed[key] = App.resultProductionProperties[key];
  }

  for (key in App.resultRevenueProperties) {
    computed[key] = App.resultRevenueProperties[key];
  }

  App.Result = Vue.extend({
    computed: computed,
    data: function() {
      return { userEnteredCashPrice: 0 };
    }
  });
})();
