//= require_tree ./mixins
/* jshint camelcase: false */
/* globals Vue */

(function() {
  "use strict";

  var computed = {
    acres: function() {
      return this.results.map(function(result) {
        return result.plantedAcres;
      }).reduce(function(previous, current) {
        return previous + current;
      }, 0);
    },

    breakEven: function() {
      if (this.production === 0) {
        return 0;
      } else {
        return this.expenses / this.production;
      }
    },

    costPerAcre: function() {
      if (this.acres === 0) {
        return 0;
      } else {
        return this.expenses / this.acres;
      }
    },

    costPerUnit: function() {
      if (this.acres === 0) {
        return 0;
      } else if (this.production === 0) {
        return 0;
      } else {
        return this.expenses / this.production;
      }
    },

    income: function() {
      return this.revenue - this.expenses;
    },

    incomePerAcre: function() {
      if (this.acres === 0) {
        return 0;
      } else {
        return this.income / this.acres;
      }
    },

    progressWidth: function() {
      var pWidth = ((this.productionSold / this.production) * 100);
      if (pWidth > 100) {
        return 'width: 100%;';
      }else{
        return 'width: ' + pWidth + '%;';
      }
    },

    results: function() {
      var cropName = this.name;
      return this.allResults.filter(function(result) {
        return result.cropName === cropName;
      });
    },

    breakevenYield: function() {
      return (this.expenses / this.acres) / (this.revenue / this.production);
    }
  };

  var key;

  for (key in App.cropExpensesProperties) {
    computed[key] = App.cropExpensesProperties[key];
  }

  for (key in App.cropProductionProperties) {
    computed[key] = App.cropProductionProperties[key];
  }

  for (key in App.cropRevenuesProperties) {
    computed[key] = App.cropRevenuesProperties[key];
  }

  App.Crop = Vue.extend({
    computed: computed
  });
})();
