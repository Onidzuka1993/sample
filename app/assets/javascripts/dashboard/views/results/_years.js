/* globals Turbolinks */
(function() {
  "use strict";

  // Activate the All Years toggle switch
  App.register('results').enter(function() {
    $('.all-years input').change(function() {
      $('.all-years').addClass('loading');

      var showAllYears = $('.all-years input').prop('checked');
      var year = window.location.pathname.replace(/\D/g, '');
      var path = '/' + year + '/results';

      if (showAllYears) {
        path += '/all';
      }

      Turbolinks.visit(path + window.location.search);
    });
  });
})();
