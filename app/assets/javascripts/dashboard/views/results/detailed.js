(function() {
  "use strict";

  App.register('results.detailed').enter(function() {
    App.resultsView = new Vue({
      el: '#results',

      components: {
        'profit-loss-by-crop-table':   App.profitLossByCropTable,
        'profit-loss-breakdown-table': App.profitLossBreakdownTable
      },

      computed: {
        allResults: function() {
          var applications = this.applications;
          var inputIndex = this.parsedInputIndex;
          return applications.map(function(application) {
            return new App.Result({
              data: {
                application:    application,
                applications:   applications,
                useBudgetPrice: false,
                inputIndex: inputIndex
              }
            });
          });
        },

        canPrint: function() {
          return navigator.userAgent.indexOf('Firefox') < 0;
        },

        results: function() {
          var selectedOwner = this.selectedOwner;
          var useBudgetPrice = this.useBudgetPrice;

          return this.allResults.filter(function(result, index) {
            result.useBudgetPrice = useBudgetPrice;
            if (selectedOwner === 'All Owners') {
              return true;
            } else {
              return result.application.attributes.field.owner === selectedOwner;
            }
          });
        },
        parsedInputIndex: function() {
          var parsed = JSON.parse(this.inputIndex);
          if (Object.keys(parsed).length < 1) {
            parsed["other"] = "Other";
          }
          return parsed
        }
      },

      data: function() {
        return {
          applications:     [],
          loading:          true,
          selectedOwner:    'All Owners',
          useBudgetPrice:   false,
          usePerAcreValues: false
        };
      },

      methods: {
        exportPage: function(format) {
          if (this.applications.length < 1) { return; }

          switch (format) {
            case 'csv':
              App.csvGenerator.downloadPnlByCrop(this);
              break;
            case 'pdf':
              App.pdfGenerator.downloadPnlByCrop(this, null, 'profit-loss-by-crop.pdf');
              break;
            default:
              App.pdfGenerator.downloadPnlByCrop(this, null, 'profit-loss-by-crop.pdf');
          }
        },
        download: function() {
          if (this.applications.length < 1) { return; }
          App.pdfGenerator.downloadPnlByCrop(this, null, 'profit-loss-by-crop.pdf');
        },

        print: function() {
          if (this.applications.length < 1) { return; }
          App.pdfGenerator.printPnlByCrop(this, null, 'print-pdf-iframe');
        }
      },
      props: ['inputIndex', 'entity']
    });

    App.loadResultsApplications();
  }).exit(function() {
    App.resultsView = null;
    App.cancelLoadResultsApplications();
    $('#print-pdf-iframe')[0].src = '';
  });
})();
