/* globals Vue, numeral */

(function() {
  "use strict";

  App.profitLossBreakdownTable = Vue.extend({
    components: {
      'crop-column': App.CropColumn
    },

    computed: {
      acres: function() {
        return this.crops.map(function(crop) {
          return crop.acres;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      chemicalCost: function() {
        return this.crops.map(function(crop) {
          return crop.chemicalCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      cropInsuranceCost: function() {
        return this.crops.map(function(crop) {
          return crop.cropInsuranceCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      cropRevenue: function() {
        return this.crops.map(function(crop) {
          return crop.cropRevenue;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      crops: function() {
        var crops = [];
        var cropNames = [];
        var crop, cropName, cropIndex, result;

        // Loop through the results to find the unique crops
        for (var i = 0; i < this.results.length; i++) {
          result = this.results[i];
          cropName = result.cropName;
          cropIndex = cropNames.indexOf(cropName);
          // Only add each crop once
          if (cropIndex < 0) {
            crop = new App.Crop({
              data: {
                allResults:              this.results,
                name:                    cropName,
                userEnteredCashPrice:    -1,
                userEnteredYield:        -1,
                useUserEnteredYield:     false,
                useUserEnteredCashPrice: false,
              }
            });
            crops.push(crop);
            cropNames.push(cropName);
          }
        }

        return crops;
      },

      inputs: function() {
        var calcInputs = {};

        $.each(this.crops, function(key, crop) {
          $.each(crop.inputs, function(k,v) {
            calcInputs[k] = (calcInputs[k] || 0) + v;
          });
        });

        return calcInputs;
      },
      dryingCost: function() {
        return this.crops.map(function(crop) {
          return crop.dryingCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      expenses: function() {
        return this.crops.map(function(crop) {
          return crop.expenses;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      fertilizerCost: function() {
        return this.crops.map(function(crop) {
          return crop.fertilizerCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      fuelCost: function() {
        return this.crops.map(function(crop) {
          return crop.fuelCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      income: function() {
        return this.revenue - this.expenses;
      },

      laborCost: function() {
        return this.crops.map(function(crop) {
          return crop.laborCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      landCost: function() {
        return this.crops.map(function(crop) {
          return crop.landCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      otherCost: function() {
        return this.crops.map(function(crop) {
          return crop.otherCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      otherRevenue: function() {
        return this.crops.map(function(crop) {
          return crop.otherRevenue;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      paymentsDepreciationCost: function() {
        return this.crops.map(function(crop) {
          return crop.paymentsDepreciationCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      repairCost: function() {
        return this.crops.map(function(crop) {
          return crop.repairCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      revenue: function() {
        return this.crops.map(function(crop) {
          return crop.revenue;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      seedCost: function() {
        return this.crops.map(function(crop) {
          return crop.seedCost;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      usePerAcreValues: function() {
        return this.$root.usePerAcreValues;
      }
    },

    props:    ['results', 'inputIndex'],
    methods:  {
      numeral: numeral,
      fetchTotal: function() {

      }
    },
    template: '#profit-loss-breakdown-table'
  });
})();
