/* globals numeral, Vue */

(function() {
  "use strict";

  App.CropRow = Vue.extend({
    methods:  { numeral: numeral },
    props:    ['crop'],
    template: '#crop-row'
  });
})();
