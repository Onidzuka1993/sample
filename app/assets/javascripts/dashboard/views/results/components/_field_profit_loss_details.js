/* globals numeral, Vue */

(function() {
  "use strict";

  App.FieldProfitLossDetails = Vue.extend({
    methods:  { numeral: numeral },
    props:    ['result', 'inputIndex'],
    template: '#field-profit-loss-details'
  });
})();
