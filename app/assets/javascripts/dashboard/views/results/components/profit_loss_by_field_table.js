/* globals numeral, Vue */

(function() {
  "use strict";

  App.profitLossByFieldTable = Vue.extend({
    components: {
      'result-row':      App.ResultRow,
      'sortable-header': App.SortableHeader
    },

    computed: {
      expenses: function() {
        return this.results.map(function(result) {
          return result.expenses;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      income: function() {
        return this.results.map(function(result) {
          return result.income;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      production: function() {
        return this.results.map(function(result) {
          return result.production;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      revenue: function() {
        return this.results.map(function(result) {
          return result.revenue;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      selectedOwner: function() {
        return this.$root.selectedOwner;
      }
    },

    data: function() {
      return {
        sortBy:        'fieldName',
        sortDirection: 1
      };
    },

    methods:  {
      numeral: numeral
     },
    props:    ['results', 'inputIndex'],
    template: '#profit-loss-by-field-table'
  });
})();
