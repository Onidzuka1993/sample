/* globals Vue */

(function() {
  "use strict";

  App.SortableHeader = Vue.extend({
    props:    ['class', 'content', 'sort-by'],
    template: '#sortable-header',

    computed: {
      cssClass: function() {
        var cssClass = this.class;

        if (this.sortBy === this.$parent.sortBy) {
          cssClass += ' current';
          if (this.$parent.sortDirection === -1) {
            cssClass += ' desc';
          } else {
            cssClass += ' asc';
          }
        }

        return cssClass;
      }
    },

    methods: {
      sort: function() {
        var oldSortBy = this.$parent.sortBy;
        var newSortBy = this.sortBy;

        if (newSortBy === oldSortBy) {
          this.$parent.sortDirection = -this.$parent.sortDirection;
        } else {
          this.$parent.sortBy = newSortBy;
          this.$parent.sortDirection = 1;
        }
      }
    }
  });
})();
