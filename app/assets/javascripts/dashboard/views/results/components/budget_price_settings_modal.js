(function() {
  "use strict";

  // Hide the budget price settings modal when the close button is clicked
  // of when the popover overlay is clicked.
  function activateBudgetPricePopoverClose() {
    $('.budget-price.popover .close, .popover-overlay').click(function() {
      hideBudgetPricePopover();
      return false;
    });
  }

  // Add a close button to the budget price popover
  function addBudgetPricePopoverCloseButton() {
    $('.budget-price.popover .title').append('<a href="#" class="close"><i class="fa fa-remove"></i></a>');
  }

  // Hide the budget price settings modal and load applications using the new settings
  function hideBudgetPricePopover() {
    App.loadResultsApplications();
    $('.popover-overlay, .budget-price.popover').fadeOut(250);
  }

  // Set the max height of the budget price settings modal
  function setBudgetPricePopoverHeight() {
    var popover = $('.budget-price.popover');
    var settingsForm = $('.budget-price.popover .content');
    var originalBottom = settingsForm.css('bottom');

    // Get the actual height of the form
    popover.show();
    settingsForm.css('bottom', 'auto');
    var settingsFormHeight = settingsForm.outerHeight();
    popover.hide();

    // Set the max height on the form and popover so it doesn't look bad on large screens
    popover.css('max-height', settingsFormHeight);
    settingsForm.css('max-height', settingsFormHeight);
    settingsForm.css('bottom', originalBottom);
  }

  // Display the budget price settings modal
  function showBudgetPricePopover() {
    $('.popover-overlay, .budget-price.popover').fadeIn(250);
  }

  App.register('results').enter(function() {
    var settingsLinks = $('.budget-price.settings');

    // Remove default link click handler
    settingsLinks.unbind('click');

    // Add click handler for displaying modal budget price view
    settingsLinks.click(function() {
      showBudgetPricePopover();
      return false;
    });

    // Activate the budget price settings form
    App.activateSettingsForms();
    addBudgetPricePopoverCloseButton();
    setBudgetPricePopoverHeight();
    activateBudgetPricePopoverClose();

    // Update the budget price in the Vue.js components when the budget price
    // is saved using the modal settings form.
  });
})();
