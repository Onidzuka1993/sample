/* globals numeral, Vue */

(function() {
  "use strict";

  App.CropColumn = Vue.extend({
    computed: {
      useBudgetPrice: function() {
        return this.$root.useBudgetPrice;
      },

      usePerAcreValues: function() {
        return this.$root.usePerAcreValues;
      },
      inputs: function() {
        var self = this;
        var calcInputs = {};
        $.each(self.inputIndex, function(key, value) {
          var totalInput = self.crop.inputs[key];
          if (totalInput !== undefined) {
            calcInputs[key] = totalInput;
          }
        });
        return calcInputs;
      }
    },

    methods:  {
      numeral: numeral
     },
    props:    ['crop', 'inputIndex'],
    template: '#crop-column'
  });
})();
