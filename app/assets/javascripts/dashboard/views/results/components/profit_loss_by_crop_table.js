/* globals numeral, Vue */

(function() {
  "use strict";

  App.profitLossByCropTable = Vue.extend({
    components: {
      'crop-row':        App.CropRow,
      'sortable-header': App.SortableHeader
    },

    computed: {
      crops: function() {
        var crops = [];
        var cropNames = [];
        var crop, cropName, cropIndex, result;

        // Loop through the results to find the unique crops
        for (var i = 0; i < this.results.length; i++) {
          result = this.results[i];
          cropName = result.cropName;
          cropIndex = cropNames.indexOf(cropName);
          // Only add each crop once
          if (cropIndex < 0) {
            crop = new App.Crop({
              data: {
                allResults: this.results,
                name:       cropName
              }
            });
            crops.push(crop);
            cropNames.push(cropName);
          }
        }

        return crops;
      },

      income: function() {
        return this.results.map(function(result) {
          return result.income;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      incomeChangeFromYesterday: function() {
        return this.income - this.incomeFromYesterday;
      },

      incomeFromYesterday: function() {
        return this.resultsFromYesterday.map(function(result) {
          return result.income;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      incomeFromYesterdayClass: function() {
        if (this.incomeChangeFromYesterday > 0) {
          return 'positive amount';
        } else if (this.incomeChangeFromYesterday < 0) {
          return 'negative amount';
        } else {
          return 'none amount';
        }
      },

      resultsFromYesterday: function() {
        return this.results.map(function(result) {
          return result.resultFromYesterday;
        }).filter(function(result) {
          return result.application !== null;
        });
      }
    },

    data: function() {
      return {
        sortBy:        'name',
        sortDirection: 1
      };
    },

    methods: {
      numeral: numeral
    },

    props:    ['results'],
    template: '#profit-loss-by-crop-table'
  });
})();
