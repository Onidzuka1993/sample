/* globals numeral, Vue */

(function() {
  "use strict";

  App.ResultRow = Vue.extend({
    components: {
      'field-profit-loss-details': App.FieldProfitLossDetails
    },

    data: function() {
      return { showDetails: false };
    },

    methods: {
      numeral: numeral,

      toggleDetails: function() {
        this.showDetails = !this.showDetails;
      }
    },

    props:    ['result', 'inputIndex'],
    template: '#result-row'
  });
})();
