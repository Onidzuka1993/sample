/* globals numeral */

(function() {
  "use strict";

  var form, nextStepButton, tbody;

  // Set up the onboarding crops form to use AJAX for saving each crop.
  App.activateOnboardingCropsForm = function() {
    form = $('.crops.popover form');
    nextStepButton = $('.next-step.button');
    tbody = $('.crops.popover table tbody');

    // Disable the form fields and button on submit
    form.on('ajax:send', function() {
      form.find('input, .button').prop('disabled', true);
    });

    // Add the new crop to the list after successfully creating
    form.on('ajax:success', function(event, crop) {
      resetForm();
      addCrop(crop);
      sortCrops();
      clearForm();
    });

    // The only error we're handling is if the crop has already been added,
    // so we don't really need to do anything other than reset the form
    form.on('ajax:error', function(event, xhr, status, error) {
      resetForm();
      clearForm();
    });

    // We don't really need to sort the crops on page load, but it will also
    // perform the check to determine if the next step can be activated
    sortCrops();

    // Focus the crop name input on page load
    form.find('#crop_name').focus();
  };

  // Activate the next step button for moving on to crop price settings
  function activateNextStep() {
    tbody.find('.no-crops').remove();
    nextStepButton.prop('disabled', false);
  }

  // Add the new crop as a table row
  function addCrop(crop) {
    var cropHtml = '<tr>';
    cropHtml += '<td>' + crop.name + '</td>';
    cropHtml += '</tr>';
    tbody.append(cropHtml);
  }

  // Clear all form values and focus the text field for the crop's name
  function clearForm() {
    form.find('input').val('');
    form.find('#crop_name').focus();
  }

  // Reactivate the submit button
  function resetForm() {
    form.find('input, .button').prop('disabled', false);
  }

  // Sort the crops by alphabetical order
  function sortCrops() {
    var rows = tbody.find('tr:not(.no-crops)').get();

    rows.sort(function(a, b) {
      var nameA = $(a).find('td:first-of-type').text().toUpperCase();
      var nameB = $(b).find('td:first-of-type').text().toUpperCase();

      if (nameA < nameB) {
        return -1;
      } else if (nameB > nameA) {
        return 1;
      } else {
        return 0;
      }
    });

    tbody.append(rows);

    if (rows.length > 0) {
      activateNextStep();
    }
  }
})();
