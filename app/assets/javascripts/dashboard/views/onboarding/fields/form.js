/* globals numeral */

(function() {
  "use strict";

  var form, nextStepButton, tbody;

  // Set up the onboarding fields form to use AJAX for saving each field.
  App.activateOnboardingFieldsForm = function() {
    form = $('.fields.popover form');
    nextStepButton = $('.next-step.button');
    tbody = $('.fields.popover table tbody');

    // Disable the form fields and button on submit
    form.on('ajax:send', function() {
      form.find('input, .button').prop('disabled', true);
    });

    // Add the new field to the list after successfully creating
    form.on('ajax:success', function(event, field) {
      resetForm();
      addField(field);
      sortFields();
      clearForm();
    });

    // Add the new errors to each attribute field after errors are returned
    form.on('ajax:error', function(event, xhr, status, error) {
      setTimeout(function() {
        resetForm();
        $.each(xhr.responseJSON, function(attribute, attributeErrors) {
          var inputContainer = form.find('.input.' + attribute);
          inputContainer.addClass('field-with-errors');
          inputContainer.append('<span class="error">' + attributeErrors.join(', ') + '</span>');
        });
      }, 500);
    });

    // We don't really need to sort the fields on page load, but it will also
    // perform the check to determine if the next step can be activated
    sortFields();

    // Focus the field name input on page load
    form.find('#field_name').focus();
  };

  // Activate the next step button for moving on to adding crops
  function activateNextStep() {
    tbody.find('.no-fields').remove();
    nextStepButton.prop('disabled', false);
  }

  // Add the new field as a table row
  function addField(field) {
    var fieldHtml = '<tr>';
    fieldHtml += '<td>' + field.name + '</td>';
    fieldHtml += '<td>' + field.owner + '</td>';
    fieldHtml += '<td class="number">' + numeral(field.acres).format('0,0.0') + '</td>';
    fieldHtml += '</tr>';
    tbody.append(fieldHtml);
  }

  // Clear all form values and focus the text field for the field's name
  function clearForm() {
    form.find('input').val('');
    form.find('#field_name').focus();
  }

  // Clear any errors and reset the submit button
  function resetForm() {
    form.find('.input').removeClass('field-with-errors');
    form.find('.error').remove();
    form.find('input, .button').prop('disabled', false);
  }

  // Sort the fields by alphabetical order
  function sortFields() {
    var rows = tbody.find('tr:not(.no-fields)').get();

    rows.sort(function(a, b) {
      var nameA = $(a).find('td:first-of-type').text().toUpperCase();
      var nameB = $(b).find('td:first-of-type').text().toUpperCase();

      if (nameA < nameB) {
        return -1;
      } else if (nameB > nameA) {
        return 1;
      } else {
        return 0;
      }
    });

    tbody.append(rows);

    if (rows.length > 0) {
      activateNextStep();
    }
  }
})();
