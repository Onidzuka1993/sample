$(document).on('turbolinks:load', function() {
    var $incomeStatementFilter = $('#income_statement_filter');

    var $customDate     = $('.custom-date');
    var $applyDatesLink = $('.apply-dates');

    var $startDateField = $('#start_date');
    var $endDateField   = $('#end_date');

    function setQueryStringValues() {
        var $links       = $('.panel a');
        var $selectValue = $(this).val();

        $.each($links, function (index, value) {
            var $link = value;
            var $path = $link.href.split('?')[0];

            $($link).attr('href', $path + '?filter_by=' + $selectValue);
        })
    }

    function setPickDates() {
        $startDateField.pickadate();
        $endDateField.pickadate();
    }

    function showCustomDateForm() {
        $incomeStatementFilter.change(function () {
            if ($(this).val() == 'Custom date') {
                $(this).prop('disabled', 'disabled');

                $('.custom-date').show();
            }
        });
    }

    function hideCustomDate() {
        var $hideCustomDate = $('.hide-custom-date');

        $hideCustomDate.click(function (e) {
            e.preventDefault();
            $customDate.hide();
            $incomeStatementFilter.prop('disabled', false);
        });
    }

    function applyCustomDate() {
        $applyDatesLink.click(function (e) {
            e.preventDefault();
            var $links = $('.panel a');

            $.each($links, function (index, value) {
                var $link = value;
                var $path = $link.href.split('?')[0];

                $($link).attr('href', $path + '?filter_by=custom_date&start_date=' + $startDateField.val() + '&end_date=' + $endDateField.val());
                $customDate.hide();
            });

            $incomeStatementFilter.prop('disabled', false);
        });

        $incomeStatementFilter.change(function () {
            setQueryStringValues.call(this);
        });
    }

    setPickDates();
    showCustomDateForm();
    hideCustomDate();
    applyCustomDate();
});