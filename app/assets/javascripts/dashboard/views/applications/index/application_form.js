(function() {
  "use strict";

  // Reset form after successfully updating the application
  function formAjaxSuccess(event, data) {
    var form = $('#edit_application_' + data.applicationId);

    // Replace the expanded form with the new partial
    var panelContent = $('.content[data-application-id="' + data.applicationId + '"]');
    panelContent.html(data.formHtml);

    // Trigger the events needed after replacing the form
    $(document).trigger('applications:expand-form');
    form.trigger('input:change');
  }

  // Update the total costs after an application form has been expanded
  function updateTotalCosts() {
    var amountElements = $('.total-cost-amount .amount');

    amountElements.each(function() {
      var amountElement = $(this);
      var panelContentElement = amountElement.parents('.panel')[0];
      var applicationElement = $(panelContentElement).parents('.application.panel')[0];
      var totalAcres = App.parseNumber($(applicationElement).data('total-acres'));
      var totalCostCalc = 0;
      var totalCost = 0;
      var costPerAcre, appliedAcres;

      // Calculate the total cost by adding all `data-cost-per-acre` attributes in the panel
      var costElements = $(panelContentElement).find('[data-cost-per-acre]');
      costElements.each(function() {
        costPerAcre = App.parseNumber($(this).data('cost-per-acre'));
        appliedAcres = App.parseNumber($(this).data('applied-acres')) || totalAcres;

        totalCostCalc += (costPerAcre * appliedAcres);
      });


      totalCost = totalCostCalc / totalAcres;

      amountElement.text(App.formatCurrency(totalCost, 3));
    });
  }

  // Handle the input changes on the form and enable/disable the submit button
  // Requires using the "change tracking field component" to receive change events.
  App.register('applications.index').enter(function() {
    var forms = $('form');
    forms.off('ajax:success', formAjaxSuccess);
    forms.on('ajax:success', formAjaxSuccess);

    // The total costs need to be calculated with JS because the costs calculated
    // in the database might not be up-to-date until all background jobs finish.
    $(document).off('applications:expand-form', updateTotalCosts);
    $(document).on('applications:expand-form', updateTotalCosts);
  });
})();
