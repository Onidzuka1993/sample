(function() {
  "use strict";

  function loadingIndicatorOnCopy() {
    $(".copy-line-items.popover .submit .button").click(function(e){
      App.beginTransaction('copy-applications-data');
      e.preventDefault();
      this.textContent = "Loading...";
      this.disabled = true;
      var form = this.form;
      setTimeout(function () {
        form.submit();
      }, 10);
    });
  }

  // Hide the copy line items modal when the close button is clicked or when the popover overlay is clicked.
  function activateCopyLineItemsPopoverClose() {
    $('.copy-line-items.popover .close, .popover-overlay').click(function() {
      hideCopyLineItemsPopover();
      return false;
    });
  }

  // Select or deselect all applications in copy line items popover
  function activateSelectAllApplications() {
    $('.copy-line-items.popover .select-all-applications').change(function() {
      if ($(this).is(':checked')) {
        $('.copy-line-items.popover input[type="checkbox"]:visible').prop('checked', true);
      } else {
        $('.copy-line-items.popover input[type="checkbox"]').prop('checked', false);
      }
    });
  }

  // Add a close button to the copy line items popover
  function addCopyLineItemsPopoverCloseButton() {
    $('.copy-line-items.popover .title').append('<a href="#" class="close"><i class="fa fa-remove"></i></a>');
  }

  // Display copy popover on click of copy link
  function enableCopyLineItemsAction() {
    $('.copy.action').click(function() {
      var link = $(this);
      var applicationId = link.data('application-id');
      showCopyLineItemsPopover(applicationId, link.data('line-item-type'));
      showFilteredApplications(applicationId, link.data('crop-id'));
      //$('.copy-line-items.popover .select-all-applications').click();
      return false;
    });
  }

  // Hide the copy line items modal
  function hideCopyLineItemsPopover() {
    $('.popover-overlay, .copy-line-items.popover').fadeOut(250);
  }

  // Display the copy line items modal
  function showCopyLineItemsPopover(applicationId, lineItemType) {
    $('#copy_copy_from_id').val(applicationId);
    $('#copy_line_item_type').val(lineItemType);
    $('.copy-line-items.popover input[type="checkbox"]').prop('checked', false);
    $('.copy-line-items.popover .line-item-type').text(lineItemType.toLowerCase());
    $('.popover-overlay, .copy-line-items.popover').fadeIn(250);
  }

  // Only show applications with the same crop in the copy popover
  function showFilteredApplications(applicationId, cropId) {
    var applications = $('.copy-to-application[data-crop-id="' + cropId + '"]:not([data-application-id="' + applicationId + '"])');
    $('.copy-to-application').hide();

    if (applications.length > 0) {
      applications.show();
      $('.copy-line-items.popover .no-applications').hide();
      $('.copy-line-items.popover .submit').show();
    } else {
      $('.copy-line-items.popover .no-applications').show();
      $('.copy-line-items.popover .submit').hide();
    }
  }

  // Enable popover for copying line items to other applications
  App.register('applications.index').enter(function() {
    App.endTransaction('copy-applications-data');
    $(document).on('applications:expand-form', enableCopyLineItemsAction);
    addCopyLineItemsPopoverCloseButton();
    activateCopyLineItemsPopoverClose();
    activateSelectAllApplications();
    loadingIndicatorOnCopy();
  });
})();
