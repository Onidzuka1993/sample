(function() {
  "use strict";

  // Activate the change tracking fields and forms after JS events
  // on the Applications page that cause changes to the DOM
  App.register('applications.index').enter(function() {
    // Activate the behavior after a new form has been expanded
    $(document).on('applications:expand-form', App.activateChangeTrackingFields);
    $(document).on('applications:expand-form', App.activateChangeTrackingForms);

    // Activate the change tracking fields after inserting a new Other line item
    $(document).on('cocoon:after-insert', function() {
      setTimeout(App.activateChangeTrackingFields, 0);
    });
  }).exit(function() {
    $(document).off('applications:expand-form', App.activateChangeTrackingFields);
    $(document).off('applications:expand-form', App.activateChangeTrackingForms);
  });
})();
