(function() {
  "use strict";

  // Activate expandable application forms
  App.register('applications.index').enter(function() {
    var applicationId, expandElement, newApplicationId, panelContent, request;

    // Attempt to close the currently expanded form.
    // Returns false if there are unsaved changes.
    // Returns true if there are no changes or no expanded form.
    function closeExpandedForm() {
      panelContent = $('.content[data-application-id="' + applicationId + '"]');
      panelContent.hide('slow');
      return true;
    }

    // Loads the application form into the panel content and animates it to show it.
    function expandForm() {
      // Use applicationId to load and expand the form
      request = $.ajax('/ajax/applications/' + applicationId + '/edit.json');

      request.done(function(data, textStatus, xhr) {
        panelContent = $('.content[data-application-id="' + applicationId + '"]');
        panelContent.html(data.formHtml);
        panelContent.show('slow');
        $(document).trigger('applications:expand-form');
      });

      request.fail(function(xhr, textStatus, error) {
        console.error('applications.index.request.fail', xhr, textStatus, error);
      });

      request.always(function() {
        expandElement.html('<i class="fa fa-angle-double-up"></i> Close');
        expandElement.removeClass('loading');
        expandElement = undefined;
      });
    }

    // Activate expanding application forms
    $('.expand').click(function() {
      if (expandElement === undefined) {
        expandElement = $(this);
      } else {
        return;
      }
      newApplicationId = expandElement.data('application-id');

      $('.expand').html('<i class="fa fa-angle-double-down"></i> More');

      if (newApplicationId === applicationId) {
        closeExpandedForm();
        applicationId = null;
        expandElement.html('<i class="fa fa-angle-double-down"></i> More');
        expandElement.removeClass('loading');
        expandElement = undefined;
        return;
      } else {
        expandElement.html('<i class="fa fa-spinner fa-spin"></i> <i class="fa fa-angle-double-down"></i> More');
        expandElement.addClass('loading');
      }

      // Close the expanded form and open the newly selected form
      if (closeExpandedForm()) {
        applicationId = newApplicationId;
        expandForm();
      }
    });
  });
})();
