/* globals numeral, Vue */

(function() {
  "use strict";

  App.MarketingPlanTable = Vue.extend({
    methods: {
      numeral: numeral
    },

    props:    ['marketing-plan'],
    template: '#marketing-plan-table'
  });
})();
