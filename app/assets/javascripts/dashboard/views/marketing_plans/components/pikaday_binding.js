/* globals moment, Pikaday, Vue */

(function() {
  "use strict";

  App.PikadayBinding = Vue.extend({
    methods: {
      showCalendar: function() {
        this.picker.show();
      }
    },

    props: ['year'],

    ready: function() {
      var field = $(this.$el).siblings('input')[0];
      var options = {
        field: field
      };

      if (this.year !== undefined && this.year !== null) {
        options.minDate   = new Date(this.year + '-01-01');
        options.yearRange = [this.year];
      }

      this.picker = new Pikaday(options);
    },

    template: '<span class="pikaday-binding" v-on:click="showCalendar"></span>'
  });
})();
