/* globals Vue */

(function() {
  "use strict";

  App.DecisionDateTriggersTable = Vue.extend({
    methods: {
      numeral: numeral
    },
    props:    ['marketingPlan'],
    template: '#decision-date-triggers-table'
  });
})();
