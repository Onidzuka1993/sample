/* globals numeral, Vue */

(function() {
  "use strict";

  App.ProfitTargetTriggersTable = Vue.extend({
    methods: {
      numeral: numeral
    },

    props:    ['marketingPlan'],
    template: '#profit-target-triggers-table'
  });
})();
