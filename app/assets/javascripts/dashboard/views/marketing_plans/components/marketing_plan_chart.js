//= require c3
/* globals c3, moment, numeral, Vue */

var globalPrices;

(function() {
  "use strict";

  var axisOptions = {
    x: {
      type: 'timeseries'
    },

    y: {
      tick: {
        format: function(value) {
          return numeral(value).format('$0,0.00');
        }
      }
    }
  };

  var legendOptions = {
    show: false
  };

  var lineColor = '#e53935';

  var tooltipOptions = {
    format: {
      value: function(value) {
        return numeral(value).format('$0,0.00');
      }
    }
  };

  function marketingPlanC3Chart(chartElement) {
    return c3.generate({
      axis:    axisOptions,
      bindto:  chartElement,
      legend:  legendOptions,
      tooltip: tooltipOptions,

      data: {
        colors: {
          'High':  '#eee',
          'Low':   '#eee',
          'Close': lineColor
        },

        columns: [
          ['Date'],
          ['Close'],
          ['High'],
          ['Low']
        ],

        x: 'Date'
      }
    });
  }

  // Super hacky way to make sure regions are always updated
  function updateRegions(chart, regions) {
    chart.regions.remove();

    for (var i = 0; i < 10; i++) {
      setTimeout(function() {
        chart.regions(regions);
      }, i * 200);
    }
  }

  App.MarketingPlanChart = Vue.extend({
    computed: {
      dates: function() {
        return this.futuresData.map(function(item) {
          return item.tradingDay;
        });
      },

      futuresMonths: function() {
        if (/Soybeans.*/i.test(this.cropName)) {
          return ['January', 'March', 'May', 'July', 'August', 'September', 'November'];
        } else { // Corn, Hard Red Wheat, Spring Wheat
          return ['March', 'May', 'July', 'September', 'December'];
        }
      },

      futuresPricesClose: function() {
        return this.futuresData.map(function(item) {
          return item.close / 100;
        });
      },

      futuresPricesLow: function() {
        return this.futuresData.map(function(item) {
          return item.low / 100;
        });
      },

      futuresPricesHigh: function() {
        return this.futuresData.map(function(item) {
          return item.high / 100;
        });
      },

      futuresPricesMax: function() {
        var max = 0;
        var high;

        for (var i = 0; i < this.futuresData.length; i++) {
          high = this.futuresData[i].high;
          if (high > max) {
            max = high;
          }
        }

        return max / 100;
      },

      futuresPricesMaxLine: function() {
        return {
          position: 'end',
          text:     'Highest Price - ' + App.formatCurrency(this.futuresPricesMax),
          value:    this.futuresPricesMax
        };
      },

      futuresPricesMin: function() {
        var min = 999;
        var low;

        for (var i = 0; i < this.futuresData.length; i++) {
          low = this.futuresData[i].low;
          if (low < min) {
            min = low;
          }
        }

        return min / 100;
      },

      futuresYears: function() {
        return [this.marketingPlan.year, this.marketingPlan.year + 1];
      },

      maxX: function() {
        return this.marketingPlan.endDate;
      },

      maxY: function() {
        var maxY = this.futuresPricesMax;

        if (this.targetPricesMax > maxY) {
          maxY = this.targetPricesMax;
        }

        return maxY;
      },

      minX: function() {
        return this.marketingPlan.startDate;
      },

      minY: function() {
        var minY = this.futuresPricesMin;

        if (this.targetPricesMin < minY) {
          minY = this.targetPricesMin;
        }

        return minY;
      },

      targetLines: function() {
        return this.marketingPlan.triggers.map(function(trigger) {
          return {
            position: 'start',
            text:     'Target #' + trigger.targetNumber + ' - ' + trigger.futuresPrice,
            value:    App.parseNumber(trigger.futuresPrice)
          };
        });
      },

      targetPricesMax: function() {
        var max = 0;
        var high;

        for (var i = 0; i < this.marketingPlan.triggers.length; i++) {
          high = App.parseNumber(this.marketingPlan.triggers[i].futuresPrice);
          if (high > max) {
            max = high;
          }
        }

        return max;
      },

      targetPricesMin: function() {
        var min = 999;
        var low;

        for (var i = 0; i < this.marketingPlan.triggers.length; i++) {
          low = App.parseNumber(this.marketingPlan.triggers[i].futuresPrice);
          if (low < min) {
            min = low;
          }
        }

        return min;
      },

      targetRegions: function() {
        return this.marketingPlan.triggers.map(function(trigger) {
          return {
            start: trigger.startDate,
            end:   moment(trigger.startDate).add(5, 'days').format('YYYY-MM-DD')
          };
        });
      }
    },

    data: function() {
      return {
        futuresData:    [],
        futuresDataUrl: null
      };
    },

    methods: {
      updateChart: function() {
        if (this.chart !== undefined) {
          // Update the futures prices + dates
          this.chart.load({
            columns: [
              ['Date'].concat(this.dates),
              ['Close'].concat(this.futuresPricesClose),
              ['High'].concat(this.futuresPricesHigh),
              ['Low'].concat(this.futuresPricesLow)
            ]
          });

          // Update the target lines
          this.chart.ygrids(this.targetLines.concat([this.futuresPricesMaxLine]));

          // Update the X and Y axis ranges
          this.chart.axis.max({ x: this.maxX, y: this.maxY });
          this.chart.axis.min({ x: this.minX, y: this.minY });

          // Update the target regions
          updateRegions(this.chart, this.targetRegions);
        }
      },

      updateFuturesData: function() {
        var queryStrings = ['crop_name=' + this.cropName];
        queryStrings.push('end_date=' + this.marketingPlan.endDate);
        queryStrings.push('start_date=' + this.marketingPlan.startDate);
        queryStrings.push('futures_month=' + this.marketingPlan.futuresMonth);
        queryStrings.push('futures_year=' + this.marketingPlan.futuresYear);

        var url = '/ajax/histories.json?' + queryStrings.join('&');

        if (url !== this.futuresDataUrl) {
          this.futuresDataUrl = url;

          var request = $.ajax(this.futuresDataUrl);
          var self = this;

          request.done(function(data, textStatus, xhr) {
            self.futuresData = data;
          });

          request.fail(function(xhr, textStatus, error) {
            console.error('App.MarketingPlanChart.updateFuturesData.request.fail', xhr, textStatus, error);
          });
        }

      }
    },

    props: ['cropName', 'marketingPlan'],

    ready: function() {
      if (this.chart === undefined) {
        this.chart = marketingPlanC3Chart(this.$el.firstElementChild);
        this.updateChart();
      }
    },

    template: '#marketing-plan-chart'
  });

  Vue.directive('update-marketing-plan-chart', function() {
    if (this.vm.updateChart === undefined) { return; }
    this.vm.updateChart();
  });

  Vue.directive('update-marketing-plan-chart-futures-data', function() {
    if (this.vm.updateFuturesData === undefined) { return; }
    this.vm.updateFuturesData();
  });
})();
