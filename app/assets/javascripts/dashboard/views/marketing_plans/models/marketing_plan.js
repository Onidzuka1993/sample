/* globals Vue */
/* jshint camelcase: false */

(function() {
  "use strict";

  App.MarketingPlan = Vue.extend({
    computed: {
      acres: function() {
        if (this.json.planted_acres === 0) {
          return 1;
        } else {
          return this.json.planted_acres;
        }
      },

      basisEstimate: {
        get: function() {
          var computedBasisEstimate = this.json.basis_estimate;

          if (this.userBasisEstimate !== null) {
            computedBasisEstimate = this.userBasisEstimate;
          }

          return App.formatCurrency(computedBasisEstimate);
        },

        set: function(value) {
          value = App.parseNumber(value);

          if (value === this.json.basis_estimate) {
            this.userBasisEstimate = null;
          } else {
            this.userBasisEstimate = value;
          }
        }
      },

      breakEven: function() {
        var production = App.parseNumber(this.yield) * this.acres;
        var totalCost = App.parseNumber(this.costPerAcre) * this.acres;
        return App.formatCurrency(totalCost / production);
      },

      costPerAcre: {
        get: function() {
          var computedCostPerAcre = 0;

          if (this.userCostPerAcre !== null) {
            computedCostPerAcre = this.userCostPerAcre;
          } else if (this.savedCostPerAcre !== null) {
            computedCostPerAcre = this.savedCostPerAcre;
          } else {
            if (this.acres === 0) {
              return 0;
            }
            computedCostPerAcre = this.totalCost / this.acres;
          }

          return App.formatCurrency(computedCostPerAcre);
        },

        set: function(value) {
          value = App.parseNumber(value);

          var costPerAcre = 0;
          if (this.acres > 0) {
            costPerAcre = this.totalCost / this.acres;
          }

          if (value === costPerAcre) {
            this.userCostPerAcre = null;
          } else {
            this.userCostPerAcre = value;
          }
        }
      },

      endDate: {
        get: function() {
          var computedEndDate = this.json.end_date;

          if (this.userEndDate !== null) {
            computedEndDate = this.userEndDate;
          }

          return computedEndDate;
        },

        set: function(value) {
          if (value === this.json.end_date) {
            this.userEndDate = null;
          } else {
            this.userEndDate = value;
          }
        }
      },

      futuresMonth: {
        get: function() {
          var computedFuturesMonth = this.json.futures_month;

          if (this.userFuturesMonth !== null) {
            computedFuturesMonth = this.userFuturesMonth;
          }

          return computedFuturesMonth;
        },

        set: function(value) {
          if (value === this.json.futures_month) {
            this.userFuturesMonth = null;
          } else {
            this.userFuturesMonth = value;
          }
        }
      },

      futuresYear: {
        get: function() {
          var computedFuturesYear = this.json.futures_year;

          if (this.userFuturesYear !== null) {
            computedFuturesYear = this.userFuturesYear;
          }

          return computedFuturesYear;
        },

        set: function(value) {
          if (value === this.json.futures_year) {
            this.userFuturesYear = null;
          } else {
            this.userFuturesYear = value;
          }
        }
      },

      savedCostPerAcre: function() {
        return this.json.saved_cost_per_acre;
      },

      production: function() {
        return this.yield * this.acres;
      },

      projectedAveragePrice: function() {
        var bushels = this.triggers.map(function(trigger) {
          return App.parseNumber(trigger.bushels);
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);

        if (bushels <= 0) {
          return 0;
        }

        return this.projectedRevenue / bushels;
      },

      projectedTotalProduction: function() {
        return this.triggers.map(function(trigger) {
          return trigger.percent_of_production;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      projectedIncome: function() {
        return (this.production * this.projectedAveragePrice) - this.projectedTotalCost;
      },

      projectedIncomePerAcre: function() {
        return this.projectedIncome / this.acres;
      },

      projectedRevenue: function() {
        var basisEstimate = App.parseNumber(this.basisEstimate);

        return this.triggers.map(function(trigger) {
          var bushels = App.parseNumber(trigger.bushels);
          var futuresPrice = App.parseNumber(trigger.futuresPrice);
          return (futuresPrice + basisEstimate) * bushels;
        }).reduce(function(previous, current) {
          return previous + current;
        }, 0);
      },

      projectedTotalCost: function() {
        var costPerAcre = App.parseNumber(this.costPerAcre);
        return costPerAcre * this.acres;
      },

      startDate: {
        get: function() {
          var computedStartDate = this.json.start_date;

          if (this.userStartDate !== null) {
            computedStartDate = this.userStartDate;
          }

          return computedStartDate;
        },

        set: function(value) {
          if (value === this.json.start_date) {
            this.userStartDate = null;
          } else {
            this.userStartDate = value;
          }
        }
      },

      totalCost: function() {
        return this.json.total_cost;
      },

      triggers: function() {
        var self = this;
        return this.json.triggers.map(function(json) {
          return new App.MarketingPlansTrigger({
            data: function() {
              return {
                json:          json,
                marketingPlan: self,
                production: self.production
              };
            }
          });
        });
      },

      year: function() {
        return this.json.year.year;
      },

      yield: {
        get: function() {
          var computedYield = this.json.yield;

          if (this.userYield !== null) {
            computedYield = this.userYield;
          }

          return App.formatNumber(computedYield, 2);
        },

        set: function(value) {
          value = App.parseNumber(value);

          if (value === this.json.yield) {
            this.userYield = null;
          } else {
            this.userYield = value;
          }
        }
      }
    },

    data: function() {
      return {
        userBasisEstimate: null,
        userCostPerAcre:   null,
        userEndDate:       null,
        userFuturesMonth:  null,
        userFuturesYear:   null,
        userStartDate:     null,
        userYield:         null
      };
    }
  });
})();
