/* globals Vue */
/* jshint camelcase: false */

(function() {
  "use strict";

  App.MarketingPlansTrigger = Vue.extend({
    computed: {
      bushels: {
        get: function() {
          var computedBushels = 0;

          if (this.userBushels !== null) {
            computedBushels = this.userBushels;
          } else {
            computedBushels = this.json.bushels;
          }

          return App.formatNumber(computedBushels);
        },

        set: function(value) {
          value = App.parseNumber(value);

          if (value === this.json.bushels) {
            this.userBushels = null;
          } else {
            this.userBushels = value;
          }
        }
      },

      percent_of_production: function() {
        if (this.production > 0) {
          return App.parseNumber(this.bushels) / this.production;
        }else{
          return 0;
        }
      },

      dateHit: function() {
        return this.json.date_hit;
      },

      futuresPrice: {
        get: function() {
          var computedFuturesPrice = 0;

          if (this.userFuturesPrice !== null) {
            computedFuturesPrice = this.userFuturesPrice;
          } else {
            computedFuturesPrice = this.json.futures_price;
          }

          return App.formatCurrency(computedFuturesPrice, 2);
        },

        set: function(value) {
          value = App.parseNumber(value);

          if (value === this.json.futures_price) {
            this.userFuturesPrice = null;
          } else {
            this.userFuturesPrice = value;
          }
        }
      },

      id: function() {
        return this.json.id;
      },

      margin: function() {
        var basisEstimate = App.parseNumber(this.marketingPlan.basisEstimate);
        var breakEven = App.parseNumber(this.marketingPlan.breakEven);
        var futuresPrice = App.parseNumber(this.futuresPrice);

        var margin = (futuresPrice + basisEstimate) - breakEven;
        return App.formatCurrency(margin);
      },

      profitHit: function() {
        return this.json.profit_hit;
      },

      startDate: {
        get: function() {
          var computedStartDate = 0;

          if (this.userStartDate !== null) {
            computedStartDate = this.userStartDate;
          } else {
            computedStartDate = this.json.start_date;
          }

          return computedStartDate;
        },

        set: function(value) {
          if (value === this.json.start_date) {
            this.userStartDate = null;
          } else {
            this.userStartDate = value;
          }
        }
      },

      targetNumber: function() {
        return this.json.target_number;
      }
    },

    data: function() {
      return {
        userBushels:      null,
        userFuturesPrice: null,
        userStartDate:    null
      };
    }
  });
})();
