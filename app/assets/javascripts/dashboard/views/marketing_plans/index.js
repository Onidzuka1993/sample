//= require ./components/pikaday_binding
/* globals marketingPlan, Vue */

(function() {
  "use strict";

  App.register('marketing-plans.index').enter(function() {
    Vue.component('pikaday-binding', App.PikadayBinding);

    var marketingPlanView = new Vue({
      components: {
        'marketing-plan-chart':         App.MarketingPlanChart,
        'marketing-plan-table':         App.MarketingPlanTable,
        'decision-date-triggers-table': App.DecisionDateTriggersTable,
        'profit-target-triggers-table': App.ProfitTargetTriggersTable
      },

      data: function() {
        return {
          marketingPlan: new App.MarketingPlan({
            data: function() {
              return { json: marketingPlan };
            }
          })
        };
      },

      el: '#marketing-plan'
    });
  });
})();
