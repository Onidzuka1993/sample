/* globals numeral */
(function() {
  "use strict";

  // Set up the settings form to use AJAX for saving each setting.
  App.activateSettingsForms = function() {
    $('form.settings .edit').click(function() {
      var editLink = $(this);
      var textField = editLink.siblings('input[type="text"]');
      var saveButton = editLink.siblings('input[type="submit"]');

      textField.attr('disabled', false);
      textField.focus();

      editLink.hide();
      saveButton.show();

      return false;
    });

    $('form.settings').on('ajax:send', function() {
      var form = $(this);
      form.find('a').removeClass('active');
      form.find('input[type="text"]').attr('disabled', true);
      form.find('input[type="submit"]').hide();
      form.find('.saving').text('Saving...').show();
    }).on('ajax:success', function() {
      var form = $(this);
      form.find('.saving').text('Saved!');
      setTimeout(function() {
        form.find('.saving').hide();
        form.find('.edit').show();
      }, 1000);
    }).on('ajax:error', function(event, xhr, status, error) {
      var form = $(this);
      form.find('.saving').hide();
      form.find('.error').text(error);
      form.find('.error').show();
    });

    $('form.dashboard-markets-settings').on('ajax:send', function() {
      var form = $(this);
      form.find('select').attr('disabled', true);
      form.find('.saving').text('Saving...').show();
    }).on('ajax:success', function() {
      var form = $(this);
      form.find('select').attr('disabled', false);
      form.find('.saving').text('Saved!');
      setTimeout(function() {
        form.find('.saving').hide();
      }, 1000);
    }).on('ajax:error', function(event, xhr, status, error) {
      var form = $(this);
      form.find('select').attr('disabled', false);
      form.find('.saving').hide();
      form.find('.error').text(error);
      form.find('.error').show();
    });

    $('form.dashboard-markets-settings select').on('change', function() {
      var form = $(this.form);
      form.submit();
    });
  };

  // Set up the settings form to use AJAX for saving each setting.
  App.register('settings.index').enter(function() {
    App.activateSettingsForms();
  });
})();
