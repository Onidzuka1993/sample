/* globals numeral */
(function() {
  "use strict";

  var cell, cells, change, formattedPrice, futuresPrice, month, oldPrice;
  var refreshInterval, request, result, settingsIcon, span;
  var firstUpdate = true;

  function updatePrice(element, newPrice) {
    element.removeClass('loading');
    span = element.find('span');
    oldPrice = numeral(span.text()).value();
    formattedPrice = numeral(newPrice / 100).format('$0,0.0000');

    // Update price and add class for flashing positive or negative
    span.text(formattedPrice);
    span.removeClass();

    if (!firstUpdate) {
      if (newPrice > oldPrice) {
        span.addClass('positive');
      } else if (newPrice < oldPrice) {
        span.addClass('negative');
      } else {
        span.addClass('none');
      }
    }
  }

  function updateFuturesPrices() {
    settingsIcon.addClass('fa-spin');
    request = $.ajax('/ajax/futures.json');

    request.done(function(data, textStatus, xhr) {
      $.each(data, function(key, value) {
        cells = $('td.' + key);
        cells.each(function(index) {
          cell = $(this);
          result = value[index];
          month = cell.find('.month');
          futuresPrice = cell.find('.futures-price');
          change = cell.find('.change');

          month.text(result.month);
          updatePrice(futuresPrice, result.lastPrice);
          updatePrice(change, result.netChange);

          change.removeClass().addClass('change');
          month.removeClass('loading');

          if (result.netChange > 0) {
            change.addClass('positive');
          } else if (result.netChange < 0) {
            change.addClass('negative');
          } else {
            change.addClass('none');
          }
        });
      });
    });

    request.fail(function(xhr, textStatus, error) {
      console.error('activateFuturesMarkets.request.fail', xhr, textStatus, error);
    });

    request.always(function() {
      firstUpdate = false;
      settingsIcon.removeClass('fa-spin');
    });
  }

  App.register('dashboards.show').enter(function() {
    settingsIcon = $('.futures-markets.panel .title i');
    updateFuturesPrices();

    // Activate the polling of futures prices for updating the table.
    refreshInterval = setInterval(updateFuturesPrices, 5000);

    // Create crops summary view
    App.resultsView = new App.CropsSummaryDashboard({
      el: $('#crops-summary div')[0]
    });
    App.loadResultsApplications({ filter: { 'show_on_dashboard': true } });
  }).exit(function() {
    // Deactivate polling
    clearInterval(refreshInterval);
    App.resultsView = null;
    App.cancelLoadResultsApplications();
  });
})();
