/* globals numeral, Vue */

(function() {
  "use strict";

  // Vue.js component for displaying a summary of crops sold for the year on the dashboard.
  App.CropsSummaryDashboard = Vue.extend({
    computed: {
      crops: function() {
        var crops = [];
        var cropNames = [];
        var crop, cropName, cropIndex, result;

        // Loop through the results to find the unique crops
        for (var i = 0; i < this.results.length; i++) {
          result = this.results[i];
          cropName = result.cropName;
          cropIndex = cropNames.indexOf(cropName);
          // Only add each crop once
          if (cropIndex < 0) {
            crop = new App.Crop({
              data: {
                allResults: this.results,
                name:       cropName
              }
            });
            crops.push(crop);
            cropNames.push(cropName);
          }
        }

        return crops;
      },

      results: function() {
        var applications = this.applications;
        var inputIndex = this.parsedInputIndex;
        return applications.map(function(application) {
          return new App.Result({
            data: {
              application:    application,
              applications:   applications,
              useBudgetPrice: false,
              inputIndex: inputIndex
            }
          });
        });
      },

      year: function() {
        return window.location.pathname.replace(/\D/g, '');
      },
      parsedInputIndex: function() {
        var parsed = JSON.parse(this.inputIndex);
        if (Object.keys(parsed).length < 1) {
          parsed["other"] = "Other";
        }
        return parsed
      }
    },

    data: function() {
      return {
        applications: [],
        loading:      true
      };
    },

    methods: {
      numeral: numeral
    },

    template: '#crops-summary-dashboard',
    props: ['inputIndex']
  });
})();
