(function() {
  "use strict";

  var productComponent = {
    props: ['argProduct', 'units', 'coverageAmounts'],
    data: function() {
      return {
        changed: false,
        product: this.argProduct,
        initial: {
          amount: this.argProduct.amount,
          crop_names: this.argProduct.crop_names,
          price: this.argProduct.price,
          units: this.argProduct.units,
          coverage_amount: this.argProduct.coverage_amount,
          density: this.argProduct.density,
          liquid: this.argProduct.liquid,
          multiplier: this.argProduct.multiplier
        }
      }
    },
    watch: {
      product: {
        handler: function(){
          if (
              (this.product.amount !== this.initial.amount) ||
              (this.product.crop_names !== this.initial.crop_names) ||
              (this.product.price !== this.initial.price) ||
              (this.product.units !== this.initial.units) ||
              (this.product.coverage_amount !== this.initial.coverage_amount) ||
              (this.product.density !== this.initial.density) ||
              (this.product.liquid !== this.initial.liquid) ||
              (this.product.multiplier !== this.initial.multiplier)
             ) {
            if (this.$root.recalc.indexOf(this.product.calc) === -1){
              this.$root.recalc.push(this.product.calc);
            }
          }

          // Object exists in DB and needs to be updated, not inserted
          if ( !this.changed && this.product.id !== undefined && this.product.id !== null) {
            this.$root.productChangeset.update.push(this.product);
            this.changed = true;
          }
        },
        deep: true
      }
    }
  }

  App.register('inputs.show').enter(function() {
    var defaultProductObject = {
      template: '#v-inputs-product-default',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.price !== oldObj.price) ||
              (newObj.units !== oldObj.units)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    defaultProductObject.data = productComponent.data;
    defaultProductObject.props = productComponent.props;
    defaultProductObject.watch = productComponent.watch;
    var DefaultProduct = Vue.extend(defaultProductObject);


    var CropInsuranceProductObject = {
      template: '#v-inputs-product-crop-insurance',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.coverage_amount !== oldObj.coverage_amount) ||
              (newObj.amount !== oldObj.amount) ||
              (newObj.price !== oldObj.price)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    CropInsuranceProductObject.data = productComponent.data;
    CropInsuranceProductObject.props = productComponent.props;
    CropInsuranceProductObject.watch = productComponent.watch;
    var CropInsuranceProduct = Vue.extend(CropInsuranceProductObject);

    var DryingProductObject = {
      template: '#v-inputs-product-drying',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.price !== oldObj.price)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    DryingProductObject.data = productComponent.data;
    DryingProductObject.props = productComponent.props;
    DryingProductObject.watch = productComponent.watch;
    var DryingProduct = Vue.extend(DryingProductObject);


    var FertilizerProductObject = {
      template: '#v-inputs-product-fertilizer',
      methods: {
        checkLiquidInput: function(){
          if (this.liquidType) {
            this.product.liquid = true;
          }else{
            this.product.liquid = false;
          }
        },
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.units !== oldObj.units) ||
              (newObj.liquid !== oldObj.liquid) ||
              (newObj.price !== oldObj.price) ||
              (newObj.density !== oldObj.density)
            ){
            return true;
          }else{
            return false;
          }
        }
      },
      computed: {
        liquidType: function(){
          if (this.product.units === 'per gallon' ||
              this.product.units === 'per fl oz') {
            return true;
          }else{
            return false;
          }
        },
        showDensity: function(){
          if (!this.liquidType && this.product.liquid){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    FertilizerProductObject.data = productComponent.data;
    FertilizerProductObject.props = productComponent.props;
    FertilizerProductObject.watch = productComponent.watch;
    var FertilizerProduct = Vue.extend(FertilizerProductObject);

    var FuelProductObject = {
      template: '#v-inputs-product-fuel',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.amount !== oldObj.amount) ||
              (newObj.price !== oldObj.price)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    FuelProductObject.data = productComponent.data;
    FuelProductObject.props = productComponent.props;
    FuelProductObject.watch = productComponent.watch;
    var FuelProduct = Vue.extend(FuelProductObject);

    var LaborProductObject = {
      template: '#v-inputs-product-labor',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.price !== oldObj.price)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    LaborProductObject.data = productComponent.data;
    LaborProductObject.props = productComponent.props;
    LaborProductObject.watch = productComponent.watch;
    var LaborProduct = Vue.extend(LaborProductObject);

    var PaymentsDepreciationProductObject = {
      template: '#v-inputs-product-payments-depreciation',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.price !== oldObj.price)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    PaymentsDepreciationProductObject.data = productComponent.data;
    PaymentsDepreciationProductObject.props = productComponent.props;
    PaymentsDepreciationProductObject.watch = productComponent.watch;
    var PaymentsDepreciationProduct = Vue.extend(PaymentsDepreciationProductObject);

    var RepairProductObject = {
      template: '#v-inputs-product-repair',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.price !== oldObj.price)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    RepairProductObject.data = productComponent.data;
    RepairProductObject.props = productComponent.props;
    RepairProductObject.watch = productComponent.watch;
    var RepairProduct = Vue.extend(RepairProductObject);

    var SeedProductObject = {
      template: '#v-inputs-product-seed',
      methods: {
        significantChange: function(newObj, oldObj) {
          if (
              (newObj.units !== oldObj.units) ||
              (newObj.price !== oldObj.price) ||
              (newObj.multiplier !== oldObj.multiplier)
            ){
            return true;
          }else{
            return false;
          }
        }
      }
    };
    SeedProductObject.data = productComponent.data;
    SeedProductObject.props = productComponent.props;
    SeedProductObject.watch = productComponent.watch;
    var SeedProduct = Vue.extend(SeedProductObject);

    var MiscRevenue = Vue.extend({
      template: '#v-inputs-misc-revenue',
      props: ['argRevenue'],
      data: function() {
        return {
          changed: false,
          revenue: this.argRevenue,
          initial: {
            amount: this.argRevenue.amount,
            crop_names: this.argRevenue.crop_names
          }
        }
      },
      watch: {
        revenue: {
          handler: function(){

            if (
                (this.revenue.amount !== this.initial.amount) ||
                (this.revenue.crop_names !== this.initial.crop_names)
               ) {
              if (this.$root.recalc.indexOf(this.revenue.type) === -1){
                this.$root.recalc.push(this.revenue.type);
              }
            }
            // Object exists in DB and needs to be updated, not inserted
            if ( !this.changed && this.revenue.id !== undefined) {
              this.$root.revenueChangeset.update.push(this.revenue);
              this.changed = true;
            }
          },
          deep: true
        }
      }
    });

    var page = new Vue({
      el: '#v-inputs',

      components: {
        default: DefaultProduct,
        cropInsurance: CropInsuranceProduct,
        drying: DryingProduct,
        fertilizer: FertilizerProduct,
        fuel: FuelProduct,
        labor: LaborProduct,
        paymentsDepreciation: PaymentsDepreciationProduct,
        repair: RepairProduct,
        seed: SeedProduct,
        miscRevenue: MiscRevenue
      },
      computed: {
        hasChangeReordered: function() {
          if (this.reordered) {
            return true;
          }else{
            return false;
          }
        },
        hasChangeProducts: function() {
          if ((this.productChangeset.create.length > 0) ||
              (this.productChangeset.update.length > 0) ||
              (this.productChangeset.destroy.length > 0)) {
            return true;
          }else{
            return false;
          }
        },
        hasChangeRevenue: function() {
          if ((this.revenueChangeset.create.length > 0) ||
              (this.revenueChangeset.update.length > 0) ||
              (this.revenueChangeset.destroy.length > 0)) {
            return true;
          }else{
            return false;
          }
        },
        hasChange: function() {
          if (this.hasChangeReordered) {
            return true;
          }

          if (this.hasChangeProducts) {
            return true;
          }

          if (this.hasChangeRevenue) {
            return true;
          }

          return false;
        }
      },
      ready: function() {
        this.fetchInputs();

        App.pageActions.exportOptions.push({
          name: 'PDF', icon: 'pdf', function: this.downloadPDF
        });
      },

      data: function() {
        return {
          initial: true,
          groupEvery: 5,
          groupAfter: 10,
          displayContents: false,
          inputs: [],
          revenues: [],
          errorOnPage: false,
          reordered: false,
          recalc: [],
          queryData: {},
          productChangeset: {
            create:  [],
            update:  [],
            destroy: []
          },
          revenueChangeset: {
            create:  [],
            update:  [],
            destroy: []
          }
        };
      },
      methods: {
        validationCallback: function() {
          this.errorOnPage = true;
        },
        downloadPDF: function() {
          App.pageStatus.putLoading();
          App.pdfGenerator.downloadProducts(this.inputs, this.revenues, this.queryData.entities, this.queryData.years);
          App.pageStatus.dismiss();
        },
        splitProducts: function(index, input) {
          return false;
          if (input.products.length >= this.groupAfter && index != 0 && (index % this.groupEvery) == 0){
            return true;
          }else{
            return false
          }
        },
        initChangelist: function() {
          this.reordered = false;
          this.productChangeset = {
            create:  [],
            update:  [],
            destroy: []
          };
          this.revenueChangeset = {
            create:  [],
            update:  [],
            destroy: []
          };
        },
        inputsHaveProducts: function(inputs) {
          for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].products.length < 1) {
              this.insertProduct(inputs[i]);
            }
          }
        },
        revenuesHaveProducts: function(revenues) {
          if (revenues.length < 1) {
            this.insertRevenue();
          }
        },
        fetchInputs: function() {
          var request = App.api.getAll('input', {});
          var self = this;
          App.pageStatus.putLoading();
          App.beginTransaction('load-inputs');

          request.done(function(json, textStatus, xhr) {
            self.inputs = json.payload.inputs;
            self.revenues = json.payload.revenues;
            self.queryData = json.query;
            self.displayContents = true;
            self.recalc = [];
            self.initChangelist();
            self.inputsHaveProducts(self.inputs);
            self.revenuesHaveProducts(self.revenues);

            App.pageStatus.dismiss();
            App.endTransaction('load-inputs');
            setTimeout(function () {
              self.initSortable(".sort-container", ".input-sort-handle", self.inputs, function(){
                self.reordered = true;
              });
              var nodes = $(".sort-products-container");
              for (var i = 0; i < nodes.length; i++) {
                self.initSortable(nodes[i], ".product-sort-handle", self.inputs[i].products);
              }

              self.initSortable(".sort-revenues-container", ".revenues-sort-handle", self.revenues);

            }, 100);
          });

          request.fail(function(xhr, textStatus, error) {
            App.failTransaction('load-inputs');
            App.pageStatus.putLoadingError({retry: self.fetchInputs});
          });
        },
        discardChanges: function() {
          if (confirm("Are you sure?  This will undo all of your changes you have made.")) {
            this.fetchInputs();
          }
        },
        saveInputs: function() {
          this.errorOnPage = false;
          //this.$broadcast('validate-inputs', this.validationCallback);
          var self = this;
          setTimeout(function () {
            if (!self.errorOnPage) {

              App.pageStatus.putSaving();

              if (self.hasChangeProducts || self.hasChangeRevenue) {
                // Only Track intensive saving
                App.beginTransaction('save-inputs');
              }

              var payload = {};
              payload.recalculate = self.recalc;
              if (self.reordered) {
                payload.reordered_inputs = self.inputs.map(function(input){
                  return {
                    id: input.id,
                    position: input.position
                  }
                });
              }

              payload.product_changeset = self.productChangeset;
              payload.revenue_changeset = self.revenueChangeset;
              var request = App.api.update('input', null, {}, payload);
              request.done(function(json, textStatus, xhr) {
                App.endTransaction('save-inputs');
                self.fetchInputs();
              });

              request.fail(function(xhr, textStatus, error) {
                App.failTransaction('save-inputs');
                App.pageStatus.putSavingError({retry: self.saveInputs});
              });
            }
          }, 0);
        },
        initSortable: function(container, handle, list, callback) {
          $(container).sortable({
            handle: handle,
            placeholder: 'placeholder',
            forcePlaceholderSize: true,
            start: function(e, ui) {
              $(this).addClass("sortable-dragging");
              ui.item.data('from', ui.item.index());
            },
            stop: function() {
              $(this).removeClass("sortable-dragging");
            },
            update: function(event, ui) {
              var from = ui.item.data('from'),
                  to = ui.item.index();

              list.splice(to, 0, list.splice(from, 1)[0]);

              for (var i = 0; i < list.length; i++) {
                list[i].position = i + 1;
              }

              if (callback !== undefined){
                callback();
              }
            }
          })
        },
        productComponent: function(productType) {
          switch (productType) {
            case "Inputs::CropInsurance::Input":
              return "cropInsurance";
            case "Inputs::Drying::Input":
              return "drying";
            case "Inputs::Fertilizer::Input":
              return "fertilizer";
            case "Inputs::Fuel::Input":
              return "fuel";
            case "Inputs::Labor::Input":
              return "labor";
            case "Inputs::PaymentsDepreciation::Input":
              return "paymentsDepreciation";
            case "Inputs::Repair::Input":
              return "repair";
            case "Inputs::Seed::Input":
              return "seed";
            default:
              return "default";
          }
        },
        insertProduct: function(input) {
          var coverageAmount = null;
          if (input.coverageAmounts !== undefined) {
            coverageAmount = input.coverageAmounts[0] || null;
          }
          var product = {
            "name": "",
            "cropNames": ["All Crops"],
            "type": "Inputs::" + input.inputType + "::Product",
            "calc": input.inputType,
            "amount": 0,
            "price": 0,
            "input_id": input.id,
            "units": input.productUnits[0],
            "position": input.products.length + 2,
            "coverage_amount": coverageAmount,
            "density": 0,
            "liquid": false,
            "multiplier": 1
          }

          input.products.push(product);

          // Track new product
          this.productChangeset.create.push(product);
        },
        removeProduct: function(input, product) {
          if (confirm("Are you sure you want to remove this entry?")) {
            if (product.id !== undefined) {
              this.productChangeset.destroy.push(product);
              this.productChangeset.update.$remove(product);
              if (this.recalc.indexOf(product.calc) === -1){
                this.recalc.push(product.calc);
              }
            }
            input.products.$remove(product);

            // Untrack product if it is being tracked
            this.productChangeset.create.$remove(product);
          }

          if (input.products.length < 1) {
            this.insertProduct(input);
          }
        },
        insertRevenue: function() {
          var revenue = {
            "name": "",
            "cropNames": ["All Crops"],
            "type": "Revenue",
            "amount": 0
          }

          this.revenues.push(revenue);

          // Track new revenue
          this.revenueChangeset.create.push(revenue);
        },
        removeRevenue: function(revenue) {
          if (confirm("Are you sure you want to remove this entry?")) {
            if (revenue.id !== undefined) {
              this.revenueChangeset.destroy.push(revenue);
              this.revenueChangeset.update.$remove(revenue);
              if (this.recalc.indexOf(revenue.type) === -1){
                this.recalc.push(revenue.type);
              }
            }
            this.revenues.$remove(revenue);

            // Untrack revenue if it is being tracked
            this.revenueChangeset.create.$remove(revenue);
          }

          if (this.revenues.length < 1) {
            this.insertRevenue();
          }
        }
      }
    });
  });
})();
