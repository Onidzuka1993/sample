/* globals numeral */
(function() {
  "use strict";

  var refreshInterval;

  App.register('pages.markets').enter(function() {
    var change, formattedPrice, futuresPrice, month, oldPrice, request, result, rows, span;
    var firstUpdate = true;
    var refreshButton = $('.markets.panel .title i');

    function updatePrice(element, newPrice) {
      element.removeClass('loading');
      span = element.find('span');
      oldPrice = numeral(span.text()).value();
      formattedPrice = numeral(newPrice / 100).format('$0,0.0000');

      // Update price and add class for flashing positive or negative
      span.text(formattedPrice);
      span.removeClass();

      if (!firstUpdate) {
        if (newPrice > oldPrice) {
          span.addClass('positive');
        } else if (newPrice < oldPrice) {
          span.addClass('negative');
        } else {
          span.addClass('none');
        }
      }
    }

    function updateRow(row, result) {
      // Add the month and year to the month column and remove the loading class
      month = $(row.find('.month'));
      month.text(result.month);
      month.removeClass('loading');

      // Update the change column
      change = $(row.find('.change'));
      updatePrice(change, result.netChange);
      change.removeClass().addClass('change number');
      // Add a class to the change column depending on a positive or negative number
      if (result.netChange > 0) {
        change.addClass('positive');
      } else if (result.netChange < 0) {
        change.addClass('negative');
      } else {
        change.addClass('none');
      }

      // The rest of the prices can be updated the same way
      updatePrice($(row.find('.last-price')), result.lastPrice);
    }

    function updateMarketPrices() {
      refreshButton.addClass('fa-spinner fa-pulse');
      request = $.ajax('/ajax/markets.json');

      request.done(function(data, textStatus, xhr) {
        $.each(data, function(key, value) {
          rows = $('tr.' + key);

          $.each(value, function(index, result) {
            updateRow($(rows[index]), result);
          });
        });
      });

      request.fail(function(xhr, textStatus, error) {
        console.error('activateFuturesMarkets.request.fail', xhr, textStatus, error);
      });

      request.always(function() {
        firstUpdate = false;
        refreshButton.removeClass('fa-spinner fa-pulse');
      });
    }
    updateMarketPrices();

    // Activate the polling of markets prices for updating the table.
    refreshInterval = setInterval(updateMarketPrices, 5000);
  }).exit(function() {
    // Deactivate polling
    clearInterval(refreshInterval);
  });
})();
