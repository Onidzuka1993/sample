(function() {
  "use strict";

  App.validators.presence = function(value) {
    if (value === undefined || value === null || String(value).trim().length < 1) {
      return false;
    }else{
      return true;
    }
  };
})();
