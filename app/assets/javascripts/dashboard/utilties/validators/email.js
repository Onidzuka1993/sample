(function() {
  "use strict";

  App.validators.email = function(email) {
    var r = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    if (r.test(email)){
      return true;
    }else{
      return false;
    }
  };
})();
