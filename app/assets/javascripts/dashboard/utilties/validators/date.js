(function() {
  "use strict";

  App.validators.date = function(date) {
    var r = /^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$/;
    if (r.test(date)){
      return true;
    }else{
      return false;
    }
  };
})();
