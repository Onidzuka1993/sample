(function() {
  "use strict";

  var STRING_UNDERSCORE_REGEXP = (/[ -]/g);
  var STRING_DECAMELIZE_REGEXP = (/([a-z\d])([A-Z])/g);

  /**
    Returns the lowerCamelCase form of a string.
    @see http://emberjs.com/api/classes/Ember.String.html

    ```js
    import camelize from 'camelize';

    let key = camelize('user-name');
    // "userName"
    ```

    @method dasherize
    @param {String} string
    @return {String}
  */

  App.underscore = function(string) {
    return string.replace(STRING_DECAMELIZE_REGEXP, '$1_$2')
                 .toLowerCase()
                 .replace(STRING_UNDERSCORE_REGEXP, '_');
  };
})();
