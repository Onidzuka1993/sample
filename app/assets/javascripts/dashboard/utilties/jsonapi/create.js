/*
  App.api.create('User', {}, {});
  --> user{}
  !-> errors
*/

(function() {
  "use strict"

  App.api.create = function(model, options, data) {
    var url = App.api.requestPath(model);
    var path = url;
    var requestUrl = path + App.api.queryStringFromOptions(options);
    return App.api.sendRequest(requestUrl, 'POST', data);
  };
})();
