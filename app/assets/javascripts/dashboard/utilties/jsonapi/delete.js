/*
  App.api.delete('User', 1, {});
  --> true
  !-> errors
*/

(function() {
  "use strict"

  App.api.delete = function(model, id, options) {
    var url = App.api.requestPath(model);
    var path = url + '/' + id;
    var requestUrl = path + App.api.queryStringFromOptions(options);
    return App.api.sendRequest(requestUrl, 'DELETE');
  };
})();
