/*
  App.api.read('User', 1, {});
  --> user{}
  !-> errors

  App.api.readAll('User', {});
  --> users[]
  !-> errors
*/

(function() {
  "use strict"

  App.api.read = function(model, id, options) {
    var url = App.api.requestPath(model);
    var path = url + '/' + id;
    var requestUrl = path + App.api.queryStringFromOptions(options);
    return App.api.sendRequest(requestUrl, 'GET');
  };

  App.api.readAll = function(model, options) {
    var path = App.api.requestPath(model);
    var requestUrl = path + App.api.queryStringFromOptions(options);
    return App.api.sendRequest(requestUrl, 'GET');
  };

  App.api.get = App.api.read;
  App.api.getAll = App.api.readAll;
})();
