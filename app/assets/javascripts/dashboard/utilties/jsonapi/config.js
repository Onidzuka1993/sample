
(function() {
  "use strict"

  function url() {
    if (App.api.version === undefined) {
      return '/api';
    }else{
      return '/api/v' + App.api.version;
    }
  }

  function toPath(model) {
    var pluralModel = App.pluralize(model);
    var modelPath = App.dasherize(pluralModel);
    return url() + '/' + modelPath;
  }

  App.api.sendRequest = function(requestUrl, method, data) {
    return $.ajax({
      url: requestUrl,
      type: method,
      data: data
    });
  }

  App.api.requestPath = function(model) {
    return toPath(model);
  };

  App.api.queryStringFromOptions = function(options) {
    var queries = [];

    if (options.yearId === undefined) {
      options.yearId = App.year();
    }

    Object.keys(options).forEach(function(key) {
      queries.push(App.underscore(key) + "=" + options[key]);
    });

    if (queries.length > 0) {
      return '?'+queries.join('&');
    } else {
      return '';
    }
  }
})();
