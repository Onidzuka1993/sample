/*
  App.api.update('User', 1, {}, {});
  --> user{}
  !-> errors
*/

(function() {
  "use strict"

  App.api.update = function(model, id, options, data) {
    var url = App.api.requestPath(model);
    if (id === undefined || id === null) {
      var path = url + '/';
    }else{
      var path = url + '/' + id;
    }
    var requestUrl = path + App.api.queryStringFromOptions(options);
    return App.api.sendRequest(requestUrl, 'PATCH', data);
  };
})();
