(function() {
  "use strict";

  function lastCharacter(string) {
    return string.substr(string.length - 1)
  }

  function lessLastCharacter(string) {
    return string.substr(0, string.length - 1)
  }

  App.pluralize = function(string) {
    if (lastCharacter(string) === "s"){
      return string + "es";
    }

    if (lastCharacter(string) === "y"){
      return lessLastCharacter(string) + "ies";
    }

    return string + "s";
  };
})();
