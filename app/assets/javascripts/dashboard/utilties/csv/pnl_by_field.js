(function() {
  "use strict";

  function parseResults(resultRows) {
    var resultsList = [];
    var resultAttrs;
    var row;
    for (var i = 0; i < resultRows.length; i++) {
      row = resultRows[i];
      resultAttrs = [];
      resultAttrs.push("\"" + row.$els.fieldName.innerText + "\"");
      resultAttrs.push("\"" + row.$els.cropName.innerText + "\"");
      resultAttrs.push("\"" + row.$els.plantedAcres.innerText + "\"");
      resultAttrs.push("\"" + row.$els.yield.innerText + "\"");
      resultAttrs.push("\"" + row.$els.revenue.innerText + "\"");
      resultAttrs.push("\"" + row.$els.expenses.innerText + "\"");
      resultAttrs.push("\"" + row.$els.breakEven.innerText + "\"");
      resultAttrs.push("\"" + row.$els.income.innerText + "\"");
      resultsList.push(resultAttrs);
    }
    return resultsList;
  }

  function buildDataList(resultsView) {
    var resultRows = parseResults(resultsView.$refs.profitLossByFieldTable.$refs.resultRow);

    var totalRevenue = resultsView.$refs.profitLossByFieldTable.$els.totalRevenue.innerText;
    var totalExpenses = resultsView.$refs.profitLossByFieldTable.$els.totalExpenses.innerText;
    var totalIncome = resultsView.$refs.profitLossByFieldTable.$els.totalIncome.innerText;

    var totalRow = ["Totals", "","","","\"" + totalRevenue + "\"", "\"" + totalExpenses + "\"", "", "\"" + totalIncome + "\""];

    resultRows.push(totalRow);
    return resultRows;
  }

  App.csvGenerator.downloadPnlByField = function(resultsView) {
    var headers = ["Field", "Crop", "Acres", "Yield", "Revenue", "Expenses", "Break Even", "Profit/Loss"];
    var data = buildDataList(resultsView);
    App.csvGenerator.generate({
      headers: headers,
      data: data,
      fileName: App.year() + "_profit_and_loss_by_field.csv"
    });
  };

})();
