(function() {
  "use strict";

  function profitLossByCropTable(cropRows, totalProfitLoss) {
    var rows = {
      name:             [],
      acres:            [],
      breakEven:        [],
      averageCashPrice: [],
      incomePerAcre:    [],
      income:           []
    };
    var row;

    for (var i = 0; i < cropRows.length; i++) {
      row = cropRows[i];
      rows.name.push({ align: 'left', text: row.$els.name.innerText });
      rows.acres.push({ text: row.$els.acres.innerText });
      rows.breakEven.push({ text: row.$els.breakEven.innerText });
      rows.averageCashPrice.push({ text: row.$els.averageCashPrice.innerText });
      rows.incomePerAcre.push({ text: row.$els.incomePerAcre.innerText });
      rows.income.push({ text: row.$els.income.innerText });
    }

    rows.income.push({
      borderColor: '#fff',
      font:        'Helvetica-Bold',
      text:        totalProfitLoss
    });

    var columns = [{
      header: {
        align: 'left',
        text:  'Crop'
      },
      rows: rows.name,
      width: 0.17
    }, {
      header: { text: 'Acres' },
      rows: rows.acres,
      width: 0.11
    }, {
      header: { text: 'Break Even' },
      rows: rows.breakEven,
      width: 0.17
    }, {
      header: { text: 'Avg Cash Price' },
      rows: rows.averageCashPrice,
      width: 0.21
    }, {
      header: { text: 'P/L per Acre' },
      rows: rows.incomePerAcre,
      width: 0.18
    }, {
      header: { text: 'Total P/L' },
      rows: rows.income,
      width: 0.16
    }];

    var tableOptions = {
      cellAlign:   'right',
      headerAlign: 'right'
    };

    return new PDFTable(columns, tableOptions);
  }

  function profitLossBreakdownTable(labelColumn, cropColumns, totalColumn) {
    var baseFontSize = 10.5;
    var rowCount = labelColumn.children.length;
    var labelRows = [];
    var i, row, costRow, lastRow, totalRow, revenueRow;

    for (i = 1; i < rowCount; i++) {
      row = labelColumn.children[i];
      costRow = row.className.indexOf('cost') > -1;
      totalRow = row.className.split(" ").indexOf('total') > -1;
      revenueRow = row.className.split(" ").indexOf('revenue') > -1;
      lastRow = i + 1 === rowCount;

      labelRows.push({
        align:       'left',
        borderColor: lastRow ? '#fff' : '#ccc',
        font:        totalRow ? 'Helvetica-Bold' : 'Helvetica',
        fontSize:    costRow || revenueRow ? baseFontSize - 2 : baseFontSize,
        text:        row.innerText
      });
    }

    var columns = [{
      header: { text: ' ' },
      rows:   labelRows
    }];

    var cropColumn;
    for (i = 0; i < cropColumns.length; i++) {
      cropColumn = cropColumns[i];

      var cropName = cropColumn.$el.children[0].innerText;
      var cropRows = [];

      for (var ii = 1; ii < rowCount; ii++) {
        row = cropColumn.$el.children[ii];
        costRow = row.className.indexOf('cost') > -1;
        totalRow = row.className.split(" ").indexOf('total') > -1;
        revenueRow = row.className.split(" ").indexOf('revenue') > -1;
        lastRow = ii + 1 === rowCount;

        cropRows.push({
          borderColor: lastRow ? '#fff' : '#ccc',
          font:        totalRow ? 'Helvetica-Bold' : 'Helvetica',
          fontSize:    costRow || revenueRow ? baseFontSize - 2 : baseFontSize,
          text:        row.innerText
        });
      }

      columns.push({
        header: { text: cropName },
        rows:   cropRows
      });
    }

    if (totalColumn != null) {
      var totalRows = [];
      for (i = 1; i < rowCount; i++) {
        row = totalColumn.children[i];
        costRow = row.className.indexOf('cost') > -1;
        totalRow = row.className.split(" ").indexOf('total') > -1;
        revenueRow = row.className.split(" ").indexOf('revenue') > -1;
        lastRow = i + 1 === rowCount;

        totalRows.push({
          borderColor: lastRow ? '#fff' : '#ccc',
          font:        totalRow ? 'Helvetica-Bold' : 'Helvetica',
          fontSize:    costRow || revenueRow ? baseFontSize - 2 : baseFontSize,
          text:        row.innerText
        });
      }

      columns.push({
        header: { text: 'Total' },
        rows:   totalRows
      });
    }

    var tableOptions = {
      cellAlign:      'right',
      cellFontSize:   baseFontSize,
      headerAlign:    'right',
      headerFontSize: baseFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function generate(resultsView, year) {
    pdfBuilder = App.pdfGenerator.landscapePageLayout(year, 'P&L by Crop', {data: [{label: "Entity", value: resultsView.entity}, {label: "Owner", value: resultsView.selectedOwner}]});
    var cropRows = resultsView.$refs.profitLossByCropTable.$refs.cropRow;
    var totalProfitLoss = resultsView.$refs.profitLossByCropTable.$els.totalProfitLoss.innerText;

    pdfBuilder.addHeading('Profit/Loss by Crop');
    pdfBuilder.addTable(profitLossByCropTable(cropRows, totalProfitLoss));

    var labelColumn = resultsView.$refs.profitLossBreakdownTable.$els.labelColumn;
    var cropColumns = resultsView.$refs.profitLossBreakdownTable.$refs.cropColumn;
    var totalColumn = resultsView.$refs.profitLossBreakdownTable.$els.totalColumn;
    var groupedCropColumns = [];

    if (cropColumns.length === 5) {
      groupedCropColumns = [cropColumns.slice(0, 3), cropColumns.slice(3, 5)];
    } else if (cropColumns.length > 5 && cropColumns.length < 9) {
      groupedCropColumns = [cropColumns.slice(0, 4), cropColumns.slice(4, cropColumns.length)];
    } else if (cropColumns.length === 9) {
      groupedCropColumns = [cropColumns.slice(0, 3), cropColumns.slice(3, 6), cropColumns.slice(6, 9)];
    } else if (cropColumns.length === 10) {
      groupedCropColumns = [cropColumns.slice(0, 4), cropColumns.slice(4, 7), cropColumns.slice(7, 10)];
    } else if (cropColumns.length === 11 || cropColumns.length === 12) {
      groupedCropColumns = [cropColumns.slice(0, 4), cropColumns.slice(4, 8), cropColumns.slice(8, cropColumns.length)];
    } else {
      groupedCropColumns = [cropColumns];
    }

    for (var i = 0; i < groupedCropColumns.length; i++) {
      pdfBuilder.addPage();

      // Only add the total column to the last page
      if (groupedCropColumns.length === i + 1) {
        pdfBuilder.addTable(profitLossBreakdownTable(labelColumn, groupedCropColumns[i], totalColumn));
      } else {
        pdfBuilder.addTable(profitLossBreakdownTable(labelColumn, groupedCropColumns[i]));
      }
    }
  }

  function parseResults(resultRows) {
    var resultsList = [];
    var resultAttrs;
    var row;
    for (var i = 0; i < resultRows.length; i++) {
      row = resultRows[i];
      resultAttrs = [];
      resultAttrs.push("\"" + row.$els.fieldName.innerText + "\"");
      resultAttrs.push("\"" + row.$els.cropName.innerText + "\"");
      resultAttrs.push("\"" + row.$els.plantedAcres.innerText + "\"");
      resultAttrs.push("\"" + row.$els.yield.innerText + "\"");
      resultAttrs.push("\"" + row.$els.revenue.innerText + "\"");
      resultAttrs.push("\"" + row.$els.expenses.innerText + "\"");
      resultAttrs.push("\"" + row.$els.breakEven.innerText + "\"");
      resultAttrs.push("\"" + row.$els.income.innerText + "\"");
      resultsList.push(resultAttrs);
    }
    return resultsList;
  }

  function buildDataList(resultsView) {

    var labelColumn = resultsView.$refs.profitLossBreakdownTable.$els.labelColumn;
    var cropColumns = resultsView.$refs.profitLossBreakdownTable.$refs.cropColumn;
    var totalColumn = resultsView.$refs.profitLossBreakdownTable.$els.totalColumn;

    console.log(labelColumn.children[0].innerText);


    var rows = [];
    var row;
    for (var i = 0; i < labelColumn.children.length; i++) {
      row = [];

      // Add Label to row
      row.push("\"" + labelColumn.children[i].innerText + "\"");

      // Add Crops to row
      for (var j = 0; j < cropColumns.length; j++) {
        row.push("\"" + cropColumns[j].$el.children[i].innerText + "\"");
      }

      // Add Totals for row
      row.push("\"" + totalColumn.children[i].innerText + "\"");

      // Add to row list
      rows.push(row);
    }
    return rows;
  }

  App.csvGenerator.downloadPnlByCrop = function(resultsView) {
    var data = buildDataList(resultsView);
    App.csvGenerator.generate({
      data: data,
      fileName: App.year() + "_profit_and_loss_by_crop.csv"
    });
  };

})();
