(function() {
  "use strict";

  App.csvGenerator.generate = function(args) {
    var headers = args.headers || [];
    var data = args.data || [];
    var fileName = args.fileName || "harvest_profit.csv";
    var totalRowCount = data.length;
    var contentType = "text/csv;charset=utf-8;header=present";
    var csvContent = "";
    var rowContent = "";

    if (headers.length > 0) {
      csvContent += headers.join(",") +  "\r\n";
    }

    for (var i = 0; i < data.length; i++) {
      rowContent = data[i].join(",");
      csvContent += (i + 1) < totalRowCount ? rowContent + "\r\n" : rowContent;
    }

    var blob = new Blob([csvContent], {type: contentType});
    saveAs(blob, fileName);
  };
})();
