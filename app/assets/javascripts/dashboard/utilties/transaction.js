(function() {
  "use strict";

  App.beginTransaction = function(name) {
    var atatus = atatus || window.atatus;
    // quit if atatus isn't on the page
    if (!atatus || !atatus.config) {return;}
    atatus.beginTransaction(name);
  };

  App.endTransaction = function(name) {
    var atatus = atatus || window.atatus;
    // quit if atatus isn't on the page
    if (!atatus || !atatus.config) {return;}
    atatus.endTransaction(name);
  };

  App.failTransaction = function(name) {
    var atatus = atatus || window.atatus;
    // quit if atatus isn't on the page
    if (!atatus || !atatus.config) {return;}
    atatus.failTransaction(name);
  };

  App.cancelTransaction = function(name) {
    var atatus = atatus || window.atatus;
    // quit if atatus isn't on the page
    if (!atatus || !atatus.config) {return;}
    atatus.cancelTransaction(name);
  };
})();
