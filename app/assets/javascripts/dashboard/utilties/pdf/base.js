//= require pdf-builder/harvest-profit-pdf-header
//= require pdf-builder/basic-pdf-footer

//= require pdf-builder/harvest-profit-pdf-footer
//= require pdf-builder/basic-pdf-header
//= require save-as
(function() {
  "use strict";

  function fetchYear(year) {
    if (year === undefined || year === null) {
      var matches = window.location.pathname.match(/\/(\d{4})/g);
      if (matches === null) {
        year = (new Date()).getFullYear().toString();
      }else{
        year = matches[0].replace("/", "");
      }
    }
    return year;
  }

  App.pdfGenerator.landscapePageLayout = function(year, title, footer_data) {
    return new PDFBuilder({
      header: new BasicPDFHeader(footer_data || {}, fetchYear(year)),
      footer: new HarvestProfitPDFFooter({}),
      layout: 'landscape',
      margins: { bottom: -30, left: 30, right: 30, top: 30 },
      subHeadingFontColor: '#555555',
      subHeadingFontSize: 10,
      includePageNumber: true,
      title: title
    });
  };

  App.pdfGenerator.pageLayout = function(year, title, footer_data) {
    return new PDFBuilder({
      header: new BasicPDFHeader(footer_data || {}, fetchYear(year)),
      footer: new HarvestProfitPDFFooter({}),
      margins: { bottom: -30, left: 30, right: 30, top: 30 },
      subHeadingFontColor: '#555555',
      subHeadingFontSize: 10,
      includePageNumber: true,
      title: title
    });
  };

  App.pdfGenerator.print = function(builder, id) {
    builder.generateBlobURL().then(function(url) {
      $('#' + id)[0].src = url;

      setTimeout(function() {
        var iframe = window.frames[id];
        iframe = iframe.contentWindow || iframe;
        iframe.focus();
        iframe.print();
      }, 100);
    });
  };

  App.pdfGenerator.download = function(builder, name) {
    builder.generateBlob().then(function(blob) {
      saveAs(blob, name);
    });
  };
})();
