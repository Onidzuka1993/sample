
//   EXPOSED AS   App.pdfGenerator.printcontracts(data, year, id)
//   EXPOSED AS   App.pdfGenerator.downloadContracts(data, year, name)

//= require pdf-builder/pdf-builder
//= require pdf-builder/pdf-table

(function() {
  "use strict";

  var pdfBuilder;
  var tableFontSize = 9;

  var bushCol = 0.09;
  var locCol = 0.15;
  var locSpace = '     ';
  var futMonCol = 0.10;
  var priceCol = 0.15;
  var basCol = 0.07;
  var disCol = 0.09;
  var cashCol = 0.11;
  var prodCol = 0.12;
  var conCol = 0.12;

  var printedHeader = false;

  function basisTable(data) {
    var rows = {
      bushels:           [],
      deliveryLocation:  [],
      futuresMonth:      [],
      estFuturesPrice:   [],
      basis:             [],
      cashPrice:         [],
      contractNumber:    [],
      productionPercent: []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.bushels.push({text: row.pdf_bushels });
      rows.deliveryLocation.push({align: 'left', text: locSpace + row.pdf_delivery_location });
      rows.futuresMonth.push({align: 'left', text: row.pdf_futures_month });
      rows.estFuturesPrice.push({text: row.pdf_est_futures_price });
      rows.basis.push({text: row.pdf_basis });
      rows.cashPrice.push({text: row.pdf_cash_price });
      rows.contractNumber.push({text: row.pdf_contract_number });
      rows.productionPercent.push({text: row.pdf_production_percent });
    }

    var columns = [{
      header: {text:  'Bushels' },
      rows: rows.bushels,
      width: bushCol
    }, {
      header: {
        align: 'left',
        text: locSpace + 'Delivery Location'
      },
      rows: rows.deliveryLocation,
      width: locCol
    }, {
      header: {
        align: 'left',
        text: 'Futures Month'
      },
      rows: rows.futuresMonth,
      width: futMonCol
    }, {
      header: { text: 'Est. Futures Price'  },
      rows: rows.estFuturesPrice,
      width: priceCol
    }, {
      header: { text: 'Basis'  },
      rows: rows.basis,
      width: basCol + disCol
    }, {
      header: { text: 'Cash Price'  },
      rows: rows.cashPrice,
      width: cashCol
    }, {
      header: { text: 'Contract Number'  },
      rows: rows.contractNumber,
      width: conCol
    }, {
      header: { text: '% of Production'  },
      rows: rows.productionPercent,
      width: prodCol
    }];

    var tableOptions = {
      cellAlign:   'right',
      headerAlign: 'right',
      cellFontSize: tableFontSize,
      headerFontSize: tableFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function cashSaleTable(data) {
    var rows = {
      bushels:           [],
      deliveryLocation:  [],
      futuresPrice:      [],
      basis:             [],
      discount:          [],
      cashPrice:         [],
      productionPercent: []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.bushels.push({text: row.pdf_bushels });
      rows.deliveryLocation.push({align: 'left', text: locSpace + row.pdf_delivery_location });
      rows.futuresPrice.push({text: row.pdf_futures_price });
      rows.basis.push({text: row.pdf_basis });
      rows.discount.push({text: row.pdf_discount });
      rows.cashPrice.push({text: row.pdf_cash_price });
      rows.productionPercent.push({text: row.pdf_production_percent });
    }

    var columns = [{
      header: {text:  'Bushels' },
      rows: rows.bushels,
      width: bushCol
    }, {
      header: {
        align: 'left',
        text: locSpace + 'Delivery Location'
      },
      rows: rows.deliveryLocation,
      width: locCol + futMonCol
    }, {
      header: { text: 'Futures Price'  },
      rows: rows.futuresPrice,
      width: priceCol
    }, {
      header: { text: 'Discount'  },
      rows: rows.discount,
      width: disCol
    }, {
      header: { text: 'Basis'  },
      rows: rows.basis,
      width: basCol
    }, {
      header: { text: 'Cash Price'  },
      rows: rows.cashPrice,
      width: cashCol
    }, {
      header: { text: '% of Production'  },
      rows: rows.productionPercent,
      width: prodCol + conCol
    }];

    var tableOptions = {
      cellAlign:   'right',
      headerAlign: 'right',
      cellFontSize: tableFontSize,
      headerFontSize: tableFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function forwardTable(data) {
    var rows = {
      bushels:           [],
      deliveryLocation:  [],
      deliveryPeriod:    [],
      futuresPrice:      [],
      basis:             [],
      discount:          [],
      cashPrice:         [],
      contractNumber:    [],
      productionPercent: []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.bushels.push({text: row.pdf_bushels });
      rows.deliveryLocation.push({align: 'left', text: locSpace + row.pdf_delivery_location });
      rows.deliveryPeriod.push({align: 'left', text: row.pdf_delivery_period });
      rows.futuresPrice.push({text: row.pdf_futures_price });
      rows.basis.push({text: row.pdf_basis });
      rows.discount.push({text: row.pdf_discount });
      rows.cashPrice.push({text: row.pdf_cash_price });
      rows.contractNumber.push({text: row.pdf_contract_number });
      rows.productionPercent.push({text: row.pdf_production_percent });
    }

    var columns = [{
      header: {text:  'Bushels' },
      rows: rows.bushels,
      width: bushCol
    }, {
      header: {
        align: 'left',
        text: locSpace + 'Delivery Location'
      },
      rows: rows.deliveryLocation,
      width: locCol
    }, {
      header: {
        align: 'left',
        text: 'Delivery Period'
      },
      rows: rows.deliveryPeriod,
      width: futMonCol
    }, {
      header: { text: 'Futures Price'  },
      rows: rows.futuresPrice,
      width: priceCol
    }, {
      header: { text: 'Discount'  },
      rows: rows.discount,
      width: disCol
    }, {
      header: { text: 'Basis'  },
      rows: rows.basis,
      width: basCol
    }, {
      header: { text: 'Cash Price'  },
      rows: rows.cashPrice,
      width: cashCol
    }, {
      header: { text: 'Contract Number'  },
      rows: rows.contractNumber,
      width: conCol
    }, {
      header: { text: '% of Production'  },
      rows: rows.productionPercent,
      width: prodCol
    }];

    var tableOptions = {
      cellAlign:   'right',
      headerAlign: 'right',
      cellFontSize: tableFontSize,
      headerFontSize: tableFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function futuresTable(data) {
    var rows = {
      bushels:           [],
      longShort:         [],
      futuresMonth:      [],
      futuresPrice:      [],
      estBasis:          [],
      estCashPrice:      [],
      size:              [],
      numberOfContracts: []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.bushels.push({text: row.pdf_bushels });
      rows.longShort.push({align: 'left', text: locSpace + row.pdf_long_short });
      rows.futuresMonth.push({align: 'left', text: row.pdf_futures_month });
      rows.futuresPrice.push({text: row.pdf_futures_price });
      rows.estBasis.push({text: row.pdf_est_basis });
      rows.estCashPrice.push({text: row.pdf_est_cash_price });
      rows.size.push({text: row.pdf_size});
      rows.numberOfContracts.push({text: row.pdf_number_of_contracts });
    }

    var columns = [{
      header: {text:  'Bushels' },
      rows: rows.bushels,
      width: bushCol
    }, {
      header: {
        align: 'left',
        text: locSpace + 'Long/Short'
      },
      rows: rows.longShort,
      width: locCol
    }, {
      header: {
        align: 'left',
        text: 'Futures Month'
      },
      rows: rows.futuresMonth,
      width: futMonCol
    }, {
      header: { text: 'Futures Price'  },
      rows: rows.futuresPrice,
      width: priceCol
    }, {
      header: { text: 'Est. Basis'  },
      rows: rows.estBasis,
      width: basCol + disCol
    }, {
      header: { text: 'Est. Cash Price'  },
      rows: rows.estCashPrice,
      width: cashCol
    }, {
      header: { text: 'Size'  },
      rows: rows.size,
      width: conCol
    }, {
      header: { text: '# of Contracts'  },
      rows: rows.numberOfContracts,
      width: prodCol
    }];

    var tableOptions = {
      cellAlign:   'right',
      headerAlign: 'right',
      cellFontSize: tableFontSize,
      headerFontSize: tableFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function hedgeToArriveTable(data) {
    var rows = {
      bushels:           [],
      deliveryLocation:  [],
      futuresMonth:      [],
      futuresPrice:      [],
      estBasis:          [],
      cashPrice:         [],
      contractNumber:    [],
      productionPercent: []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.bushels.push({text: row.pdf_bushels });
      rows.deliveryLocation.push({align: 'left', text: locSpace + row.pdf_delivery_location });
      rows.futuresMonth.push({align: 'left', text: row.pdf_futures_month });
      rows.futuresPrice.push({text: row.pdf_futures_price });
      rows.estBasis.push({text: row.pdf_est_basis });
      rows.cashPrice.push({text: row.pdf_cash_price });
      rows.contractNumber.push({text: row.pdf_contract_number });
      rows.productionPercent.push({text: row.pdf_production_percent });
    }

    var columns = [{
      header: {text:  'Bushels' },
      rows: rows.bushels,
      width: bushCol
    }, {
      header: {
        align: 'left',
        text: locSpace + 'Delivery Location'
      },
      rows: rows.deliveryLocation,
      width: locCol
    }, {
      header: {
        align: 'left',
        text: 'Futures Month'
      },
      rows: rows.futuresMonth,
      width: futMonCol
    }, {
      header: { text: 'Est. Futures Price'  },
      rows: rows.futuresPrice,
      width: priceCol
    }, {
      header: { text: 'Est. Basis'  },
      rows: rows.estBasis,
      width: basCol + disCol
    }, {
      header: { text: 'Cash Price'  },
      rows: rows.cashPrice,
      width: cashCol
    }, {
      header: { text: 'Contract Number'  },
      rows: rows.contractNumber,
      width: conCol
    }, {
      header: { text: '% of Production'  },
      rows: rows.productionPercent,
      width: prodCol
    }];

    var tableOptions = {
      cellAlign:   'right',
      headerAlign: 'right',
      cellFontSize: tableFontSize,
      headerFontSize: tableFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function optionsTable(data) {
    var rows = {
      numberOfContracts: [],
      strategy:          [],
      futuresMonth:      [],
      strikePrice:       [],
      premium:           [],
      notes:             []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.numberOfContracts.push({text: row.pdf_number_of_contracts });
      rows.strategy.push({align: 'left', text: locSpace + row.pdf_option_strategy });
      rows.futuresMonth.push({align: 'left', text: row.pdf_futures_month });
      rows.strikePrice.push({text: row.pdf_strike_price });
      rows.premium.push({text: row.pdf_premium });
      rows.notes.push({text: locSpace + row.pdf_notes, align: 'left' });
    }

    var columns = [{
      header: {text:  '# of Contracts' },
      rows: rows.numberOfContracts,
      width: bushCol
    }, {
      header: {
        align: 'left',
        text: locSpace + 'Option Strategy'
      },
      rows: rows.strategy,
      width: locCol
    }, {
      header: {
        align: 'left',
        text: 'Futures Month'
      },
      rows: rows.futuresMonth,
      width: futMonCol
    }, {
      header: { text: 'Strike Price'  },
      rows: rows.strikePrice,
      width: priceCol
    }, {
      header: { text: 'Premium'  },
      rows: rows.premium,
      width: disCol
    }, {
      header: { text: locSpace + 'Notes', align: 'left'  },
      rows: rows.notes,
      width: cashCol + prodCol + basCol + conCol
    }];

    var tableOptions = {
      cellAlign:   'right',
      headerAlign: 'right',
      cellFontSize: tableFontSize,
      headerFontSize: tableFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function cropSummaryTable(crop) {
    var rows = {
      costAcre:  [],
      costUnit:  [],
      acre:       [],
      yield:      [],
      production: []
    };

    rows.costAcre.push({text: numeral(crop.costPerAcre).format('$0,0.00')});
    rows.costUnit.push({text: numeral(crop.costPerUnit).format('$0,0.00')});
    rows.acre.push({text: numeral(crop.acres).format('0,0.0')});
    rows.yield.push({text: numeral(crop.yield).format('0,0')});
    rows.production.push({text: numeral(crop.production).format('0,0')});

    var columns = [{
      header: {text:  'Cost / Acre' },
      rows: rows.costAcre,
      width: .2
    }, {
      header: {text: 'Cost / Unit'},
      rows: rows.costUnit,
      width: .2
    }, {
      header: {text: 'Acres'},
      rows: rows.acre,
      width: .2
    }, {
      header: { text: 'Yield'  },
      rows: rows.yield,
      width: .2
    }, {
      header: { text: 'Production'  },
      rows: rows.production,
      width: .2
    }];

    var tableOptions = {
      cellAlign:   'left',
      headerAlign: 'left',
      cellFontSize: tableFontSize,
      headerFontSize: tableFontSize
    };

    return new PDFTable(columns, tableOptions);
  }

  function generateSection(data, func, title) {
    if (data === undefined || data === null || data.length < 1) {
      return;
    }

    if (!printedHeader) {
      pdfBuilder.addPage();
      pdfBuilder.addHeading('Open Orders');
      printedHeader = true;
    }

    pdfBuilder.addSubHeading(title);
    pdfBuilder.addTable(func.call(this, data));
  }

  function generate(data, resView, year) {

    var pdfLabel = [];
    if (data.entity !== undefined || data.entity !== null) {
      pdfLabel.push({label: "Entity", value: data.entity});
    }

    pdfBuilder = App.pdfGenerator.landscapePageLayout(year, data.crop + ' Contracts', {data: pdfLabel});
    var crop = resView.crops[0];
    pdfBuilder.addHeading('Summary for ' + crop.name);
    pdfBuilder.addTable(cropSummaryTable(crop));
    pdfBuilder.addPage();
    pdfBuilder.addHeading('Contracts for ' + data.crop);

    if (data.closed !== undefined && data.closed !== null) {
      printedHeader = true;
      generateSection(data.closed.basis, basisTable, 'Basis');
      generateSection(data.closed.cashSale, cashSaleTable, 'Cash Sale');
      generateSection(data.closed.forward, forwardTable, 'Forward');
      generateSection(data.closed.futures, futuresTable, 'Futures');
      generateSection(data.closed.hedgeToArrive, hedgeToArriveTable, 'Hedge To Arrive');
      generateSection(data.closed.options, optionsTable, 'Options');
    }
    if (data.open !== undefined && data.open !== null) {
      printedHeader = false;
      generateSection(data.open.basis, basisTable, 'Basis');
      generateSection(data.open.cashSale, cashSaleTable, 'Cash Sale');
      generateSection(data.open.forward, forwardTable, 'Forward');
      generateSection(data.open.futures, futuresTable, 'Futures');
      generateSection(data.open.hedgeToArrive, hedgeToArriveTable, 'Hedge To Arrive');
      generateSection(data.open.options, optionsTable, 'Options');
    }
  }

  App.pdfGenerator.printContracts = function(data, resView, id, year) {
    generate(data, resView, year);
    App.pdfGenerator.print(pdfBuilder, id);
  };

  App.pdfGenerator.downloadContracts = function(data, resView, name, year) {
    generate(data, resView, year);
    App.pdfGenerator.download(pdfBuilder, name);
  };
})();
