
//   EXPOSED AS   App.pdfGenerator.printProducts(data, year, id)
//   EXPOSED AS   App.pdfGenerator.downloadProducts(data, year, name)

//= require pdf-builder/pdf-builder
//= require pdf-builder/pdf-table

(function() {
  "use strict";

  var pdfBuilder;
  var tableFontSize = 9;

  var tableOptions = {
    cellAlign:   'left',
    headerAlign: 'left',
    cellFontSize: tableFontSize,
    headerFontSize: tableFontSize
  };

  var cropCol = 0.30;
  var nameCol = 0.26;
  var costCol = 0.16;
  var unitCol = 0.13;
  var liqCol = 0.06;
  var denCol = 0.09;

  function defaultTable(data) {
    var rows = {
      crops:       [],
      description: [],
      cost:        [],
      unit:        []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
      rows.unit.push({text: row.units });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol
    }, {
      header: { text: 'Units' },
      rows: rows.unit,
      width: unitCol + liqCol + denCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function fertilizerTable(data) {
    var rows = {
      crops:       [],
      description: [],
      cost:        [],
      unit:        [],
      liquid:      [],
      density:     []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
      rows.unit.push({text: row.units });

      if (row.units === 'per gallon' || row.units === 'per fl oz') {
        rows.liquid.push({text: ''});
        rows.density.push({text: ''});
      }else{
        rows.liquid.push({text: row.liquid ? 'Yes' : 'No' });
        if (row.liquid) {
          rows.density.push({text: numeral(row.density).format('0,0.00') });
        }else{
          rows.density.push({text: '' });
        }
      }
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol
    }, {
      header: { text: 'Units' },
      rows: rows.unit,
      width: unitCol
    }, {
      header: { text: 'Liquid' },
      rows: rows.liquid,
      width: liqCol
    }, {
      header: { text: 'Density' },
      rows: rows.density,
      width: denCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function seedTable(data) {
    var rows = {
      crops:       [],
      description: [],
      cost:        [],
      unit:        []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
      rows.unit.push({text: row.units });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol
    }, {
      header: { text: 'Units' },
      rows: rows.unit,
      width: unitCol + liqCol + denCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function defaultTable(data) {
    var rows = {
      crops:       [],
      description: [],
      cost:        [],
      unit:        []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
      rows.unit.push({text: row.units });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol
    }, {
      header: { text: 'Units' },
      rows: rows.unit,
      width: unitCol + liqCol + denCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function cropInsuranceTable(data) {
    var rows = {
      crops:          [],
      coverageAmount: [],
      aph:            [],
      cost:           []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.coverageAmount.push({text: numeral(row.coverage_amount).format('0,0') + '%'});
      rows.aph.push({text: numeral(row.aph).format('0,0.00') });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Coverage Amount' },
      rows: rows.coverageAmount,
      width: nameCol
    }, {
      header: { text: 'APH' },
      rows: rows.aph,
      width: costCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: unitCol + liqCol + denCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function fuelTable(data) {
    var rows = {
      crops:       [],
      gallons: [],
      cost:        [],
      unit:        []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.gallons.push({text: numeral(row.gallons).format('0,0') });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
      rows.unit.push({text: row.units });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Gallons' },
      rows: rows.gallons,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol
    }, {
      header: { text: 'Units' },
      rows: rows.unit,
      width: unitCol + liqCol + denCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function laborTable(data) {
    var rows = {
      crops:    [],
      employee: [],
      cost:     []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.employee.push({text: row.name });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Employee' },
      rows: rows.employee,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol + liqCol + denCol + unitCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function repairsTable(data) {
    var rows = {
      crops:       [],
      description: [],
      cost:        []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol + liqCol + denCol + unitCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function dryingTable(data) {
    var rows = {
      crops:       [],
      cost:        [],
      unit:        []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
      rows.unit.push({text: row.units });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol
    }, {
      header: { text: 'Units' },
      rows: rows.unit,
      width: unitCol + liqCol + denCol + nameCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function paymentsDepreciationTable(data) {
    var rows = {
      crops:       [],
      description: [],
      amount:      [],
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.amount.push({text: numeral(row.price).format('$0,0.000') });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Amount' },
      rows: rows.amount,
      width: costCol + liqCol + denCol + unitCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function otherTable(data) {
    var rows = {
      crops:       [],
      description: [],
      cost:        [],
      unit:        []
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.cost.push({text: numeral(row.price).format('$0,0.000') });
      rows.unit.push({text: row.units });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Cost' },
      rows: rows.cost,
      width: costCol
    }, {
      header: { text: 'Units' },
      rows: rows.unit,
      width: unitCol + liqCol + denCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function miscRevenueTable(data) {
    var rows = {
      crops:       [],
      description: [],
      amount:      [],
    };

    var row;

    for (var i = 0; i < data.length; i++) {
      row = data[i];
      rows.crops.push({text: row.cropNames.join(', ') });
      rows.description.push({text: row.name });
      rows.amount.push({text: numeral(row.amount).format('$0,0.000') });
    }

    var columns = [{
      header: { text:  'Crops' },
      rows: rows.crops,
      width: cropCol
    }, {
      header: { text: 'Description' },
      rows: rows.description,
      width: nameCol
    }, {
      header: { text: 'Amount' },
      rows: rows.amount,
      width: costCol  + liqCol + denCol + unitCol
    }];

    return new PDFTable(columns, tableOptions);
  }

  function generateSection(data, func, title) {
    if (data === undefined || data === null || data.length < 1) {
      return;
    }

    pdfBuilder.addSubHeading(title);
    pdfBuilder.addTable(func.call(this, data));
  }

  function generate(inputs, revenues, entities, years) {

    var pdfLabel = [];
    if (entities !== undefined || entities !== null) {
      if (entities.length > 1) {
        pdfLabel.push({label: "Entities", value: entities.join(", ")});
      }else{
        pdfLabel.push({label: "Entity", value: entities[0]});
      }
    }

    pdfBuilder = App.pdfGenerator.pageLayout(years[0], 'Inputs', {data: pdfLabel});

    var input;
    for (var i = 0; i < inputs.length; i++) {
      input = inputs[i];
      switch (input.inputType) {
        case 'Fertilizer':
          generateSection(input.products, fertilizerTable, 'Fertilizer');
          break;
        case 'Seed':
          generateSection(input.products, seedTable, 'Seed');
          break;
        case 'CropInsurance':
          generateSection(input.products, cropInsuranceTable, 'Crop Insurance');
          break;
        case 'Fuel':
          generateSection(input.products, fuelTable, 'Fuel');
          break;
        case 'Labor':
          generateSection(input.products, laborTable, 'Labor');
          break;
        case 'Repair':
          generateSection(input.products, repairsTable, 'Repairs');
          break;
        case 'Drying':
          generateSection(input.products, dryingTable, 'Drying');
          break;
        case 'PaymentsDepreciation':
          generateSection(input.products, paymentsDepreciationTable, 'Payments/Depreciation');
          break;
        default:
          generateSection(input.products, defaultTable, input.inputType);
      }
    }
    generateSection(revenues, miscRevenueTable, 'Misc Revenue');
  }

  App.pdfGenerator.printProducts = function(inputs, revenues, entities, years) {
    generate(inputs, revenues, entities, years);
    App.pdfGenerator.print(pdfBuilder, App.year() + "_inputs.pdf");
  };

  App.pdfGenerator.downloadProducts = function(inputs, revenues, entities, years) {
    generate(inputs, revenues, entities, years);
    App.pdfGenerator.download(pdfBuilder, App.year() + "_inputs.pdf");
  };
})();
