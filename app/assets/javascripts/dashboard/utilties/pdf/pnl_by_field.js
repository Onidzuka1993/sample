
//   EXPOSED AS   App.pdfGenerator.printProfitAndLoss(resultsView, year, id)
//   EXPOSED AS   App.pdfGenerator.downloadProfitAndLoss(resultsView, year, name)

//= require pdf-builder/pdf-builder
//= require pdf-builder/pdf-table

(function() {
  "use strict";

  var pdfBuilder;

  function profitLossByFieldTable(resultRows, totalRevenue, totalExpenses, totalIncome) {
    var rows = {
      fieldName:  [],
      cropName:   [],
      acres:      [],
      yield:      [],
      revenue:    [],
      expenses:   [],
      breakEven:  [],
      income:     []
    };
    var row;

    for (var i = 0; i < resultRows.length; i++) {
      row = resultRows[i];
      rows.fieldName.push({ align: 'left', text: row.$els.fieldName.innerText });
      rows.cropName.push({ align: 'left', text: row.$els.cropName.innerText });
      rows.acres.push({ text: row.$els.plantedAcres.innerText });
      rows.yield.push({ text: row.$els.yield.innerText });
      rows.revenue.push({ text: row.$els.revenue.innerText });
      rows.expenses.push({ text: row.$els.expenses.innerText });
      rows.breakEven.push({ text: row.$els.breakEven.innerText });
      rows.income.push({ text: row.$els.income.innerText });
    }

    rows.fieldName.push({
      align:       'left',
      borderColor: '#fff',
      font:        'Helvetica-Bold',
      text:        'Totals'
    });

    rows.revenue.push({
      borderColor: '#fff',
      font:        'Helvetica-Bold',
      text:        totalRevenue
    });

    rows.expenses.push({
      borderColor: '#fff',
      font:        'Helvetica-Bold',
      text:        totalExpenses
    });

    rows.income.push({
      borderColor: '#fff',
      font:        'Helvetica-Bold',
      text:        totalIncome
    });

    var columns = [{
      header: {
        align: 'left',
        text:  'Field'
      },
      rows: rows.fieldName,
      width: 0.20
    }, {
      header: {
        align: 'left',
        text:  'Crop'
      },
      rows: rows.cropName,
      width: 0.15
    }, {
      header: { text: 'Acres' },
      rows: rows.acres,
      width: 0.07
    }, {
      header: { text: 'Yield' },
      rows: rows.yield,
      width: 0.07
    }, {
      header: { text: 'Revenue' },
      rows: rows.revenue,
      width: 0.13
    }, {
      header: { text: 'Expenses' },
      rows: rows.expenses,
      width: 0.13
    }, {
      header: { text: 'Break Even' },
      rows: rows.breakEven,
      width: 0.12
    }, {
      header: { text: 'Profit/Loss' },
      rows: rows.income,
      width: 0.13
    }];

    var tableOptions = {
      cellAlign:      'right',
      cellFontSize:   12,
      headerAlign:    'right',
      headerFontSize: 12
    };

    return new PDFTable(columns, tableOptions);
  }

  function generate(resultsView, year) {
    pdfBuilder = App.pdfGenerator.landscapePageLayout(year, 'P&L by Field', {data: [{label: "Entity", value: resultsView.entity}, {label: "Owner", value: resultsView.selectedOwner}]});
    var resultRows = resultsView.$refs.profitLossByFieldTable.$refs.resultRow;
    var totalRevenue = resultsView.$refs.profitLossByFieldTable.$els.totalRevenue.innerText;
    var totalExpenses = resultsView.$refs.profitLossByFieldTable.$els.totalExpenses.innerText;
    var totalIncome = resultsView.$refs.profitLossByFieldTable.$els.totalIncome.innerText;

    pdfBuilder.addHeading('Profit/Loss by Field');
    pdfBuilder.addTable(profitLossByFieldTable(resultRows, totalRevenue, totalExpenses, totalIncome));
  }

  App.pdfGenerator.printPnlByField = function(resultsView, year, id) {
    generate(resultsView, year);
    App.pdfGenerator.print(pdfBuilder, id);
  };

  App.pdfGenerator.downloadPnlByField = function(resultsView, year, name) {
    generate(resultsView, year);
    App.pdfGenerator.download(pdfBuilder, name);
  };
})();
