/* globals numeral */

(function() {
  "use strict";

  App.parseNumber = function(value) {
    value = (value + '').replace(/[^0123456789.-]/g, '');
    return numeral(value).value();
  };
})();
