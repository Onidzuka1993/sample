(function() {
  "use strict";

  App.year = function(year) {
    if (year === undefined || year === null) {
      var matches = window.location.pathname.match(/\/(\d{4})/g);
      if (matches === null) {
        year = (new Date()).getFullYear().toString();
      }else{
        year = matches[0].replace("/", "");
      }
    }
    return year;
  };
})();
