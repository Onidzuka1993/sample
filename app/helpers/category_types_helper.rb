module CategoryTypesHelper
  def category_types
    [
      'Income',
      'Cost of Goods Sold',
      'Expenses',
      'Accounts receivable',
      'Current Assets',
      'Bank',
      'Fixed Assets',
      'Other Assets',
      'Account payable(A/P)',
      'Credit Card',
      'Current Liabilities',
      'Long Term Liabilities',
      'Equity'
    ]
  end
end