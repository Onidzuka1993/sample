module ReportsHelper
  def select_options
    [
        ['Year to date', 'year_to_date'],
        ['This month', 'this_month '],
        ['Last month', 'last_month'],
        ['Last 3 months', 'last_three_months'],
        ['Last year', 'last_year'],
        'Custom date'
    ]
  end
end