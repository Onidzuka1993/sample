module ApplicationHelper
	include ActionView::Helpers::NumberHelper

	def formatted_amount(amount)
		number_to_currency(amount)
	end
end
