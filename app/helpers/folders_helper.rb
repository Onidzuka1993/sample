module FoldersHelper
	def get_folder_path(folder)
		case folder.folder_type
			when 'expense'
				edit_expense_path(folder)
			when 'check'
				edit_check_path(folder)
			when 'transfer'
				edit_transfer_path(folder)
			when 'journal_entry'
				edit_journal_entry_path(folder)
			else
				raise 'Undefined folder type'
		end
	end
end