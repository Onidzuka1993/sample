module AccountsHelper
  def sort_by(column, direction=nil)
    if direction
      accounts_path(column: column, direction: direction)
    else
      accounts_path(column: column, direction: params[:direction] == 'asc' ? 'desc' : 'asc')
    end
  end
end