class HeaderAccount < ApplicationRecord
  attr_accessor :filter_by

  has_many :category_types

  validates :name, :description, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  def total_balance_amount
    if category_types.exists?
      category_types.inject(0) {|memo, sub_account| memo += sub_account.total_balance_amount }
    else
      0.0
    end
  end
end
