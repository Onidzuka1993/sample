class DynamicMenu < ApplicationRecord
  has_many :dynamic_menus_detail_types, dependent: :destroy
  has_many :detail_types, through: :dynamic_menus_detail_types

  validates :name, :description, :from_list, :to_list, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :detail_type_ids, presence: true
end