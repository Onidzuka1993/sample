class DynamicMenusDetailType < ApplicationRecord
  belongs_to(:detail_type)
  belongs_to(:dynamic_menu)

  validates :detail_type, :dynamic_menu, presence: true
end
