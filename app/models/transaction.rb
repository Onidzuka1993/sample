class Transaction < ApplicationRecord
  STATUSES          = %w(created completed)
  TRANSACTION_TYPES = %w(credit debit transfer)

  before_destroy { |record| reverse_transaction(record) }

	belongs_to :journal_transaction, class_name: Folder, foreign_key: :folder_id
  belongs_to :source_account,      class_name: Account, optional: true
  belongs_to :target_account,      class_name: Account, optional: true

  validates :status, :transaction_type, :amount, presence: true

  validates :transaction_type, inclusion: {in: TRANSACTION_TYPES}
  validates :status,           inclusion: {in: STATUSES}

  validates :source_account_id, numericality: true, presence: true, if: :debit?
  validates :target_account_id, numericality: true, presence: true, if: :credit?
  validates :source_account_id, :target_account_id, presence: true, numericality: true, if: :transfer?

  validates :amount, numericality: true

  def debit?
    transaction_type == 'debit'
  end

  def credit?
    transaction_type == 'credit'
  end

  def transfer?
    transaction_type == 'transfer'
  end

	def account_id
    return source_account_id if debit?
    return target_account_id if credit?
    raise 'transfer transaction has two account ids'
  end

  def reverse_transaction(record)
    account = Account.find(record.account_id)
    new_balance = record.debit? ? account.total_balance_amount + record.amount : account.total_balance_amount - record.amount
    account.update_attribute(:total_balance_amount, new_balance)
  end
end