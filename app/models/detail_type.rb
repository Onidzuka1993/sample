class DetailType < ApplicationRecord
  belongs_to :category_type
  has_many   :accounts

  has_many   :dynamic_menus_detail_types
  has_many   :dynamic_menus, through: :dynamic_menus_detail_types

  validates :name, :description, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  delegate :header_account_id, to: :category_type, allow_nil: true

  def total_balance_amount
    if accounts.exists?
      accounts.inject(0) {|memo, account| memo += account.total_balance_amount }
    else
      0.0
    end
  end
end
