class CategoryType < ApplicationRecord
  belongs_to :header_account
  has_many   :detail_types

  validates :name, :description, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  def total_balance_amount
    if detail_types.exists?
      detail_types.inject(0) {|memo, detail_type| memo += detail_type.total_balance_amount }
    else
      0.0
    end
  end
end
