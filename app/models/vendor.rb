class Vendor < ApplicationRecord
	validates :name, :phone_number, :email, :address, presence: true
	validates :email, uniqueness: {case_sensitive: false}, email: true

	has_many :folders
end
