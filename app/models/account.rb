class Account < ApplicationRecord
  acts_as_paranoid

  belongs_to :detail_type

  has_many :source_transactions, class_name: Transaction, foreign_key: :source_account_id
  has_many :target_transactions, class_name: Transaction, foreign_key: :target_account_id

  validates :name, :description, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :total_balance_amount, presence: true
  validates :total_balance_amount, numericality: true

  delegate :header_account_id,
           :category_type_id,
           to: :detail_type,
           allow_nil: true
end
