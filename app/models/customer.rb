class Customer < ApplicationRecord
	validates :name, :phone_number, :email, :address, presence: true
	validates :email, uniqueness: {case_sensitive: false}, email: true
end
