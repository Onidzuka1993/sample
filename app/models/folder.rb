class Folder < ApplicationRecord
	STATUSES	  = %w(created failed)
	FOLDER_TYPE = %w(deposit transfer expense journal_entry check)

	has_many :journal_transactions, class_name: Transaction

	belongs_to :vendor, optional: true

	accepts_nested_attributes_for :journal_transactions, allow_destroy: true

	validates :happened_at, :status, presence: true
	validates :status, 		  inclusion: {in: STATUSES}
	validates :folder_type, inclusion: {in: FOLDER_TYPE}

	def amount
		public_send("#{folder_type}_amount".to_sym)
	end

	def transfer_amount
		journal_transactions.first.amount.to_f
	end

	def expense_amount
		journal_transactions.where(transaction_type: 'credit').inject(0.0) {|memo, transaction| memo += transaction.amount }.to_f
	end

	def check_amount
		journal_transactions.where(transaction_type: 'credit').inject(0.0) {|memo, transaction| memo += transaction.amount }.to_f
	end

	def journal_entry_amount
		debits_amount  = journal_transactions.where(transaction_type: 'debit').inject(0) {|memo, transaction| memo += transaction.amount }.to_f
		credits_amount = journal_transactions.where(transaction_type: 'credit').inject(0) {|memo, transaction| memo += transaction.amount }.to_f
		debits_amount - credits_amount
	end
end