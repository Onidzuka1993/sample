class CreateAccount
  include Interactor

  def call
    context.account = Account.new(context.account_params)

    if context.account.save

      context.message = 'Account has successfully been saved.'
    else
      context.fail!
    end
  end

	private

	def double_entry_params
    {amount: context.account_params[:amount], source_account_id: context.account.id, happened_at: Time.now}
  end

	def has_amount?
    context.account_params[:amount].present? && context.account_params[:amount].to_f > 0
  end
end