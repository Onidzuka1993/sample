class GetFolders
  include Interactor

  # TODO retrieve only folders with status 'created'

  def call
    public_send(context.filter_by.to_sym)

    context.fail!(message: "Transactions for #{context.filter_by.humanize.downcase} not found.") if context.folders.empty?
  rescue NoMethodError
    past_thirty_days
  end

  def past_thirty_days
    context.folders = Folder.where('created_at >= :start_date AND created_at <= :end_date',
                                   {start_date: 30.days.ago.end_of_day, end_date: Date.current.end_of_day}).order(created_at: :desc)
    context.title = 'Transactions for past 30 days'
  end

  def past_sixty_days
    context.folders = Folder.where('created_at >= :start_date AND created_at <= :end_date',
                                   {start_date: 60.days.ago.end_of_day, end_date: Date.current.end_of_day}).order(created_at: :desc)
    context.title = 'Transactions for past 60 days'
  end

  def past_ninety_days
    context.folders = Folder.where('created_at >= :start_date AND created_at <= :end_date',
                                   {start_date: 90.days.ago.end_of_day, end_date: Date.current.end_of_day}).order(created_at: :desc)
    context.title = 'Transactions for past 90 days'
  end

  def all
    context.folders = Folder.all
    context.title = 'All transactions'
  end
end
