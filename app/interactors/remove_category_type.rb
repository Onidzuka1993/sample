class RemoveCategoryType
  include Interactor

  def call
    context.category_type = CategoryType.find(context.category_type_id)
    if context.category_type.detail_types.exists?
      context.category_types = CategoryType.all

      context.fail!(message: 'This category type has detail types.')
    else
      context.category_type.destroy
    end
  end
end
