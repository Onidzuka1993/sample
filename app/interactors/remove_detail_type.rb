class RemoveDetailType
  include Interactor

  def call
    context.detail_type = DetailType.find(context.detail_type_id)

    if context.detail_type.accounts.exists?
      context.detail_types = DetailType.all

      context.fail!(message: 'This detail type has accounts.')
    else
      context.detail_type.destroy
    end
  end
end
