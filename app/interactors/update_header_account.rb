class UpdateHeaderAccount
  include Interactor

  def call
    context.header_account = HeaderAccount.find(context.header_account_id)

    context.fail! unless context.header_account.update(context.update_params)
  end
end
