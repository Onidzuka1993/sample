class CreateDoubleEntry
	include Interactor
	include HasFolder
	include JournalEntry::JournalEntryType

	def call
		context.folder = create_folder(folder_attributes)
		begin
			ActiveRecord::Base.transaction do
				process_transactions(context.journal_entry_params[:debit_transactions], 'debit')
				process_transactions(context.journal_entry_params[:credit_transactions], 'credit')
			end
		rescue => e
			process_exception(e)
		end
	end
end