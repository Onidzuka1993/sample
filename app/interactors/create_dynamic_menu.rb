class CreateDynamicMenu
  include Interactor
  include HasDynamicMenu

  def call
    context.dynamic_menu = DynamicMenu.new(context.dynamic_menu_params)

    find_detail_types
    assign_attributes
    check_detail_types

    if context.dynamic_menu.save
      context.message = 'Dynamic menu has successfully been saved'
    else
      context.fail!
    end
  end

  private

  def assign_attributes
    context.dynamic_menu.detail_type_ids = detail_type_ids
    context.dynamic_menu.from_list       = context.from_list.map {|detail_type| detail_type.id }
    context.dynamic_menu.to_list         = context.to_list.map {|detail_type| detail_type.id }
  end
end
