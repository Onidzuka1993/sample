class TransferFunds
  include Interactor
  include Transfer::TransferType
  include HasFolder

  def call
    begin
      process_transfer
    rescue
      context.fail!
    ensure
      remove_empty_folder
    end
  end

  private

  def process_transfer
    check_date
    init_transaction
    create_transfer_folder
    assign_transfer_attributes
    find_accounts
    check_transfer

    if context.transaction.valid?
      ActiveRecord::Base.transaction do
        lock_accounts
        transfer(context.source_account, context.target_account, transfer_amount)
        save_transaction
      end
    else
      context.errors = context.transaction.errors.full_messages

      context.fail!
    end
  end

  def init_transaction
    context.transaction = Transaction.new(context.transfer_params.except(:happened_at, :memo))
  end

  def assign_transfer_attributes
    context.transaction.tap do |transaction|
      transaction.status           = 'completed'
      transaction.transaction_type = 'transfer'
      transaction.folder_id        = context.folder.id
    end
  end

  def lock_accounts
    context.source_account.lock!
    context.target_account.lock!
  end

  def save_transaction
    context.transaction.save
  end

  def transfer_amount
    context.transfer_params[:amount].to_f
  end

  def errors_messages
    context.transaction.errors.full_messages
  end
end
