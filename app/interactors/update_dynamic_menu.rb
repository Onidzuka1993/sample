class UpdateDynamicMenu
  include Interactor
  include HasDynamicMenu

  def call
    context.dynamic_menu = DynamicMenu.find(context.dynamic_menu_id)

    find_detail_types
    assign_attributes
    check_detail_types

    if context.dynamic_menu.update(context.dynamic_menu_params)

      context.message = 'Your dynamic menu has successfully been updated'
    else
      context.fail!
    end
  end

  private

  def assign_attributes
    context.dynamic_menu_params[:detail_type_ids] = detail_type_ids
    context.dynamic_menu_params[:from_list]    = context.from_list.map {|detail_type| detail_type.id }
    context.dynamic_menu_params[:to_list]      = context.to_list.map {|detail_type| detail_type.id }
  end
end
