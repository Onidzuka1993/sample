class CreateCheck < CreateTransactions
  include HasFolder
  include Expense::CheckType

  def call
    process_inputs
    begin
      process_print_check
    rescue => e
      process_exception(e)
    end
  end

  private

  def process_inputs
    check_inputs
    parse_entries(context.check_params[:print_check_table])
    remove_blank_entries
    check_entries
    create_folder(folder_attributes)
  end

  def process_print_check
    ActiveRecord::Base.transaction do
      get_debit_account(context.check_params[:account_id])

      context.entries.each do |entry|
        get_credit_account(entry[:Account])

        debit_account(entry)
        credit_account(entry)

        create_print_check_data
      end
    end
  end

  def create_print_check_data
    vendor = Vendor.find_by_id(context.check_params[:vendor_id])
    context.print_check_data = {
        vendor_name:        vendor.try(:name) || '',
        address:            context.folder.address,
        memo:               context.folder.memo,
        total_check_amount: get_total_check_amount
    }
  end
end
