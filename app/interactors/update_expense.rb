class UpdateExpense < CreateTransactions
  include HasFolder
  include Expense::ExpenseType
  include Expense::UpdateExpense

  def call
    process_inputs
    begin
      update_expense
    rescue => e
      process_exception(e)
    end
  end

  private

  def process_inputs
    check_inputs
    parse_entries(context.expense_params[:expense_entries])
    remove_blank_entries
    check_entries
    find_folder(context.folder_id)
  end

  def update_expense
    ActiveRecord::Base.transaction do
      get_debit_account(context.expense_params[:account_id])
      get_prev_transactions_data
      context.folder.update(update_params)
      update_accounts
    end
  end
end
