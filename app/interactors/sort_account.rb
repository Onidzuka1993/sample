class SortAccount
  include Interactor

  SORT_BY_ASC_AND_DESC = %w{id name opening_balance}

  def call
    if SORT_BY_ASC_AND_DESC.include?(context.column)
      order_by_column
    else
      public_send "order_by_#{context.column}"
    end
  rescue NoMethodError
    default
  end

  def order_by_column
    context.accounts = Account.order(context.column + ' ' + context.direction)
  end

  def order_by_sub_account
    context.accounts = Account.includes(category: :sub_account).order('sub_accounts.name')
  end

  def order_by_category
    context.accounts = Account.includes(:category).order('categories.name')
  end

  def order_by_total_balance
    context.accounts = Account.includes(:balances).sort_by(&:total_balance_amount)
  end

  def default
    context.accounts = Account.includes(detail_type: :category_type)
  end
end