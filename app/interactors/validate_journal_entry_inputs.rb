class ValidateJournalEntryInputs
	include Interactor

	def call
		check_journal_entry
		prepare_data_for_double_entry
	end

	private

	def check_journal_entry
		parse_transactions
		check_transactions
	end

	def parse_transactions
		context.transactions = JSON.parse(context.journal_entry_params[:journal_entry_table]).map(&:deep_symbolize_keys)
	end

	def check_transactions
		check_date
		remove_empty_transactions
		check_debits_and_credits
	end

	def check_date
		context.fail!(message: {error: 'You must specify a date.'})  		 if happened_at.blank?
		context.fail!(message: {error: 'You must specify a valid date'}) unless valid_date?
	end

	def remove_empty_transactions
		context.transactions.delete_if do |transaction|
			transaction[:Debits].blank? && transaction[:Credits].blank? && transaction[:Account].blank?
		end

		context.fail!(message: {error: 'You must fill out at least two detail lines.'}) if context.transactions.count < 2
	end

	def check_debits_and_credits
		context.fail!(message: {error: 'You must select an account for each split line with an amount.'})  if has_no_account?
		context.fail!(message: {error: 'You must specify either credits or debits for each detail line.'}) if has_invalid_row?
		context.fail!(message: {error: 'Please balance debits and credits.'})															 if disbalanced?
	end

	def prepare_data_for_double_entry
		context.journal_entry_params = {
				happened_at: happened_at,
				memo: context.journal_entry_params[:memo],
				debit_transactions: debit_transactions,
				credit_transactions: credit_transactions
		}
	end

	def debit_transactions
		context.transactions.select {|transaction| transaction[:Debits].present?}
	end

	def credit_transactions
		context.transactions.select {|transaction| transaction[:Credits].present?}
	end

	def has_no_account?
		context.transactions.detect { |transaction| transaction[:Account].blank?}
	end

	def has_invalid_row?
		context.transactions.each do |transaction|
			return true if transaction[:Debits].present? && transaction[:Credits].present?
			return true if transaction[:Debits].blank?   && transaction[:Credits].blank?
		end
		false
	end

	def disbalanced?
		sum_of_debits != sum_of_credits
	end

	def sum_of_credits
		context.transactions.inject(0) { |memo, transaction| memo += transaction[:Credits].to_f }
	end

	def sum_of_debits
		context.transactions.inject(0) { |memo, transaction| memo += transaction[:Debits].to_f }
	end

	def valid_date?
		Date.parse(happened_at) rescue false
	end

	def happened_at
		context.journal_entry_params[:happened_at]
	end
end