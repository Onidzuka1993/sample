class UpdateAccount
  include Interactor

  def call
    context.account = Account.find(context.account_id)

    if context.account.update(context.account_params)

      context.message = 'Account has successfully been updated.'
    else
      context.fail!
    end
  end
end
