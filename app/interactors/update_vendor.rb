class UpdateVendor
  include Interactor

  def call
    context.vendor = Vendor.find(context.vendor_id)

    context.fail! unless context.vendor.update(context.vendor_params)
  end
end
