class UpdateJournalEntry
  include Interactor
  include HasFolder
  include JournalEntry::JournalEntryType

  def call
    context.folder = find_folder(context.folder_id)

    begin
      ActiveRecord::Base.transaction do
        update_folder
        # TODO temp solution
        revert_current_balances
        destroy_current_transactions

        process_transactions(context.journal_entry_params[:debit_transactions], 'debit')
        process_transactions(context.journal_entry_params[:credit_transactions], 'credit')
      end
    rescue => e
      process_exception(e)
    end
  end

  private

  def update_folder
    context.folder.update(context.journal_entry_params.except(:debit_transactions, :credit_transactions))
  end

  def revert_current_balances
    context.folder.journal_transactions.each do |transaction|
      if transaction.debit?
        revert_balance(transaction.source_account, transaction.amount)
      else
        revert_balance(transaction.target_account, transaction.amount)
      end
    end
  end

  def destroy_current_transactions
    context.folder.journal_transactions.each do |transaction|
      transaction.delete
    end
  end

  def revert_balance(account, amount)
    new_balance = account.total_balance_amount - amount.to_f
    account.update_attribute(:total_balance_amount, new_balance)
  end
end
