class DebitAccount
  include Interactor::Organizer

  organize CreateDebitTransaction, ExecuteDebitTransaction
end
