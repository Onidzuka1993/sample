class CreateDebitTransaction
  include Interactor
  include HasFolder

  def call
    create_folder({folder_type: 'deposit'})
    context.transaction = Transaction.new(transaction_params)

    context.fail! unless context.transaction.save
  end

  private

  def transaction_params
    context.transaction_params.merge(status: 'created', transaction_type: 'debit', folder_id: context.folder.id)
  end
end
