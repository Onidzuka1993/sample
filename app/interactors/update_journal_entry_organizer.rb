class UpdateJournalEntryOrganizer
  include Interactor::Organizer

  organize ValidateJournalEntryInputs, UpdateJournalEntry
end
