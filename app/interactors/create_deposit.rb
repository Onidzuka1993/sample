class CreateDeposit
  include Interactor
  include HasFolder

  def call
    create_folder({folder_type: 'deposit'})
    initialize_debit_transaction
    initialize_credit_transaction

    if context.debit_transaction.valid? && context.credit_transaction.valid?
      ActiveRecord::Base.transaction do
        lock_accounts
        debit_account
        credit_account
        context.debit_transaction.save
        context.credit_transaction.save
      end
    else
      context.fail!
    end
  end

  private

  def initialize_debit_transaction
    context.debit_transaction  = Transaction.new(debit_transaction_params)
  end

  def initialize_credit_transaction
    context.credit_transaction = Transaction.new(credit_transaction_params)
  end

  def lock_accounts
    context.source_account.lock!
    context.target_account.lock!
  end

  def debit_account
    new_balance = context.source_account.total_balance_amount + debit_amount
    context.source_account.update!(total_balance_amount: new_balance)
  end

  def credit_account
    new_balance = context.target_account.total_balance_amount + credit_amount
    context.target_account.update!(total_balance_amount: new_balance)
  end

  def debit_transaction_params
    context.params.merge(common_params).merge(transaction_type: 'debit', source_account_id: source_account.id)
  end

  def credit_transaction_params
    context.params.merge(common_params).merge(transaction_type: 'credit', target_account_id: target_account.id).
        except(:source_account_id)
  end

  def source_account
    context.source_account = Account.find(context.params[:source_account_id])
  end

  def target_account
    context.target_account = Account.find_by_name!('Opening Balance')
  end

  def debit_amount
    context.params[:amount].to_f
  end

  def credit_amount
    context.params[:amount].to_f
  end

  def common_params
    {status: 'completed', folder_id: folder_id}
  end

  def folder_id
    context.folder.id
  end
end
