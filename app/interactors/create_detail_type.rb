class CreateDetailType
  include Interactor

  def call
    context.detail_type = DetailType.new(context.detail_type_params)

    context.fail! unless context.detail_type.save
  end
end
