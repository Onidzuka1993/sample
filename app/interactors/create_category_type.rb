class CreateCategoryType
  include Interactor

  def call
    context.category_type = CategoryType.new(context.category_type_params)

    context.fail! unless context.category_type.save
  end
end
