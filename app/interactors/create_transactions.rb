class CreateTransactions
  include Interactor

	private

  def check_inputs
    context.fail!(message: {error: 'At the top of the page, select the bank account for this check.'}) if no_cash_account?
    context.fail!(message: {error: 'You must specify a date.'})                                        if no_payment_date?
    context.fail!(message: {error: 'You must specify a valid date'})                                   unless valid_date?
  end

  def parse_entries(entries)
    context.entries = JSON.parse(entries).map(&:deep_symbolize_keys)
  end

  def remove_blank_entries
    context.entries.delete_if { |entry| entry[:Account].blank? || entry[:Amount].blank? }
  end

  def check_entries
    context.fail!(message: {error: 'You must fill out at least one detail line.'}) if context.entries.count < 1
  end

  def get_debit_account(id)
    context.account_to_debit = Account.find(id)
  end

  def get_credit_account(id)
    context.account_to_credit = Account.find(id)
  end

  def debit_account(entry)
    context.debit_transaction = Transaction.new(init_debit_transaction_attrs(entry))
    update_debit_account_balance(entry)
    context.debit_transaction.save!
  end

  def credit_account(entry)
    context.credit_transaction = Transaction.new(init_credit_transaction_attrs(entry))
    update_credit_account_balance(entry)
    context.credit_transaction.save!
  end

  def init_debit_transaction_attrs(entry)
    {
        status:            'completed',
        source_account_id: context.account_to_debit.id,
        amount:            entry[:Amount],
        folder_id:         context.folder.id,
        description:       entry[:Description],
        transaction_type: 'debit'
    }
  end

  def init_credit_transaction_attrs(entry)
    {
        status:            'completed',
        target_account_id: context.account_to_credit.id,
        amount:            entry[:Amount],
        folder_id:         context.folder.id,
        description:       entry[:Description],
        transaction_type: 'credit'
    }
  end

  def update_debit_account_balance(entry)
    new_balance = context.account_to_debit.total_balance_amount - entry[:Amount].to_f
    context.account_to_debit.update!(total_balance_amount: new_balance)
  end

  def update_credit_account_balance(entry)
    new_balance = context.account_to_credit.total_balance_amount + entry[:Amount].to_f
    context.account_to_credit.update!(total_balance_amount: new_balance)
  end

  def no_cash_account?
    raise NotImplementedError
  end

  def no_payment_date?
    raise NotImplementedError
  end

  def valid_date?
    raise NotImplementedError
  end

	def folder_attributes
    raise NotImplementedError
  end
end