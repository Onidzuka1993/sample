class UpdateDetailType
  include Interactor

  def call
    context.detail_type = DetailType.find(context.detail_type_id)

    context.fail! unless context.detail_type.update(context.detail_type_params)
  end
end