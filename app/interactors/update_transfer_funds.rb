class UpdateTransferFunds
  include Interactor
  include Transfer::TransferType
  include HasFolder

  def call
    begin
      update_transfer
    rescue
      context.fail!
    ensure
      remove_empty_folder
    end
  end

  private

  def update_transfer
    check_date
    find_folder(context.folder_id)
    find_accounts
    check_transfer
    get_prev_data
    context.folder.assign_attributes(update_attrs)

    if context.folder.valid?
      ActiveRecord::Base.transaction do
        lock_accounts(context.source_account, context.prev_source_account)
        lock_accounts(context.target_account, context.prev_target_account)
        revert_transfer(context.prev_source_account, context.prev_target_account, context.prev_transfer_amount)
        transfer(context.source_account.reload, context.target_account.reload, new_transfer_amount)
        update_folder
      end
    else
      context.errors = context.folder.errors.full_messages
      context.fail!
    end
  end

  def lock_accounts(new_account, prev_account)
    if new_account.id == prev_account.id
      new_account.lock!
    else
      new_account.lock!
      prev_account.lock!
    end
  end

  def revert_transfer(source_account, target_account, amount)
    transfer(target_account, source_account, amount)
  end

  def update_folder
    context.folder.update!(update_attrs)
  end

  def update_attrs
    params = context.transfer_params.merge({id: context.folder.journal_transactions.first.id})
    params.slice(:happened_at, :memo).merge({
        journal_transactions_attributes: [params.slice(
            :id,
            :source_account_id,
            :target_account_id,
            :amount
        )]
      })
  end

  def get_prev_data
    context.prev_source_account  = context.folder.journal_transactions.first.source_account
    context.prev_target_account  = context.folder.journal_transactions.first.target_account
    context.prev_transfer_amount = context.folder.journal_transactions.first.amount
  end

  def prev_transfer_amount
    context.folder.journal_transactions.first.amount
  end

  def new_transfer_amount
    context.transfer_params[:amount].to_f
  end
end
