class CreateExpense < CreateTransactions
  include HasFolder
	include Expense::ExpenseType

  def call
    process_inputs
    begin
      process_expense
    rescue => e
      process_exception(e)
    end
  end

  private

	def process_inputs
    check_inputs
    parse_entries(context.expense_params[:expense_entries])
    remove_blank_entries
    check_entries
    create_folder(folder_attributes)
  end

	def process_expense
    ActiveRecord::Base.transaction do
      context.entries.each do |entry|
        get_debit_account(context.expense_params[:account_id])
        get_credit_account(entry[:Account])

        debit_account(entry)
        credit_account(entry)
      end
    end
  end
end