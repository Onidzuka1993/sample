class CreateCustomer
  include Interactor

  def call
    context.customer = Customer.new(context.customer_params)

    context.fail! unless context.customer.save
  end
end
