class UpdateCategoryType
  include Interactor

  def call
    context.category_type = CategoryType.find(context.category_type_id)

    context.fail! unless context.category_type.update(context.category_type_params)
  end
end
