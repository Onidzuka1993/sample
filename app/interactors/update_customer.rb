class UpdateCustomer
  include Interactor

  def call
    context.customer = Customer.find(context.customer_id)

    context.fail! unless context.customer.update(context.customer_params)
  end
end