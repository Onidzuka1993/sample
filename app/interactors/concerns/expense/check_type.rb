module Expense
	module CheckType
		def no_cash_account?
			context.check_params[:account_id].blank?
		end

		def no_payment_date?
			context.check_params[:payment_date].blank?
		end

		def valid_date?
			Date.parse(context.check_params[:payment_date]) rescue false
		end

		def get_total_check_amount
			context.entries.inject(0) { |memo, entry| memo += entry[:Amount].to_f }
		end

		def folder_attributes
			{
					memo:        context.check_params[:memo],
					address:     context.check_params[:address],
					folder_type: 'check',
					happened_at: context.check_params[:payment_date],
					vendor_id:   context.check_params[:vendor_id]
			}
		end
	end
end