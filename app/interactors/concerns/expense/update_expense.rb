module Expense
	module UpdateExpense
		def get_prev_transactions_data
			context.prev_transactions_data = context.folder.journal_transactions.map do |t|
				{
						id:         t.id,
						account_id: t.account_id,
						amount:     t.amount.to_f,
						debit?:     t.debit?
				}
			end
		end

		def build_journal_transactions
			context.transactions = Array.new
			context.entries.each do |entry|
				get_credit_account(entry[:Account])
				context.transactions.push(init_debit_transaction_attrs(entry), init_credit_transaction_attrs(entry))
			end
		end

		def form_nested_attributes
			context.transaction_ids = context.folder.journal_transactions.map { |transaction| {id: transaction.id} }
			if has_transactions_to_remove?
				merge_ids_with_transactions
				add_transactions_to_remove
			else
				merge_ids_with_transactions
			end
		end

		def update_params
			folder_attributes.merge(journal_transactions_attributes: journal_transactions)
		end

		def update_accounts
			context.folder.journal_transactions.each do |transaction|
				update_account_balance(transaction)
			end
		end

		def update_account_balance(transaction)
			prev_transaction = get_prev_transaction(transaction)

			if prev_transaction
				update_prev_transaction(prev_transaction, transaction)
			else
				account = Account.find(transaction.account_id)
				update_new_account(transaction, account)
			end
		end

		def update_prev_transaction(prev_transaction, transaction)
			new_amount, new_account   = transaction.amount, Account.find(transaction.account_id)
			prev_amount, prev_account = prev_transaction.amount, Account.find(prev_transaction.account_id)

			if account_not_changed?(new_account, prev_account)
				update_amount    = get_update_amount(new_amount, prev_amount)
				amount_decreased = amount_decreased?(new_amount, prev_amount)
				new_balance      = get_new_balance(new_account, transaction, update_amount, amount_decreased)

				new_account.update_attribute(:total_balance_amount, new_balance)
			else
				update_prev_account(prev_transaction, prev_account)
				update_new_account(transaction, new_account)
			end
		end

		def update_prev_account(prev_transaction, prev_account)
			prev_transaction.debit? ? credit_account(prev_account, prev_transaction) : debit_account(prev_account, prev_transaction)
		end

		def update_new_account(transaction, account)
			transaction.debit? ? debit_account(account, transaction) : credit_account(account, transaction)
		end

		def debit_account(account, transaction)
			new_balance = account.total_balance_amount - transaction.amount
			account.update_attribute(:total_balance_amount, new_balance)
		end

		def credit_account(new_account, transaction)
			new_balance = new_account.total_balance_amount + transaction.amount
			new_account.update_attribute(:total_balance_amount, new_balance)
		end

		def get_update_amount(new_amount, prev_amount)
			amount_decreased?(new_amount, prev_amount) ? prev_amount - new_amount : new_amount - prev_amount
		end

		def get_new_balance(account, transaction, update_amount, amount_decreased)
			if transaction.debit? && amount_decreased || transaction.credit? && !amount_decreased
				account.total_balance_amount + update_amount
			else
				account.total_balance_amount - update_amount
			end
		end

		def amount_decreased?(new_amount, prev_amount)
			new_amount < prev_amount
		end

		def account_not_changed?(new_account, prev_account)
			new_account.id == prev_account.id
		end

		def get_prev_transaction(transaction)
			prev_transaction_attrs = context.prev_transactions_data.detect {|prev_transaction| prev_transaction[:id] == transaction.id}
			prev_transaction_attrs ? OpenStruct.new(prev_transaction_attrs) : false
		end

		def journal_transactions
			build_journal_transactions
			form_nested_attributes
		end

		def has_transactions_to_remove?
			context.transaction_ids.count > context.transactions.count
		end

		def merge_ids_with_transactions
			context.transactions = context.transactions.zip(context.transaction_ids).map { |first, second| first.merge(second) rescue first }
		end

		def add_transactions_to_remove
			transaction_ids_to_remove = context.transaction_ids.map {|t| t[:id]} - context.transactions.map {|t| t[:id]}
			transaction_ids_to_remove.each {|id| context.transactions.push({id: id, _destroy:  '1'})}
			context.transactions
		end
	end
end
