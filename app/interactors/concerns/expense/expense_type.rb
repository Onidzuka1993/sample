module Expense
	module ExpenseType
		def no_cash_account?
			context.expense_params[:account_id].blank?
		end

		def no_payment_date?
			context.expense_params[:payment_date].blank?
		end

		def valid_date?
			Date.parse(context.expense_params[:payment_date]) rescue false
		end

		def folder_attributes
			{
					folder_type:    'expense',
					memo:           context.expense_params[:memo],
					happened_at:    context.expense_params[:payment_date],
					payment_method: context.expense_params[:payment_method],
					vendor_id:      context.expense_params[:vendor_id]
			}
		end
	end
end