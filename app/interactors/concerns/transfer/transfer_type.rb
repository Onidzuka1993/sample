module Transfer
	module TransferType
		def check_date
			if context.transfer_params[:happened_at].present? && context.transfer_params[:happened_at].to_date > Date.current
				context.fail!(errors: ['Happened at cannot be after today'])
			end
		end

		def create_transfer_folder
			folder_params = {folder_type: 'transfer', happened_at: context.transfer_params[:happened_at], memo: context.transfer_params[:memo]}
			create_folder(folder_params)

			unless context.folder.valid?
				context.errors = context.folder.errors.full_messages

				context.fail!
			end
		end

		def find_accounts
			context.source_account = Account.find_by_id(source_account_id)
			context.target_account = Account.find_by_id(target_account_id)

			context.fail!(errors: ['You must specify transfer from account and transfer to account']) if blank_accounts?
		end

		def check_transfer
			context.fail!(errors: ["You can't transfer to the same account"])                        if self_selection_transfer?
			context.fail!(errors: ["You can't transfer between accounts from different categories"]) if under_different_category?
		end

		def transfer(source_account, target_account, amount)
			debit_account(source_account, amount)
			credit_account(target_account, amount)
		end

		def debit_account(source_account, amount)
			new_balance = source_account.total_balance_amount - amount
			source_account.update!(total_balance_amount: new_balance)
		end

		def credit_account(target_account, amount)
			new_balance = target_account.total_balance_amount + amount
			target_account.update!(total_balance_amount: new_balance)
		end

		def self_selection_transfer?
			context.source_account.id == context.target_account.id
		end

		def under_different_category?
			context.source_account.header_account_id != context.target_account.header_account_id
		end

		def blank_accounts?
			context.source_account.nil? || context.target_account.nil?
		end

		def source_account_id
			context.transfer_params[:source_account_id]
		end

		def target_account_id
			context.transfer_params[:target_account_id]
		end
	end
end