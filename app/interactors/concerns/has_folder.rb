module HasFolder
	#TODO remove happened_at
	def create_folder(options={})
		context.folder = Folder.create({status: 'created', happened_at: Date.current, memo: ''}.merge(options))
	end

	def find_folder(id)
		context.folder = Folder.find(id)
	end

	def remove_empty_folder
		context.folder.destroy! if context.folder && context.folder.journal_transactions.blank?
	end

	def process_exception(e)
		context.folder.update(status: 'failed', reason: e)
		context.fail!(message: {error: 'fail'})
	end
end