module JournalEntry
	module JournalEntryType
		def process_transactions(transactions, type)
			transactions.each do |transaction|
				account 							 	= Account.find(transaction[:Account])
				initialized_transaction = initialize_transaction(account, transaction, type)
				update_balance(account, initialized_transaction)

				initialized_transaction.save!
			end
		end

		def initialize_transaction(account, transaction, type)
			transaction = Transaction.new(
					status: 				  'completed',
					amount: 				   transaction["#{type.capitalize}s".to_sym].to_f.abs,
					folder_id: 				 context.folder.id,
					description: 			 transaction[:Description],
					transaction_type:  type
			)
			transaction.source_account_id = account.id if type == 'debit'
			transaction.target_account_id = account.id if type == 'credit'
			transaction
		end

		def update_balance(account, transaction)
			new_balance = account.total_balance_amount + transaction.amount.to_f
			account.update!(total_balance_amount: new_balance)
		end

		def folder_attributes
			{happened_at: context.journal_entry_params[:happened_at], memo: context.journal_entry_params[:memo], folder_type: 'journal_entry'}
		end
	end
end