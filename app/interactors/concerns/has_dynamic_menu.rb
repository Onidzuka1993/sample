module HasDynamicMenu
  def find_detail_types
    context.from_list = DetailType.find(context.dynamic_menu_params[:from_list].without(''))
    context.to_list   = DetailType.find(context.dynamic_menu_params[:to_list].without(''))
  end

  def check_detail_types
    if has_common_detail_types?
      context.dynamic_menu.errors.add(:base, :cannot_add_on_both_ends, message: "You can't add the same account list for on both ends.")
      context.fail!
    end
  end

  def has_common_detail_types?
    (context.from_list & context.to_list).any?
  end

  def detail_type_ids
    (context.from_list + context.to_list).map {|detail_type| detail_type.id }
  end
end
