class GetBalanceSheet
  include Interactor

  BALANCE_SHEET = %w(Assets Liabilities Equity)

  def call
    context.balance_sheet = eager_loaded_header_accounts

    context.fail!(message: 'Accounts not found') if context.balance_sheet.empty?
  end

  private

  def eager_loaded_header_accounts
    HeaderAccount.includes(category_types: [{ detail_types: :accounts }]).
        where(name: BALANCE_SHEET).
        where('accounts.created_at <= ?', Time.parse(context.date).end_of_day).
        references(:accounts)
  end
end