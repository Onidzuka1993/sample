class JournalEntryOrganizer
	include Interactor::Organizer

	organize ValidateJournalEntryInputs, CreateDoubleEntry
end