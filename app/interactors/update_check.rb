class UpdateCheck < CreateTransactions
  include HasFolder
  include Expense::CheckType
  include Expense::UpdateExpense

  def call
    process_inputs
    begin
      update_check
    rescue => e
      process_exception(e)
    end
  end

  private

  def process_inputs
    check_inputs
    parse_entries(context.check_params[:print_check_table])
    remove_blank_entries
    check_entries
    find_folder(context.folder_id)
  end

  def update_check
    ActiveRecord::Base.transaction do
      get_debit_account(context.check_params[:account_id])
      get_prev_transactions_data
      context.folder.update(update_params)
      update_accounts
    end
  end
end
