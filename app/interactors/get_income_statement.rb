class GetIncomeStatement
  include Interactor

  PROFIT_AND_LOSS = %w(Income Expense)

  def call
    public_send(context.filter_by.to_sym)

    context.fail!(message: "Accounts for #{context.filter_by.humanize.downcase} not found") if accounts_not_found?
  rescue NoMethodError
    year_to_date
  ensure
    context.net_income = calculate_net_income
  end

  def year_to_date
    context.income_statement = eager_loaded_income_statement(beginning_of_current_year, current_date).where(name: PROFIT_AND_LOSS)
    context.date             = "#{formatted_to_date(beginning_of_current_year)} - #{formatted_to_date(current_date)}"

    context.fail!(message: 'Accounts for year to date not found') if accounts_not_found?
  end

  def this_month
    context.income_statement = eager_loaded_income_statement(beginning_of_current_month, current_date).where(name: PROFIT_AND_LOSS)
    context.date             = "#{formatted_to_date(beginning_of_current_month)} - #{formatted_to_date(current_date)}"
  end
  
  def last_month
    context.income_statement = eager_loaded_income_statement(beginning_of_last_month, end_of_last_month).where(name: PROFIT_AND_LOSS)
    context.date             = "#{formatted_to_date(beginning_of_last_month)} - #{formatted_to_date(end_of_last_month)}"
  end

  def last_three_months
    context.income_statement = eager_loaded_income_statement(last_three_month, current_date).where(name: PROFIT_AND_LOSS)
    context.date             = "#{formatted_to_date(last_three_month)} - #{formatted_to_date(current_date)}"
  end

  def last_year
    context.income_statement = eager_loaded_income_statement(beginning_of_last_year, end_of_last_year).where(name: PROFIT_AND_LOSS)
    context.date             = "#{formatted_to_date(beginning_of_last_year)} - #{formatted_to_date(end_of_last_year)}"
  end

  def custom_date
    if valid_dates?
      context.income_statement = eager_loaded_income_statement(custom_start_date, custom_end_date).where(name: PROFIT_AND_LOSS)
      context.date             = "#{formatted_to_date(custom_start_date)} - #{formatted_to_date(custom_end_date)}"
    end
  end

  private

  def eager_loaded_income_statement(start_date, end_date)
    HeaderAccount.includes(category_types: [{ detail_types: :accounts }]).
        where('accounts.created_at >= :start_date AND accounts.created_at <= :end_date',
              start_date: start_date,
              end_date: end_date).
        references(:accounts)
  end

  def calculate_net_income
    income  = context.income_statement.detect { |income_statement| income_statement.name == 'Income' }  rescue nil
    expense = context.income_statement.detect { |income_statement| income_statement.name == 'Expense' } rescue nil

    return income.total_balance_amount - expense.total_balance_amount if income && expense
    return income.total_balance_amount             if income
    return "-#{expense.total_balance_amount}".to_f if expense
    0.0
  end

  def beginning_of_current_month
    current_date.beginning_of_month
  end

  def beginning_of_current_year
    current_date.beginning_of_year
  end

  def beginning_of_last_month
    current_date.prev_month.beginning_of_month
  end

  def end_of_last_month
    current_date.prev_month.end_of_month
  end

  def last_three_month
    beginning_of_current_month - 3.month
  end

  def beginning_of_last_year
    (current_date - 1.year).beginning_of_year
  end

  def end_of_last_year
    (current_date - 1.year).end_of_year
  end

  def formatted_to_date(datetime)
    datetime.to_date
  end

  def current_date
    Date.current.end_of_day
  end

  def accounts_not_found?
    context.income_statement.empty?
  end

  def custom_start_date
    formatted_to_date(context.start_date).beginning_of_day
  end

  def custom_end_date
    formatted_to_date(context.end_date).end_of_day
  end

  def valid_dates?
    if start_date_valid? && end_date_valid? && custom_start_date <= custom_end_date
      true
    else
      context.fail!(message: 'Invalid dates')
    end
  end

  def end_date_valid?
    Date.parse(context.end_date.to_s).present? rescue false
  end

  def start_date_valid?
    Date.parse(context.start_date.to_s).present? rescue false
  end
end