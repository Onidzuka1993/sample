class ExecuteDebitTransaction
  include Interactor

  def call
    ActiveRecord::Base.transaction do
      credit_account
      update_transaction_status
    end
  end

  private

  def credit_account
    new_balance = source_account.total_balance_amount + credit_amount
    source_account.update!(total_balance_amount: new_balance)
  end

  def update_transaction_status
    context.transaction.update_attribute(:status, 'completed')
  end

  def source_account
    context.transaction.source_account
  end

  def credit_amount
    context.transaction.amount.to_f
  end
end
