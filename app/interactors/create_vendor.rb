class CreateVendor
	include Interactor

	def call
		context.vendor = Vendor.new(context.vendor_params)

		context.fail! unless context.vendor.save
	end
end