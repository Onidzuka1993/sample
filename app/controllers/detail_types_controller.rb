class DetailTypesController < DashboardsController
  def new
    @detail_type = DetailType.new

    render :new, locals: {
        detail_type:     @detail_type,
        header_accounts: HeaderAccount.order(:name),
        category_types:  []
    }
  end

  def create
    use_case = CreateDetailType.call(detail_type_params: detail_type_params)
    if use_case.success?

      redirect_to use_case.detail_type, notice: 'Detail type has successfully been saved.'
    else
      render :new, locals: {
          detail_type:     use_case.detail_type,
          header_accounts: HeaderAccount.order(:name),
          category_types:  CategoryType.order(:name)
      }
    end
  end

  def show
    @detail_type = DetailType.find(params[:id])

    render :show, locals: {detail_type: @detail_type}
  end

  def edit
    @detail_type = DetailType.find(params[:id])

    render :edit, locals: {detail_type: @detail_type}
  end

  def update
    use_case = UpdateDetailType.call(detail_type_id: params[:id], detail_type_params: detail_type_params)
    if use_case.success?

      redirect_to use_case.detail_type, notice: 'DetailType has successfully been updated.'
    else
      render :edit, locals: {detail_type: use_case.detail_type}
    end
  end

  def index
    @detail_types = DetailType.includes(:category_type)

    render :index, locals: {detail_types: @detail_types}
  end

  def destroy
    use_case = RemoveDetailType.call(detail_type_id: params[:id])
    if use_case.success?

      redirect_to detail_types_url, alert: 'Detail type deleted.'
    else
      flash.now[:alert] = use_case.message

      render :index, locals: {detail_types: use_case.detail_types}
    end
  end

  private

  def detail_type_params
    params.require(:detail_type).permit(:category_type_id, :name, :description)
  end
end