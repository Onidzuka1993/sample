class DynamicMenusController < DashboardsController
  def new
    @dynamic_menu = DynamicMenu.new

    render :new, locals: {
        dynamic_menu: @dynamic_menu,
        lists: DetailType.order(:name),
        from_list_ids: [],
        to_list_ids: []
    }
  end

  def create
    use_case = CreateDynamicMenu.call(dynamic_menu_params: dynamic_menu_params)
    if use_case.success?

      redirect_to use_case.dynamic_menu, notice: use_case.message
    else
      render :new, locals: {
          dynamic_menu: use_case.dynamic_menu,
          lists: DetailType.order(:name),
          from_list_ids: use_case.from_list.map(&:id),
          to_list_ids: use_case.to_list.map(&:id)
      }
    end
  end

  def show
    @dynamic_menu = DynamicMenu.find(params[:id])

    from_list = @dynamic_menu.detail_types.find(@dynamic_menu.from_list)
    to_list   = @dynamic_menu.detail_types.find(@dynamic_menu.to_list)

    render :show, locals: {
        dynamic_menu: @dynamic_menu,
        from_list: from_list.map(&:name),
        to_list: to_list.map(&:name)
    }
  end

  def edit
    @dynamic_menu = DynamicMenu.find(params[:id])
    from_list_ids = @dynamic_menu.from_list
    to_list_ids = @dynamic_menu.to_list

    render :edit, locals: {
        dynamic_menu: @dynamic_menu,
        lists: DetailType.order(:name),
        from_list_ids: from_list_ids,
        to_list_ids: to_list_ids
    }
  end

  def update
    use_case = UpdateDynamicMenu.call(dynamic_menu_id: params[:id], dynamic_menu_params: dynamic_menu_params)
    if use_case.success?

      redirect_to use_case.dynamic_menu, notice: use_case.message
    else
      render :edit, locals: {
          dynamic_menu: use_case.dynamic_menu,
          lists: DetailType.order(:name),
          from_list_ids: use_case.from_list.map(&:id),
          to_list_ids: use_case.to_list.map(&:id)
      }
    end
  end

  def index
    @dynamic_menus = DynamicMenu.includes(:detail_types).all

    render :index, locals: {dynamic_menus: @dynamic_menus}
  end

  def destroy
    dynamic_menu = DynamicMenu.find(params[:id])
    dynamic_menu.destroy

    redirect_to dynamic_menu, alert: 'You removed dynamic menu'
  end

  private

  def dynamic_menu_params
    params.require(:dynamic_menu).permit(:name, :description, from_list: [], to_list: [], category_ids: [])
  end
end