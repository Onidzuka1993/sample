class IncomeStatementsController < DashboardsController
  def show
    use_case = GetIncomeStatement.call(
        filter_by:  params[:filter_by],
        start_date: params[:start_date],
        end_date:   params[:end_date]
    )

    if use_case.success?
      render :show, locals: {
          income_statement: use_case.income_statement,
          net_income: use_case.net_income,
          date: use_case.date
      }
    else
      flash.now[:alert] = use_case.message
      render template: 'reports/index'
    end
  end
end