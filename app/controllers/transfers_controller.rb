class TransfersController < DashboardsController
  def new
    render :new
  end

  def create
    use_case = TransferFunds.call(transfer_params: transfer_params)
    if use_case.success?
      render :create
    else
      render :errors, locals: {errors: use_case.errors}
    end
  end

	def edit
    @folder = Folder.find(params[:id])

    render :edit, locals: {folder: @folder}
  end

	def update
    use_case = UpdateTransferFunds.call(folder_id: params[:id], transfer_params: transfer_params)
    if use_case.success?
      render :update
    else
      render :errors, locals: {errors: use_case.errors}
    end
  end

  private

  def transfer_params
    params.permit(:amount, :happened_at, :source_account_id, :target_account_id, :memo)
  end
end