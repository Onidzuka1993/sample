class ExpensesController < DashboardsController
	def new
		render :new, locals: {detail_type_accounts: detail_type_accounts}
	end

	def create
		use_case = CreateExpense.call(expense_params: expense_params)

		if use_case.success?
			respond_to {|format| format.json {render json: {success: true}}}
		else
			respond_to {|format| format.json {render json: use_case.message, status: :unprocessable_entity}}
		end
	end

	def edit
		@folder = Folder.find(params[:id])

		render :edit, locals: {folder: @folder, detail_type_accounts: detail_type_accounts}
	end

	def update
		folder   = Folder.find(params[:id])
		use_case = UpdateExpense.call(folder_id: folder.id, expense_params: expense_params)

		if use_case.success?
			respond_to {|format| format.json {render json: {success: true}} }
		else
			respond_to {|format| format.json {render json: use_case.message, status: :unprocessable_entity}}
		end
	end

	private

	def expense_params
		params.require(:expense).permit!
	end
end