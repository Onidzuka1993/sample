class TransactionsController < DashboardsController
	def show
		render :show
	end

	#TODO remove this action
	def index
		@transactions = Transaction.all

		render :index, locals: {transactions: @transactions}
	end
end