class CategoryTypesController < DashboardsController
  def new
    @category_type = CategoryType.new

    render :new, locals: {category_type: @category_type}
  end

  def create
    use_case = CreateCategoryType.call(category_type_params: category_type_params)
    if use_case.success?

      redirect_to use_case.category_type, notice: 'Category type has successfully been saved.'
    else
      render :new, locals: {category_type: use_case.category_type}
    end
  end

  def show
    @category_type = CategoryType.find(params[:id])

    respond_to do |format|
      format.html {render :show, locals: {category_type: @category_type}}
      format.js   {render json: @category_type.detail_types.to_json(only: ['id', 'name'])}
    end
  end

  def edit
    @category_type = CategoryType.find(params[:id])

    render :edit, locals: {category_type: @category_type}
  end

  def update
    use_case = UpdateCategoryType.call(category_type_id: params[:id], category_type_params: category_type_params)
    if use_case.success?

      redirect_to use_case.category_type, notice: 'Category type has successfully been updated.'
    else
      render :edit, locals: {category_type: use_case.category_type}
    end
  end

  def index
    @category_types = CategoryType.includes(:header_account)

    render :index, locals: {category_types: @category_types}
  end

  def destroy
    use_case = RemoveCategoryType.call(category_type_id: params[:id])
    if use_case.success?

      redirect_to category_types_url, alert: 'Category type deleted.'
    else
      flash.now[:alert] = use_case.message

      render :index, locals: {category_types: use_case.category_types}
    end
  end

  private

  def category_type_params
    params.require(:category_type).permit(:name, :description, :header_account_id)
  end
end