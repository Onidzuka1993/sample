class FoldersController < DashboardsController
	def index
		use_case = GetFolders.call(filter_by: params[:filter_by])

		render :index, locals: {folders: use_case.folders, title: use_case.title}
	end
end
