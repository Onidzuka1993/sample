class DashboardsController < ApplicationController
  layout 'dashboard'

  def detail_type_accounts
    Account.joins(detail_type: :category_type).
        where('category_type_id = :category_type_id', category_type_id: CategoryType.find_by_name('Bank'))
  end
end