class VendorsController < DashboardsController
	def new
		@vendor = Vendor.new

		render :new, locals: {vendor: @vendor}
	end

	def create
		use_case = CreateVendor.call(vendor_params: vendor_params)

		if use_case.success?

			redirect_to use_case.vendor, notice: 'Vendor has successfully been saved.'
		else
			render :new, locals: {vendor: use_case.vendor}
		end
	end

	def show
		@vendor = Vendor.find(params[:id])

		render :show, locals: {vendor: @vendor}
	end

	def edit
		@vendor = Vendor.find(params[:id])

		render :edit, locals: {vendor: @vendor}
	end

	def update
		use_case = UpdateVendor.call(vendor_id: params[:id], vendor_params: vendor_params)

		if use_case.success?

			redirect_to use_case.vendor, notice: 'Vendor has successfully been updated.'
		else
			render :edit, locals: {vendor: use_case.vendor}
		end
	end

	def index
		@vendors = Vendor.all

		render :index, locals: {vendors: @vendors}
	end

	def destroy
		vendor = Vendor.find(params[:id])
		vendor.destroy

		redirect_to vendors_url, alert: 'Vendor deleted.'
	end

	private

	def vendor_params
		params.require(:vendor).permit(:name, :address, :email, :phone_number)
	end
end
