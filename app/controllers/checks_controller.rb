class ChecksController < DashboardsController
	def new
		render :new, locals: {detail_type_accounts: detail_type_accounts}
	end

	def create
		use_case = CreateCheck.call(check_params: check_params)

		if use_case.success?
			respond_to {|format| format.json {render json: use_case.print_check_data.to_json}}
		else
			respond_to {|format| format.json {render json: use_case.message, status: :unprocessable_entity}}
		end
	end

	def edit
		@folder = Folder.find(params[:id])

		render :edit, locals: {folder: @folder, detail_type_accounts: detail_type_accounts}
	end

	def update
		folder   = Folder.find(params[:id])
		use_case = UpdateCheck.call(folder_id: folder.id, check_params: check_params)

		if use_case.success?
			respond_to {|format| format.json {render json: use_case.print_check_data.to_json} }
		else
			respond_to {|format| format.json {render json: use_case.message, status: :unprocessable_entity}}
		end
	end

	private

	def check_params
		params.require(:check).permit!
	end
end