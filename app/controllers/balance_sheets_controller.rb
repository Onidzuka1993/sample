class BalanceSheetsController < DashboardsController
  def show
    use_case = GetBalanceSheet.call(date: params[:date])

    if use_case.success?
      render :show, locals: {balance_sheet: use_case.balance_sheet.reload, date: use_case.date}
    else
      flash.now[:alert] = use_case.message
      render template: 'reports/index'
    end
  end
end