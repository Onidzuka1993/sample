class JournalEntriesController < DashboardsController
	def new
		render :new
	end

	def create
		use_case = JournalEntryOrganizer.call(journal_entry_params: journal_entry_params)

		if use_case.success?
			respond_to {|format| format.json {render json: {success: true},  status: :created}}
		else
			respond_to {|format| format.json {render json: use_case.message, status: :unprocessable_entity}}
		end
	end

	def edit
		@folder = Folder.find(params[:id])

		render :edit, locals: {folder: @folder}
	end

	def update
		use_case = UpdateJournalEntryOrganizer.call(folder_id: params[:id], journal_entry_params: journal_entry_params)

		if use_case.success?
			respond_to {|format| format.json {render json: {success: true},  status: :created}}
		else
			respond_to {|format| format.json {render json: use_case.message, status: :unprocessable_entity}}
		end
	end

	private

	def journal_entry_params
		params.require(:journal_entry).permit!
	end
end