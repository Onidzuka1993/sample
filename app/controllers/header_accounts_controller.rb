class HeaderAccountsController < DashboardsController
  def edit
    @header_account = HeaderAccount.find(params[:id])

    render :edit, locals: {header_account: @header_account}
  end

  def update
    use_case = UpdateHeaderAccount.call(header_account_id: params[:id], update_params: header_account_params)
    if use_case.success?

      redirect_to use_case.header_account, notice: 'Account has successfully been updated.'
    else
      render :edit, locals: {header_account: use_case.header_account}
    end
  end

  def show
    @header_account = HeaderAccount.find(params[:id])

    respond_to do |format|
      format.html { render :show, locals: {header_account: @header_account} }
      format.js   { render json: @header_account.category_types.to_json(only: ['id', 'name']) }
    end
  end

  def index
    @header_accounts = HeaderAccount.all

    render :index, locals: {header_accounts: @header_accounts}
  end

  private

  def header_account_params
    params.require(:header_account).permit(:description)
  end
end
