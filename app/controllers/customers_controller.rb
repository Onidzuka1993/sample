class CustomersController < DashboardsController
	def new
		@customer = Customer.new

		render :new, locals: {customer: @customer}
	end

	def create
		use_case = CreateCustomer.call(customer_params: customer_params)

		if use_case.success?

			redirect_to use_case.customer, notice: 'Customer has successfully been saved.'
		else
			render :new, locals: {customer: use_case.customer}
		end
	end

	def show
		@customer = Customer.find(params[:id])

		render :show, locals: {customer: @customer}
	end

	def edit
		@customer = Customer.find(params[:id])

		render :edit, locals: {customer: @customer}
	end

	def update
		use_case = UpdateCustomer.call(customer_id: params[:id], customer_params: customer_params)

		if use_case.success?

			redirect_to use_case.customer, notice: 'Customer has successfully been updated.'
		else
			render :edit, locals: {customer: use_case.customer}
		end
	end

	def index
		@customers = Customer.all

		render :index, locals: {customers: @customers}
	end

	def destroy
		customer = Customer.find(params[:id])
		customer.destroy

		redirect_to customers_url, alert: 'Customer deleted.'
	end

	private

	def customer_params
		params.require(:customer).permit(:name, :address, :email, :phone_number)
	end
end
