class AccountsController < DashboardsController
  def new
    @account = Account.new

    render :new, locals: {
        account: @account,
        header_accounts: HeaderAccount.order(:name),
        category_types: [],
        detail_types:   []
    }
  end

  def create
    use_case = CreateAccount.call(account_params: account_params)
    if use_case.success?

      redirect_to use_case.account, notice: use_case.message
    else
      render :new, locals: {
          account:         use_case.account,
          header_accounts: HeaderAccount.order(:name),
          category_types:  CategoryType.order(:name),
          detail_types:    DetailType.order(:name)
      }
    end
  end

  def show
    @account = Account.find(params[:id])

    respond_to do |format|
      format.html {render :show, locals: {account: @account}}
      format.js   {render json: {balance: @account.total_balance_amount}}
    end
  end

  def edit
    @account = Account.find(params[:id])

    render :edit, locals: {
				account:         @account,
				header_accounts: HeaderAccount.order(:name),
				category_types:  CategoryType.order(:name),
				detail_types:    DetailType.order(:name),
		}
  end

  def update
    use_case = UpdateAccount.call(account_id: params[:id], account_params: account_params)
    if use_case.success?

      redirect_to use_case.account, notice: use_case.message
    else
      render :edit, locals: {account: use_case.account}
    end
  end

  def index
    use_case = SortAccount.call(column: column_params, direction: direction_params)

    respond_to do |format|
      format.html {render :index, locals: {accounts: use_case.accounts}}
      format.js   {render json: Account.order(:name).to_json(only: ['id', 'name'])}
    end
  end

  def destroy
    account = Account.find(params[:id])
    account.destroy

    redirect_to accounts_url, alert: 'Account deleted.'
  end

  private

  def account_params
    params.require(:account).permit(:detail_type_id, :name, :description, :amount)
  end

	def has_no_balance?
		!@account.balances.exists?
	end

  def column_params
    params[:column] || ''
  end

  def direction_params
    params[:direction] || ''
  end
end