Rails.application.routes.draw do
  root 'transactions#show'

	resource :transaction,       only: 	[:show]
	resource :income_statement , only: 	[:show]
	resource :balance_sheet,     only: 	[:show]

  resources :header_accounts,  except: [:new, :create, :destroy]
  resources :transfers,        only: 	[:new, :create, :edit, :update]
	resources :journal_entries,  only: 	[:new, :create, :edit, :update]
	resources :checks,				   only: 	[:new, :create, :edit, :update]
	resources :expenses, 			   only: 	[:new, :create, :edit, :update]
  resources :transactions,     only: 	[:index]
  resources :folders,      		 only: 	[:index]
  resources :reports,      		 only: 	[:index]
  resources :dynamic_menus
  resources :category_types
  resources :detail_types
  resources :accounts
	resources :customers
	resources :vendors
end