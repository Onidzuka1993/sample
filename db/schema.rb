# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170602215252) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "total_balance_amount", precision: 8, scale: 2, default: "0.0", null: false
    t.integer  "detail_type_id"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_accounts_on_deleted_at", using: :btree
    t.index ["detail_type_id"], name: "index_accounts_on_detail_type_id", using: :btree
  end

  create_table "category_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "header_account_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["header_account_id"], name: "index_category_types_on_header_account_id", using: :btree
  end

  create_table "customers", force: :cascade do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "email"
    t.string   "address"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "detail_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "category_type_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["category_type_id"], name: "index_detail_types_on_category_type_id", using: :btree
  end

  create_table "dynamic_menus", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "from_list",   default: [], null: false, array: true
    t.text     "to_list",     default: [], null: false, array: true
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "dynamic_menus_detail_types", force: :cascade do |t|
    t.integer  "detail_type_id"
    t.integer  "dynamic_menu_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["detail_type_id"], name: "index_dynamic_menus_detail_types_on_detail_type_id", using: :btree
    t.index ["dynamic_menu_id"], name: "index_dynamic_menus_detail_types_on_dynamic_menu_id", using: :btree
  end

  create_table "folders", force: :cascade do |t|
    t.datetime "happened_at",    null: false
    t.string   "status",         null: false
    t.string   "folder_type",    null: false
    t.string   "address"
    t.string   "payment_method"
    t.text     "memo"
    t.string   "reason"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "vendor_id"
    t.index ["vendor_id"], name: "index_folders_on_vendor_id", using: :btree
  end

  create_table "header_accounts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.string   "status",                                    null: false
    t.string   "transaction_type",                          null: false
    t.decimal  "amount",            precision: 8, scale: 2, null: false
    t.integer  "source_account_id"
    t.integer  "target_account_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.text     "description"
    t.integer  "folder_id"
    t.index ["folder_id"], name: "index_transactions_on_folder_id", using: :btree
  end

  create_table "vendors", force: :cascade do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "email"
    t.string   "address"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_foreign_key "accounts", "detail_types"
  add_foreign_key "category_types", "header_accounts"
  add_foreign_key "detail_types", "category_types"
  add_foreign_key "dynamic_menus_detail_types", "detail_types"
  add_foreign_key "dynamic_menus_detail_types", "dynamic_menus"
  add_foreign_key "folders", "vendors"
  add_foreign_key "transactions", "folders"
end
