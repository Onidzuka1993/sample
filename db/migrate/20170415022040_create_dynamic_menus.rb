class CreateDynamicMenus < ActiveRecord::Migration[5.0]
  def change
    create_table :dynamic_menus do |t|
      t.string :name
      t.text :description
      t.text :from_list, array: true, null: false, default: []
      t.text :to_list,   array: true, null: false, default: []

      t.timestamps
    end
  end
end
