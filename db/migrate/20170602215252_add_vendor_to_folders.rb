class AddVendorToFolders < ActiveRecord::Migration[5.0]
  def change
    add_reference :folders, :vendor, foreign_key: true
  end
end
