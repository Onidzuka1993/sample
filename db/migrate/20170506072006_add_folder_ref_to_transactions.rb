class AddFolderRefToTransactions < ActiveRecord::Migration[5.0]
  def change
    add_reference :transactions, :folder, foreign_key: true
  end
end
