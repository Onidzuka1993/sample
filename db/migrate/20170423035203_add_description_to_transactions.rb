class AddDescriptionToTransactions < ActiveRecord::Migration[5.0]
  def up
    add_column :transactions, :description, :text
  end

  def down
    remove_column :transactions, :description, :text
  end
end