class CreateHeaderAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :header_accounts do |t|
      t.string :name
      t.text   :description

      t.timestamps
    end
  end
end
