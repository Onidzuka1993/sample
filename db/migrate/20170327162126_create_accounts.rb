class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string     :name
      t.text       :description
      t.decimal    :total_balance_amount, :precision => 8, :scale => 2, default: 0.0, null: false
      t.references :detail_type, foreign_key: true

      t.timestamps
    end
  end
end
