class CreateDynamicMenusDetailTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :dynamic_menus_detail_types do |t|
      t.references :detail_type,  foreign_key: true
      t.references :dynamic_menu, foreign_key: true

      t.timestamps
    end
  end
end
