class CreateCategoryTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :category_types do |t|
      t.string     :name
      t.text       :description
      t.references :header_account, foreign_key: true

      t.timestamps
    end
  end
end
