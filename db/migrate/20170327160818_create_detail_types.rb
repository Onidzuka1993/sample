class CreateDetailTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :detail_types do |t|
      t.string     :name
      t.text       :description
      t.references :category_type, foreign_key: true

      t.timestamps
    end
  end
end
