class CreateVendors < ActiveRecord::Migration[5.0]
  def change
    create_table :vendors do |t|
      t.string :name
      t.string :phone_number
      t.string :email
      t.string :address

      t.timestamps
    end
  end
end
