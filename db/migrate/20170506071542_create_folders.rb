class CreateFolders < ActiveRecord::Migration[5.0]
  def change
    create_table :folders do |t|
      t.datetime :happened_at,   null: false
      t.string   :status,        null: false
      t.string   :folder_type,   null: false
      t.string   :address
      t.string   :payment_method
      t.text     :memo
      t.string   :reason

      t.timestamps
    end
  end
end