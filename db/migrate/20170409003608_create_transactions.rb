class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.string              :status,           null: false
      t.string              :transaction_type, null: false
      t.decimal             :amount, :precision => 8, :scale => 2, null: false
      t.integer             :source_account_id
      t.integer             :target_account_id

      t.timestamps
    end
  end
end
