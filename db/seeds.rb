puts 'Start seeding..'

assets 							 = HeaderAccount.create!(name: 'Assets', description: 'Something owned or controlled by an entity.')
assets_category_type = CategoryType.create!(name: 'Bank', description: 'Bank', header_account: assets)
assets_detail_type 	 = DetailType.create!(name: 'Cash', description: 'Cash and Cash Deposits at bank accounts', category_type: assets_category_type)
Account.create!(name: 'Cash', description: 'Cash', detail_type: assets_detail_type)

equity 							 = HeaderAccount.create!(name: 'Equity', description: 'The value of assets after deducting the value of all liabilities.')
equity_category_type = CategoryType.create!(name: 'Equity', description: 'The value of assets after deducting the value of all liabilities.', header_account: equity)
equity_detail_type 	 = DetailType.create!(name: 'Opening Balance', description: 'Opening Balance', category_type: equity_category_type)
Account.create!(name: 'Opening Balance', description: 'Opening Balance', detail_type: equity_detail_type)

HeaderAccount.create!(name: 'Liabilities', description: 'Economic obligations of an entity.')
HeaderAccount.create!(name: 'Income',      description: "The company's earnings and common examples.")
HeaderAccount.create!(name: 'Expense',     description: "The company's expenditures.")

puts 'Done.'