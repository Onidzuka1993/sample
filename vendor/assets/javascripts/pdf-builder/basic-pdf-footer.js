/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {module.exports = global["BasicPDFFooter"] = __webpack_require__(1);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var BasicPDFFooter = function () {
	  _createClass(BasicPDFFooter, [{
	    key: 'addFooterData',
	    value: function addFooterData() {
	      var doc = this.pdfBuilder.doc;
	      var margins = this.pdfBuilder.margins;
				var fontSize = 9;
				var lineHeight = this.data.length - 1;
				doc.font('Helvetica');
				doc.fontSize(fontSize);

				for (var i = 0; i < this.data.length; i++) {
					if (typeof this.data[i] === "string") {
						doc.text(this.data[i], margins.left, this.height + margins.bottom - (lineHeight * fontSize));
						lineHeight -= 1;
					}else if (typeof this.data[i] === "object" && this.data[i].value !== undefined && this.data[i].label !== undefined) {
						doc.font('Helvetica-Bold').text(this.data[i].label + ": ", margins.left, this.height + margins.bottom - (lineHeight * fontSize), {lineBreak: false}).font('Helvetica').text(this.data[i].value);
						lineHeight -= 1;
					}
				}
	    }
	  }, {
	    key: 'addPagination',
	    value: function addPagination() {
	      var doc = this.pdfBuilder.doc;
	      var margins = this.pdfBuilder.margins;

	      doc.fontSize(9);

				var text = this.currentPage;
				if (this.docTitle.length > 0) {
					text = this.docTitle + ' - ' + text;
				}

	      doc.text(text, margins.left, this.height + margins.bottom, { align: 'right', width: this.width });
	    }
	  }]);

	  function BasicPDFFooter(options) {
	    _classCallCheck(this, BasicPDFFooter);

			if (options.includePagination !== undefined && options.includePagination !== null && options.includePagination === false) {
				this.includePagination = false;
			}else{
				this.includePagination = true;
				this.currentPage = 1;
			}

			this.data = options.data || [];

			if ( !Array.isArray(this.data) ){
				this.data = [this.data];
			}

	  }

	  _createClass(BasicPDFFooter, [{
	    key: 'onPageAdded',
	    value: function onPageAdded(pdfBuilder) {


				this.width = pdfBuilder.doc.page.width - pdfBuilder.margins.left - pdfBuilder.margins.right;
				this.height = pdfBuilder.doc.page.height - pdfBuilder.margins.top - pdfBuilder.margins.bottom;

				this.currentPage = pdfBuilder.currentPage;
				this.docTitle = pdfBuilder.title

	      this.pdfBuilder = pdfBuilder;


	      this.addFooterData();
				if (this.includePagination) {
					this.addPagination();
				}
	    }
	  }]);

	  return BasicPDFFooter;
	}();

	exports.default = BasicPDFFooter;
	module.exports = exports['default'];

/***/ }
/******/ ]);
