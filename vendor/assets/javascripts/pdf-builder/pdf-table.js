/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {module.exports = global["PDFTable"] = __webpack_require__(75);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },

/***/ 75:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var PDFTable = function () {
	  _createClass(PDFTable, [{
	    key: 'addToPDF',

	    /**
	      Add a table to the PDF document.
	       @method addToPDF
	      @param {PDFBuilder} builder
	    */
	    value: function addToPDF(builder) {
	      var doc = builder.doc;

	      this.builder = builder;
	      this.currentPage = doc.bufferedPageRange().count - 1;
	      this.currentPosition = { x: builder.pageLeft(), y: doc.y };
	      this.startingPosition = { x: builder.pageLeft(), y: doc.y };
	      this.startingPage = doc.bufferedPageRange().count - 1;

	      for (var i = 0; i < this.columns.length; i++) {
	        this.currentPage = this.startingPage;
	        this.currentPosition.y = this.startingPosition.y;
	        doc.switchToPage(this.currentPage);
	        this._addColumn(this.columns[i]);
	      }

	      doc.y += this.margins.bottom;
	    }

	    /**
	      Initialize a table for outputting to a PDF.
	       Usage:
	       ```javascript
	      let pdfBuilder = new PDFBuilder();
	      let table = new PDFTable();
	       pdfBuilder.addTable()
	      ```
	       Minimum options required to build out the table:
	       ```javascript
	      let table = new PDFTable([{
	        header: { text: 'ID' },
	        rows:  [{ text: '1' }, { text: '2' }, { text: '3' }]
	      }, {
	        header: { text: 'Name' },
	        rows:  [{ text: 'Billy' }, { text: 'Bob' }, { text: 'Suzy' }]
	      }]);
	      ```
	       All available options:
	       ```javascript
	      let table = new PDFTable([{
	        header: {
	          align:       'center',
	          borderColor: '#000',
	          borderStyle: 'bottom',
	          borderWidth: 2,
	          color:       '#000',
	          font:        'Helvetica-Bold',
	          fontSize:    14,
	          text:        'Name'
	        },
	         rows: [{
	          align:       'center',
	          borderColor: '#000',
	          borderStyle: 'bottom',
	          borderWidth: 2,
	          color:       '#000',
	          font:        'Helvetica-Bold',
	          fontSize:    14,
	          text:        'Brian'
	        }],
	         width: 150 // or 0.25 for 25% of the table's width
	      }]);
	      ```
	       @method constructor
	      @param {Array} columns
	      @param {Object} options
	    */

	  }]);

	  function PDFTable() {
	    var columns = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
	    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

	    _classCallCheck(this, PDFTable);

	    this.cellAlign = options.cellAlign || 'left';
	    this.cellColor = options.cellColor || '#222';
	    this.cellBorderColor = options.headerColor || '#ccc';
	    this.cellBorderWidth = options.cellBorderWidth || 1;
	    this.cellFont = options.cellFont || 'Helvetica';
			this.cellItalic = options.cellItalic || false;
	    this.cellFontSize = options.cellFontSize || 14;
	    this.headerAlign = options.headerAlign || 'left';
	    this.headerColor = options.headerColor || '#000';
	    this.headerBorderColor = options.headerColor || '#000';
	    this.headerBorderStyle = options.headerBorderStyle || 'bottom';
	    this.headerBorderWidth = options.headerBorderWidth || 1.5;
	    this.headerFont = options.headerFont || 'Helvetica-Bold';
			this.headerItalic = options.headerItalic || false;
	    this.headerFontSize = options.headerFontSize || 14;
	    this.margins = options.margins || { bottom: 30, left: 0, right: 0, top: 0 };

			this.cellEmptyColor = options.cellEmptyColor || '#FFFFFF';
			this.cellEmptyFontSize = options.cellEmptyFontSize || this.cellFontSize;
			this.cellEmptyItalic = options.cellEmptyItalic || this.cellItalic;
			this.cellEmptyText = options.cellEmptyText || 'none';

			this.headerEmptyColor = options.headerEmptyColor || '#FFFFFF';
			this.headerEmptyFontSize = options.headerEmptyFontSize || this.headerFontSize;
			this.headerEmptyItalic = options.headerEmptyItalic || this.headerItalic;
			this.headerEmptyText = options.headerEmptyText || 'none';
			this.headerAllowWrap = options.headerAllowWrap || false;
			this.cellAllowWrap = options.cellAllowWrap || false;

			this.skewFactor = Math.tan(-15 * Math.PI / 180);

	    this.columns = columns;
	  }

	  /**
	    Returns the full width of the table based on the page size and margins.
	     @return {Number}
	  */


	  _createClass(PDFTable, [{
	    key: 'width',
	    value: function width() {
	      return this.builder.contentWidth();
	    }

	    /**
	      Ouput a column from the table to the PDF.
	       @method _addColumn
	      @param {Object} column
	      @private
	    */

	  }, {
	    key: '_addColumn',
	    value: function _addColumn(column) {
	      var doc = this.builder.doc;
	      this.columnWidth = this._columnWidth(column.width);

	      // Add Header Row
	      this._addHeader(column);

	      // Add Data Rows
	      for (var i = 0; i < column.rows.length; i++) {
	        this._addRow(column, column.rows[i]);
	      }

	      // Update position for the next column
	      this.currentPosition.x += this.columnWidth;
	    }

	    /**
	      Ouput the column header to the PDF.
	       @method _addHeader
	      @param {Object} column
	      @param {Number} width
	      @private
	    */

	  }, {
	    key: '_addHeader',
	    value: function _addHeader(column) {
	      var doc = this.builder.doc;
	      var header = column.header;

	      var align = header.align || this.headerAlign;
	      var borderColor = header.borderColor || this.headerBorderColor;
	      var borderStyle = header.borderStyle || this.headerBorderStyle;
	      var borderWidth = header.borderWidth || this.headerBorderWidth;
	      var color = header.color || this.headerColor;
	      var font = header.font || this.headerFont;
				var italic = header.italic || this.headerItalic;
	      var fontSize = header.fontSize || this.headerFontSize;
	      var lineGap = this._headerLineGap(header);
	      var position = this.currentPosition;
	      var width = this.columnWidth;
				var text = header.text;
				var emptyColor = header.emptyColor || this.headerEmptyColor;
				var emptyFontSize = header.emptyFontSize || this.headerEmptyFontSize;
				var emptyItalic = header.emptyItalic || this.headerEmptyItalic;
				var emptyText = header.emptyText || this.headerEmptyText;
				var allowWrap = header.allowWrap || this.headerAllowWrap;

				if (text === undefined || text === null || text.length < 1) {
					color = emptyColor;
					fontSize = emptyFontSize;
					italic = emptyItalic;
					text = emptyText;
				}

				doc.save();
				doc.fillColor(color);
				doc.font(font);
				doc.fontSize(fontSize);
				if (italic) {
					doc.transform(1, 0, this.skewFactor, 1, -position.y*this.skewFactor + 1, 0);
				}
				if (allowWrap) {
					doc.text(text, position.x, position.y, {lineBreak: false, align: align, lineGap: lineGap, width: width});
				}else{
					doc.text(text, position.x, position.y, {lineBreak: false, align: align, lineGap: lineGap, width: width, height: lineGap, ellipsis: true});
				}
				doc.restore();

	      doc.moveTo(doc.x, doc.y - fontSize * 0.85).lineWidth(borderWidth).lineTo(doc.x + width, doc.y - fontSize * 0.85).strokeColor(borderColor).stroke();
	    }
	  }, {
	    key: '_addRow',
	    value: function _addRow(column, row) {
	      var doc = this.builder.doc;
	      var pageCount = doc.bufferedPageRange().count;

	      // Repeat the header if a new page is needed
	      var newPage = this.builder.addPageIfNeeded(this.currentPage, pageCount);
	      if (newPage) {
	        this.currentPage += 1;
	        this.currentPosition.y = doc.y;
	        this._addHeader(column, column.header);
	      }

	      var align = row.align || this.cellAlign;
	      var borderColor = row.borderColor || this.cellBorderColor;
	      var borderStyle = row.borderStyle || this.cellBorderStyle;
	      var borderWidth = row.borderWidth || this.cellBorderWidth;
				var italic = row.italic || this.cellItalic;
	      var color = row.color || this.cellColor;
	      var font = row.font || this.cellFont;
	      var fontSize = row.fontSize || this.cellFontSize;

				var allowWrap = row.allowWrap || this.cellAllowWrap;

				var emptyColor = row.emptyColor || this.cellEmptyColor;
				var emptyFontSize = row.emptyFontSize || this.cellEmptyFontSize;
				var emptyItalic = row.emptyItalic || this.cellEmptyItalic;
				var emptyText = row.emptyText || this.cellEmptyText;

				var text = row.text;

	      var lineGap = this._rowLineGap(row);
	      var width = this.columnWidth;
				if (text === undefined || text === null || text.length < 1) {
					color = emptyColor;
					fontSize = emptyFontSize;
					italic = emptyItalic;
					text = emptyText;
				}

				doc.save();
				doc.fillColor(color);
				doc.font(font);
				doc.fontSize(fontSize);
				if (italic) {
					doc.transform(1, 0, this.skewFactor, 1, -doc.y*this.skewFactor + 1, 0);
				}

				if (allowWrap) {
					doc.text(text, {lineBreak: false, align: align, lineGap: lineGap, width: width});
				}else{
					doc.text(text, {lineBreak: false, align: align, lineGap: lineGap, width: width, height: lineGap, ellipsis: true});
				}
				doc.restore();

	      if (borderStyle === 'none') {
	        return;
	      }

	      doc.moveTo(doc.x, doc.y - fontSize * 0.7).lineWidth(borderWidth).lineTo(doc.x + width, doc.y - fontSize * 0.7).strokeColor(borderColor).stroke();
	    }
	    /**
	      Ouput the column header to the PDF.
	       @method _columnWidth
	      @param {Number} width
	      @return {Number}
	      @private
	    */

	  }, {
	    key: '_columnWidth',
	    value: function _columnWidth(width) {
	      if (this.columns.length < 2) {
	        return this.width();
	      }

	      if (width == null) {
	        return this.width() / this.columns.length;
	      }

	      if (width < 1) {
	        return this.width() * width;
	      }

	      return width;
	    }
	  }, {
	    key: '_headerLineGap',
	    value: function _headerLineGap(header) {
	      var fontSize = header.fontSize || this.headerFontSize;
	      return fontSize * 1.2;
	    }
	  }, {
	    key: '_rowLineGap',
	    value: function _rowLineGap(row) {
	      var fontSize = row.fontSize || this.cellFontSize;
	      return fontSize * 1.07;
	    }
	  }]);

	  return PDFTable;
	}();

	exports.default = PDFTable;
	module.exports = exports['default'];

/***/ }

/******/ });
